import { Personne } from "Heurist";
import { FC } from "react";
import { Link } from "react-router-dom";

import { BooksTimeline, TimelineDataPoint } from "./books-timeline";
import { DateHeuristComponent } from "./date";
import { Loader } from "./loader";
import { PlaceComponent } from "./place";

export const AuthorComponent: FC<{
  author: Personne;
  firstLevel?: boolean;
  showBooksHistogram?: boolean;
  booksHistogramData?: TimelineDataPoint[];
}> = ({ author, firstLevel, booksHistogramData, showBooksHistogram }) => {
  const TitleTag = firstLevel ? "h5" : "h6";
  const fonction = (author as any).fonction;

  return (
    <>
      <TitleTag>
        <i className="fas fa-user mr-1" />
        <Link to={`/auteur/${author.id}`}>
          {[author.nom_complet, fonction && `(${fonction})`].filter((s) => s).join(" ")}
        </Link>
      </TitleTag>
      {(author.lieuDeNaissance || author.dateDeNaissance) && (
        <p className="values-list-coma">
          {(author.dateDeNaissance || author.lieuDeNaissance) && (
            <span>
              {author.dateDeNaissance ? (
                <DateHeuristComponent date={author.dateDeNaissance} prefix={"Né(e)"} />
              ) : (
                "Né(e)"
              )}
              {author.lieuDeNaissance && <PlaceComponent place={author.lieuDeNaissance} prefix=" à" />}
            </span>
          )}
          {(author.dateDeDeces || author.lieuDeDeces) && (
            <span>
              {author.dateDeDeces ? <DateHeuristComponent date={author.dateDeDeces} prefix={"Mort(e)"} /> : "Mort(e)"}
              {author.lieuDeDeces && <PlaceComponent place={author.lieuDeDeces} prefix=" à" />}
            </span>
          )}
        </p>
      )}
      {showBooksHistogram && booksHistogramData && (
        <>
          <div>
            A publié{" "}
            <span className="value">
              {author.nb_authored_publications} livre{author.nb_authored_publications || 0 > 1 ? "s" : ""}
            </span>{" "}
            de ce corpus
          </div>
          {booksHistogramData.length > 0 && (
            <BooksTimeline
              authorTimeline={{
                birth: author.dateDeNaissance,
                death: author.dateDeDeces
              }}
              booksHistogram={booksHistogramData}
              showMissing={false}
              getBarURL={({ year }) =>
                `/?t=book&q=*&tous_les_auteurs.nom_complet=${encodeURIComponent(
                  author?.nom_complet || "",
                )}&dateEdition.range.min=${encodeURIComponent(year)}&dateEdition.range.max=${encodeURIComponent(year)}`
              }
              newTab
            />
          )}
        </>
      )}
      {showBooksHistogram && !booksHistogramData && <Loader tag="h6" />}
    </>
  );
};
