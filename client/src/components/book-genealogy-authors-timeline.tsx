import { Genealogy, Organisation, Personne, ProductionPedagogiqueEnDroit } from "Heurist";
import { compact, last, max, min, sortBy, values } from "lodash";

import { Link } from "react-router-dom";

interface Props {
  bookGenealogy: Genealogy<ProductionPedagogiqueEnDroit>;
  yearWidth: number;
  yearScope: {
    minYear: number;
    maxYear: number;
  };
}

interface PersonneFonction extends Personne {
  fonction?: string;
}

interface ItemsTimelineProps extends Props {
  getItems: (book: ProductionPedagogiqueEnDroit) => Array<PersonneFonction | Organisation>;
}

interface Item {
  item?: PersonneFonction | Organisation;
  boxes: {
    startYear?: number;
    endYear?: number;
  }[];
  lastAddedIndex?: number;
}

const ITEM_BOX_HEIGHT = 40;
const ITEM_BOX_MARGIN = 8;

export const getItemsGroups = (
  bookGenealogy: Genealogy<ProductionPedagogiqueEnDroit>,
  getItems: (book: ProductionPedagogiqueEnDroit) => Array<PersonneFonction | Organisation>,
): Item[] => {
  const items: { [key: string]: Item } = {};
  bookGenealogy.nodes.forEach((book: ProductionPedagogiqueEnDroit, i: number) => {
    const startYear = book.dateEdition?.range.gte.getFullYear();
    const endYear = book.dateEdition?.range.lte.getFullYear();
    getItems(book).forEach((item: PersonneFonction | Organisation) => {
      if (!items[item.id]) items[item.id] = { item, boxes: [] };

      //itemsLine[item.id] = { item, startYear: itemsLine[item.id]?.startYear || startYear, endYear };
      if (items[item.id].lastAddedIndex !== i - 1 || items[item.id].boxes.length === 0) {
        // create a group
        const box = { startYear, endYear };
        items[item.id].boxes.push(box);
      } else {
        // expand group
        const lastBox = last(items[item.id].boxes);
        if (lastBox) lastBox.endYear = endYear;
      }
      items[item.id].lastAddedIndex = i;
    });
  });
  return sortBy(values(items), (item) => {
    const start = min(item.boxes.map((b) => b.startYear));
    const end = max(item.boxes.map((b) => b.endYear));
    return [start, (end || 0) - (start || 0)];
  });
};

const BookGenealogyItemsTimeline: React.FC<ItemsTimelineProps> = (props: ItemsTimelineProps) => {
  const { bookGenealogy, yearWidth, yearScope, getItems } = props;
  const { minYear, maxYear } = yearScope;

  const yearToX = (year: number) => (year - minYear) * yearWidth;

  const itemsGroups = getItemsGroups(bookGenealogy, getItems);
  return (
    <div style={{ height: itemsGroups.length * ITEM_BOX_HEIGHT }}>
      {itemsGroups.map((item, i) => (
        <div className="items-timeline" key={i}>
          {item.boxes.map((box, box_i) => (
            <div
              key={box_i}
              className="items-group"
              style={{
                top: i * ITEM_BOX_HEIGHT,
                left: yearToX(box.startYear || minYear),
                width: ((box.endYear || maxYear) - (box.startYear || minYear) + 1) * yearWidth,
                height: ITEM_BOX_HEIGHT,
              }}
            />
          ))}
          {item.item && (
            <>
              <div
                key={`label${i}`}
                className="label"
                style={{
                  marginTop: ITEM_BOX_MARGIN,
                  height: ITEM_BOX_HEIGHT,
                  top: i * ITEM_BOX_HEIGHT,
                  left: yearToX(item.boxes[0].startYear || minYear),
                  width: max([(maxYear - (item.boxes[0].startYear || minYear) + 1) * yearWidth, 200]),
                }}
              >
                {item.item.type === "personne" && (
                  <span key={item.item.id}>
                    <i className={`fas fa-user mr-1 ${(item.item as PersonneFonction).fonction ? "secondary" : ""}`} />
                    <Link
                      to={`/auteur/${item.item.id}`}
                      title={item.item.nom_complet}
                      style={{
                        maxWidth: max([(maxYear - (item.boxes[0].startYear || minYear) + 1) * yearWidth, 200]),
                      }}
                    >
                      {item.item.nom_complet}
                      {(item.item as PersonneFonction).fonction && <> (){(item.item as PersonneFonction).fonction})</>}
                    </Link>
                  </span>
                )}
                {item.item.type === "organisation" && (
                  <span key={item.item.id}>
                    <i className="fas fa-building mr-1" />
                    <Link
                      to={`/editeur/${item.item.id}`}
                      title={item.item.nom_complet}
                      style={{
                        maxWidth: max([(maxYear - (item.boxes[0].startYear || minYear) + 1) * yearWidth, 200]),
                      }}
                    >
                      {item.item.nom_complet}
                    </Link>
                  </span>
                )}
              </div>
            </>
          )}
        </div>
      ))}
    </div>
  );
};

export const BookGenealogyAuthorsTimeline: React.FC<Props> = (props: Props) => {
  const getAuthors = (book: ProductionPedagogiqueEnDroit): (PersonneFonction | Organisation)[] => {
    return book
      ? compact(
          (book.auteurs || []).concat(
            (book.fonctionAuteur || []).map((fonctionAuteur) => ({
              ...(fonctionAuteur.other as PersonneFonction),
              fonction: fonctionAuteur.relation,
            })),
          ),
        )
      : [];
  };

  return BookGenealogyItemsTimeline({ ...props, getItems: getAuthors });
};
export const BookGenealogyEditorsTimeline: React.FC<Props> = (props: Props) => {
  const getEditors = (book: ProductionPedagogiqueEnDroit): (PersonneFonction | Organisation)[] => {
    return book.editeurs || [];
  };

  return BookGenealogyItemsTimeline({ ...props, getItems: getEditors });
};
