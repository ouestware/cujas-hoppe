import { Genealogy, ProductionPedagogiqueEnDroit } from "Heurist";
import { first } from "lodash";
import { FC } from "react";
import { Link } from "react-router-dom";

import { DateHeuristComponent } from "./date";

export const BookGenealogyResultComponent: FC<{
  bookGenealogy: Genealogy<ProductionPedagogiqueEnDroit>;
}> = ({ bookGenealogy }) => {
  if (bookGenealogy.nodes)
    return (
      <>
        <h5>
          <i className="fas fa-book mr-1" />
          {first(bookGenealogy.nodes)?.titre} de{" "}
          {first(bookGenealogy.nodes)
            ?.auteurs?.map((a) => a.nom_complet)
            .join("; ")}
        </h5>
        <p>{bookGenealogy.nodes.length} éditions</p>
        <ul>
          {bookGenealogy.nodes.map((book) => {
            return (
              <li key={book.id}>
                {book.dateEdition && (
                  <>
                    <DateHeuristComponent onlyYear={true} date={book.dateEdition} />,{" "}
                  </>
                )}
                <Link to={`/ouvrage/${book.id}/genealogie`}>{book.mentionEdition || "sans mention d'édition"}</Link>
              </li>
            );
          })}
        </ul>
      </>
    );
  else return <>Généalogie introuvable</>;
};
