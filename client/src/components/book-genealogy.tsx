import { Genealogy, ProductionPedagogiqueEnDroit } from "Heurist";
import { max, min, range, uniq } from "lodash";
import { CSSProperties } from "react";
import { Link } from "react-router-dom";
import config from "../config";
import { BookGenealogyAuthorsTimeline, BookGenealogyEditorsTimeline } from "./book-genealogy-authors-timeline";
import { dateHeuristLabel } from "./date";
import withSize, { SizeState } from "./with-size";

interface BookTimeBoxProps {
  book: ProductionPedagogiqueEnDroit;
  yearWidth: number;
  yearScope: {
    minYear: number;
    maxYear: number;
  };
  focused: boolean;
  position: number;
}
const BOX_HEIGHT = 25;
const BOX_MARGIN_BOTTOM = 5;
const BOX_BORDER_WIDTH = 2;

const BookTimeBox: React.FC<BookTimeBoxProps> = (props: BookTimeBoxProps) => {
  const { book, yearWidth, focused, position, yearScope } = props;
  const { minYear, maxYear } = yearScope;
  const yearToX = (year: number) => (year - minYear) * yearWidth;

  const startYear = book.dateEdition?.range.gte.getFullYear();
  const endYear = book.dateEdition?.range.lte.getFullYear();

  // MAIN ITEMS
  const lineStyle = { top: position * (BOX_HEIGHT + BOX_MARGIN_BOTTOM + 2 * BOX_BORDER_WIDTH) };
  const boxStyle: CSSProperties = {
    top: 0,
    left: yearToX(startYear || minYear) - BOX_BORDER_WIDTH,
    width: ((endYear || maxYear) - (startYear || minYear) + 1) * yearWidth,
    height: BOX_HEIGHT,
    borderWidth: BOX_BORDER_WIDTH,
    marginBottom: BOX_MARGIN_BOTTOM,
  };
  const labelStyle: CSSProperties = {
    top: 0,
    left: yearToX(startYear || minYear) - BOX_BORDER_WIDTH,
    width: 150,
    height: BOX_HEIGHT,
    marginBottom: BOX_MARGIN_BOTTOM,
  };

  return (
    <div className={`book-timeline ${focused ? "focused" : ""}`} style={lineStyle}>
      {/*  Label */}
      <span className={`label`} style={labelStyle}>
        <i className="fas fa-book mr-1" />
        <Link
          to={`/ouvrage/${book.id}`}
          title={`'${book.titre}'${dateHeuristLabel({ onlyYear: true, date: book.dateEdition })}`}
        >
          {book.mentionEdition || "sans mention d'édition"}
        </Link>
      </span>
      <div style={boxStyle} className="box"></div>
    </div>
  );
};

interface Props {
  bookGenealogy: Genealogy<ProductionPedagogiqueEnDroit>;
  focusedEditorId: string;
}

const BookGenealogyWithSize: React.FC<Props & SizeState> = (props) => {
  const { width, bookGenealogy, focusedEditorId } = props;
  const allYears = uniq(
    bookGenealogy.nodes
      .map((n) => n.dateEdition?.range.gte.getFullYear() || Infinity)
      .concat(bookGenealogy.nodes.map((n) => n.dateEdition?.range.lte.getFullYear() || -Infinity)),
  );
  const minYear = min(allYears) || config.minYear;
  const maxYear = max(allYears) || config.maxYear;

  const yearWidth = width / (maxYear - minYear + 1);
  // Compute x step value
  const xCaptionStep =
    [2, 5, 10, 20].find((n) => {
      const labelWidth = (width / (maxYear + 1 - minYear)) * n;
      return labelWidth >= 25;
    }) || 50;
  return (
    <>
      {bookGenealogy.nodes.length} éditions entre {minYear} et {maxYear}
      <ul className="book-genealogy-caption" style={{ width: "100%" }}>
        {range(minYear, maxYear + 1)
          .filter((y) => y % xCaptionStep === 0)
          .map((year) => (
            <li
              key={year}
              style={{
                left: (year - minYear) * yearWidth,
                width: yearWidth,
              }}
            >
              <span>{year}</span>
            </li>
          ))}
      </ul>
      <div
        className="book-genealogy"
        style={{ height: bookGenealogy.nodes.length * (BOX_HEIGHT + 2 * BOX_BORDER_WIDTH + BOX_MARGIN_BOTTOM) }}
      >
        {bookGenealogy.nodes.map((b, i) => (
          <BookTimeBox
            book={b}
            position={i}
            key={b.id}
            yearWidth={yearWidth}
            yearScope={{ minYear, maxYear }}
            focused={focusedEditorId === b.id}
          />
        ))}
      </div>
      <BookGenealogyAuthorsTimeline
        bookGenealogy={bookGenealogy}
        yearWidth={yearWidth}
        yearScope={{ minYear, maxYear }}
      />
      <BookGenealogyEditorsTimeline
        bookGenealogy={bookGenealogy}
        yearWidth={yearWidth}
        yearScope={{ minYear, maxYear }}
      />
    </>
  );
};
export const BookGenealogyComponent = withSize<Props>(BookGenealogyWithSize);
