import { compact, identity } from "lodash";
import { FC } from "react";
import { Link } from "react-router-dom";

import { Organisation, Personne, ProductionPedagogiqueEnDroit } from "Heurist";
import { AuthorComponent } from "./author";
import { DateHeuristComponent } from "./date";
import { AuthorOrgComponent } from "./editor";

export const BookComponent: FC<{ book: ProductionPedagogiqueEnDroit; firstLevel?: boolean }> = ({
  book,
  firstLevel,
}) => {
  const authors: (Personne | Organisation)[] = compact(
    (book.auteurs || []).concat(
      (book.fonctionAuteur || []).map((fonctionAuteur) => ({
        ...(fonctionAuteur.other as Personne),
        fonction: fonctionAuteur.relation,
      })),
    ),
  );

  return (
    <>
      <Link to={`/ouvrage/${book.id}`}>
        <h5 className="mv-0" title={book.titre}>
          <i className="fas fa-book mr-1" />
          {book.titre}
          {(book.complementDeTitre || []).length ? ` : ${book.complementDeTitre!.join(", ")}` : ""}
        </h5>
        {(book.titreDEnsemble || book.titreDePartie) && (
          <h6>
            {[book.titreDEnsemble, [book.numeroDePartie, book.titreDePartie].filter(identity).join(" : ")]
              .filter(identity)
              .join(" - ")}
          </h6>
        )}
      </Link>

      {((book.editeurs && book.editeurs.length > 0) || book.dateEdition) && (
        <p>
          <span className="caption">Édité</span>
          {book.editeurs && book.editeurs.length > 0 && (
            <>
              {" "}
              par{" "}
              <span className="value values-list-coma-and">
                {book.editeurs.map((o: Organisation, i: number) => (
                  <span key={i}>
                    {o.typeOrganisme === "Editeur (personne morale)" ? (
                      <Link to={`/editeur/${o.id}`}>
                        <i className="fas fa-building mr-1" />
                        {o.nomEntiteMorale}
                      </Link>
                    ) : (
                      o.nomEntiteMorale
                    )}
                  </span>
                ))}
              </span>
            </>
          )}
          {book.dateEdition && <DateHeuristComponent onlyYear={true} date={book.dateEdition} />}
        </p>
      )}

      {authors && authors.length > 0 && firstLevel && (
        <div>
          Auteurs :
          <ul className="list-unstyled">
            {authors.map((entity: Personne | Organisation) => (
              <li key={entity.id} className="result">
                {(entity as Personne).nom ? (
                  <AuthorComponent author={entity as Personne} firstLevel={false} />
                ) : (
                  <AuthorOrgComponent editor={entity as Organisation} firstLevel={false} />
                )}
              </li>
            ))}
          </ul>
        </div>
      )}
    </>
  );
};
