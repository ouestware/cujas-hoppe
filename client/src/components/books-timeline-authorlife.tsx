import { DateHeurist } from "Heurist";
import { FC } from "react";
import { dateHeuristYearRange } from "./date";
import { colorHexToRgb } from "../utils";
import config from "../config";

const DEFAULT_ALPHA = 1;
const LIFE_EXPECTANCY = 100;

function getAuthorBounds(
  birth?: DateHeurist,
  death?: DateHeurist,
): null | {
  birthRange: { value: [number, number]; computed: boolean };
  deathRange: { value: [number, number]; computed: boolean };
} {
  const birthRange = dateHeuristYearRange(birth);
  const deathRange = dateHeuristYearRange(death);
  if (birthRange && deathRange)
    return {
      birthRange: { value: birthRange, computed: false },
      deathRange: { value: deathRange, computed: false },
    };

  if (birthRange && !deathRange)
    return {
      birthRange: { value: birthRange, computed: false },
      deathRange: { value: [birthRange[1], birthRange[1] + LIFE_EXPECTANCY], computed: true },
    };
  if (deathRange && !birthRange)
    return {
      birthRange: { value: [deathRange[0] - LIFE_EXPECTANCY, deathRange[0]], computed: true },
      deathRange: { value: deathRange, computed: false },
    };

  return null;
}

export const TimelineAuthorLifeLegend: FC = () => (
  <div className="legend">
    <div className="barchart-legend" style={{ backgroundColor: config.colors.authorLifetimeColor }} />
    Période de vie connue
  </div>
);

const AUTHOR_LIFE_COLOR_RGB = colorHexToRgb(config.colors.authorLifetimeColor);
interface TimelineAuthorLifeProps {
  birth?: DateHeurist;
  death?: DateHeurist;
  timelineBounds: { min: number; max: number };
}
export const TimelineAuthorLife: FC<TimelineAuthorLifeProps> = ({ birth, death, timelineBounds }) => {
  const authorBounds = getAuthorBounds(birth, death);
  if (!authorBounds) return null;

  const { birthRange, deathRange } = authorBounds;

  // timeline year width
  const timelinePeriod = timelineBounds.max - timelineBounds.min;
  const authorPeriod = deathRange.value[1] - birthRange.value[0];
  const leftPosition = ((birthRange.value[0] - timelineBounds.min) / timelinePeriod) * 100;

  const fuzzyLeftOffset = ((birthRange.value[1] - birthRange.value[0]) / authorPeriod) * 100;
  const rightPosition = ((timelineBounds.max - deathRange.value[1]) / timelinePeriod) * 100;
  const fuzzyRightOffset = ((deathRange.value[1] - deathRange.value[0]) / authorPeriod) * 100;

  const lastAlpha =
    deathRange.value[1] > timelineBounds.max ? (timelineBounds.max - deathRange.value[0]) / authorPeriod : 0;
  return (
    <div
      style={{
        position: "absolute",
        top: 0,
        left: `${leftPosition > 0 ? leftPosition : 0}%`,
        borderLeft: !birthRange.computed && birthRange.value[0] >= timelineBounds.min ? "1px solid grey" : "1px hidden",
        borderRight:
          !deathRange.computed && deathRange.value[1] <= timelineBounds.max ? "1px solid grey" : "1px hidden",
        right: `${rightPosition > 0 ? rightPosition : 0}%`,
        background: `linear-gradient(
          90deg, 
          rgba(${AUTHOR_LIFE_COLOR_RGB.r}, ${AUTHOR_LIFE_COLOR_RGB.g}, ${AUTHOR_LIFE_COLOR_RGB.b},0.0) 0%,
          rgba(${AUTHOR_LIFE_COLOR_RGB.r}, ${AUTHOR_LIFE_COLOR_RGB.g}, ${AUTHOR_LIFE_COLOR_RGB.b}, ${DEFAULT_ALPHA}) ${fuzzyLeftOffset}%,
          rgba(${AUTHOR_LIFE_COLOR_RGB.r}, ${AUTHOR_LIFE_COLOR_RGB.g}, ${AUTHOR_LIFE_COLOR_RGB.b},${DEFAULT_ALPHA})  ${100 - fuzzyRightOffset}%,
          rgba(${AUTHOR_LIFE_COLOR_RGB.r}, ${AUTHOR_LIFE_COLOR_RGB.g}, ${AUTHOR_LIFE_COLOR_RGB.b}, ${lastAlpha}) 100%
      )`,
        height: "100%",
      }}
    />
  );
};
