import cx from "classnames";
import { keyBy, range } from "lodash";
import { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";

import config from "../config";
import { getElementPosition } from "../utils";
import withSize, { SizeState } from "./with-size";
import { TimelineAuthorLife, TimelineAuthorLifeLegend } from "./books-timeline-authorlife";
import { DateHeurist } from "Heurist";

/**
 * Check if the index is in the defined range.
 */
function isInSelection(index: number, range: [number, number] | null): boolean {
  let result = false;
  if (range) {
    const from: number = range[0];
    const to: number = range[1];
    if (from <= to && from <= index && index <= to) result = true;
    if (from > to && to <= index && index <= from) result = true;
  }
  return result;
}

interface Mark {
  year: string;
  label: string;
}

export interface TimelineDataPoint {
  year: string;
  value: number;
}

interface BookTimelineProps {
  authorTimeline?: { birth?: DateHeurist; death?: DateHeurist };
  booksHistogram: TimelineDataPoint[];
  missingBooksHistogram?: TimelineDataPoint[];
  marks?: Mark[];
  showMissing?: boolean;
  hideXAxis?: boolean;
  boundaries?: { min?: number; max?: number };
  getBarURL?: (data: { year: number }) => string;
  newTab?: boolean;
  // What we do when we do when we ends the selection (ie brush) ?
  onSelectionEnd?: (from: TimelineDataPoint | null, to: TimelineDataPoint | null) => void;
  // The  period that is highlighted (ex: [1900, 1910])
  highlightedPeriod?: [number, number];
}

export const BooksTimelineWithSize: React.FC<BookTimelineProps & SizeState> = (props) => {
  const {
    authorTimeline,
    booksHistogram,
    missingBooksHistogram,
    width,
    showMissing,
    hideXAxis,
    getBarURL,
    newTab,
    highlightedPeriod,
    onSelectionEnd,
  } = props;
  // State for the data
  const [data, setData] = useState<TimelineDataPoint[]>([]);
  // State for the missing data
  const [missingData, setMissingData] = useState<TimelineDataPoint[]>([]);
  // State for the boundaries
  const [boundaries, setBoundaries] = useState<{ min: number; max: number }>({ min: 0, max: 0 });
  // State for the max value of the data
  const [maxValue, setMaxValue] = useState<number>(-Infinity);
  // State for the width of a bar
  const [barWidth, setBarWidth] = useState<string>("0%");
  // State for the x caption step
  const [xCaptionStep, setXCaptionStep] = useState<number>(50);
  // State for the x caption step
  const [yCaptionValue, setYCaptionValue] = useState<number>(50);
  // State for the initial highlighted period defined by data's index (not per year)
  const [initialSelect, setInitialSelect] = useState<[number, number] | null>(null);
  // State for the period selection (brush) defined by  data's index (not per year)
  const [selection, setSelection] = useState<[number, number] | null>(null);
  const barchartRef = useRef<HTMLUListElement | null>(null);

  // When raw data changed
  // => we recompute the data
  useEffect(() => {
    // Compute max data
    const max = Math.max(...booksHistogram.map((d) => d.value));
    setMaxValue(max);

    // Compute boundaries
    const bounds = {
      min: config.minYear,
      max: config.maxYear,
      ...(props.boundaries || {}),
    };
    setBoundaries(bounds);

    // Compute bar width
    setBarWidth((1 / (bounds.max + 1 - bounds.min)) * 100 + "%");

    // Compute x step value
    setXCaptionStep(
      [1, 2, 5, 10, 20].find((n) => {
        const labelWidth = (width / (bounds.max + 1 - bounds.min)) * n;
        return labelWidth >= 25;
      }) || 50,
    );

    // Compute y caption value
    const maxValue10 = max > 0 ? Math.pow(10, Math.floor(Math.log10(max))) : 0;
    setYCaptionValue(([5, 2].find((n) => n * maxValue10 < max) || 1) * maxValue10);

    // Compute data & missing data
    // - add zero to missing years
    // - sort
    const dataIndexedByYear = keyBy(booksHistogram, (o) => o.year);
    const missingDataIndexedByYear = keyBy(missingBooksHistogram, (o) => o.year);
    const newData: TimelineDataPoint[] = [];
    const newMissingData: TimelineDataPoint[] = [];
    range(bounds.min, bounds.max + 1).forEach((y) => {
      newData.push(dataIndexedByYear[y] || { year: y + "", value: 0 });
      const totalYear = (dataIndexedByYear[y]?.value || 0) + (missingDataIndexedByYear[y]?.value || 0);
      newMissingData.push({
        year: y + "",
        value: totalYear !== 0 ? (missingDataIndexedByYear[y]?.value || 0) / totalYear : 0,
      });
    });
    setData(newData);
    setMissingData(newMissingData);
  }, [booksHistogram, missingBooksHistogram, props.boundaries, width]);

  // When the data changed or the selection
  //  => we recompute the initialSelect
  useEffect(() => {
    if (highlightedPeriod) {
      setInitialSelect([
        data.findIndex((e) => e.year === `${highlightedPeriod[0]}`),
        data.findIndex((e) => e.year === `${highlightedPeriod[1]}`),
      ]);
    } else {
      setInitialSelect(null);
    }
  }, [highlightedPeriod, data]);

  // When selection is in progress
  // => we register an on move on the document to track the mouse
  // => we register an mouseup to finalize the selection
  useEffect(() => {
    if (onSelectionEnd && barchartRef.current && selection) {
      // function that handle the mouse move and track the closest item for the selection
      const mouseMoveHandler = (e: MouseEvent) => {
        const barChartWidth = (barchartRef.current as HTMLElement).clientWidth;
        const barChartStartX = getElementPosition(barchartRef.current as HTMLElement)[0];
        const barChartEndX = barChartStartX + barChartWidth;
        const mouseX = e.clientX;

        // Mouse if before the element => select 0
        if (mouseX <= barChartStartX) {
          setSelection((selection) => {
            if (selection) {
              return [selection[0], 0];
            }
            return null;
          });
        }

        // Mouse if after the element => take last item
        if (mouseX >= barChartEndX) {
          setSelection((selection) => {
            if (selection) {
              return [selection[0], data.length - 1];
            }
            return null;
          });
        }

        // Mouse if in the element range => search the closest item
        if (barChartStartX < mouseX && mouseX < barChartEndX) {
          setSelection((selection) => {
            if (selection) {
              return [selection[0], Math.round((mouseX - barChartStartX) / (barChartWidth / data.length))];
            }
            return null;
          });
        }
      };

      // function to finalized the selection
      const mouseUpHandler = () => {
        // if selection monde is on
        if (onSelectionEnd && selection) {
          // Searching the data for the inf/sup border
          // Note : a selection can start by selecting the up border, so we need to reorder
          const from = selection[0] < selection[1] ? selection[0] : selection[1];
          const to = selection[0] < selection[1] ? selection[1] : selection[0];
          // Calling the function with the data that match  the borders
          onSelectionEnd(data[from], data[to]);
        }
        // reset the selection
        setSelection(null);
      };

      document.addEventListener("mousemove", mouseMoveHandler as EventListenerOrEventListenerObject);
      document.addEventListener("mouseup", mouseUpHandler as EventListenerOrEventListenerObject);
      // cleanup
      return () => {
        document.removeEventListener("mousemove", mouseMoveHandler as EventListenerOrEventListenerObject);
        document.removeEventListener("mouseup", mouseUpHandler as EventListenerOrEventListenerObject);
      };
    }
    return;
  }, [barchartRef, data, selection, onSelectionEnd]);

  return (
    <div>
      <div className="barchart unselectable position-relative">
        {/* Timeline of the author on the histogram if present */}
        {authorTimeline && (
          <TimelineAuthorLife birth={authorTimeline?.birth} death={authorTimeline?.death} timelineBounds={boundaries} />
        )}

        {/* HISTOGRAM */}
        <ul className={`bars ${selection || highlightedPeriod ? "selection" : ""}`} ref={barchartRef}>
          {data.map((d, index) => {
            const url = getBarURL && getBarURL({ year: +d.year });
            const selected = isInSelection(index, selection ? selection : initialSelect);
            const classes: string[] = ["bar-container"];

            if (onSelectionEnd)
              if (d.value !== 0 && !selected)
                // drag cursor when selecting by dragging is possible and not already selected
                classes.push("selectable");
              else if (selected || (!highlightedPeriod && d.value === 0))
                // pointer cursor (reset) when selecting by dragging is possible and already selected
                classes.push("clickable");
            if (selected) classes.push("selected");

            return (
              <li
                key={index}
                data-year={d.year}
                title={d.year + " : " + d.value}
                className={classes.join(" ")}
                onMouseDown={() => {
                  // if selection mode is on
                  if (onSelectionEnd) {
                    if (selected || (d.value === 0 && !highlightedPeriod)) {
                      // reset selection
                      onSelectionEnd(null, null);
                    } else {
                      // apply
                      setSelection([index, index]);
                    }
                  }
                }}
              >
                {!onSelectionEnd && url ? (
                  <Link
                    className="bar"
                    to={url}
                    target={newTab ? "_blank" : undefined}
                    key={d.year}
                    style={{ height: `${(100 * d.value) / maxValue}%` }}
                  />
                ) : (
                  <span className="bar" style={{ height: `${(100 * d.value) / maxValue}%` }} />
                )}
              </li>
            );
          })}
          <li className={cx("y-caption", "right")} style={{ bottom: `${(100 * yCaptionValue) / maxValue}%` }}>
            <span>{yCaptionValue}</span>
          </li>
        </ul>
        {/* MISSING DATA HEATMAP */}
        {showMissing && missingData && (
          <ul className="bars missingValues">
            {missingData.map((d) => (
              <li
                className="bar-container"
                key={d.year}
                data-year={d.year}
                title={`${d.year} : ${d.value * 100}%`}
                style={{
                  height: "100%",
                  opacity: d.value,
                }}
              />
            ))}
          </ul>
        )}
        {/* YEAR AXE */}
        {!hideXAxis && (
          <ul className="xaxis">
            {range(boundaries.min, boundaries.max + 1)
              .filter((y) => y % xCaptionStep === 0)
              .map((year) => (
                <li
                  key={year}
                  style={{
                    left: ((year - boundaries.min) / (boundaries.max + 1 - boundaries.min)) * 100 + "%",
                    width: barWidth,
                  }}
                >
                  <span>{year}</span>
                </li>
              ))}
          </ul>
        )}
      </div>
      {authorTimeline && <TimelineAuthorLifeLegend />}
    </div>
  );
};

export const BooksTimeline = withSize<BookTimelineProps>(BooksTimelineWithSize);
