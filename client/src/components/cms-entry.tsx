import { WebSitePage } from "Heurist";
import { FC, useEffect, useState } from "react";
import { useLocation } from "react-router";
import { getCMSEntry } from "../elasticsearchClient";
import { Loader } from "./loader";

interface Props {
  id: number;
  hideLoader?: boolean;
}

export const CMSEntry: FC<Props> = (props: Props) => {
  const { id, hideLoader } = props;
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const isPreview = queryParams.get("preview");
  const [loading, setLoading] = useState<boolean>(false);
  const [page, setPage] = useState<WebSitePage | null>(null);

  // fetch data when props id changed
  useEffect(() => {
    setLoading(true);
    getCMSEntry(id)
      .then((data) => {
        setPage(data);
      })
      .catch((error) => {
        console.error(error);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [id]);

  return (
    <>
      {page && typeof isPreview === "string" && (
        <textarea
          style={{ width: "100%", resize: "vertical", height: 330 }}
          value={page?.text || ""}
          onChange={(e) => setPage({ ...page, text: e.target.value })}
        />
      )}
      {loading && !hideLoader && <Loader />}
      {!loading && <div dangerouslySetInnerHTML={{ __html: page?.text || "" }} />}
    </>
  );
};
