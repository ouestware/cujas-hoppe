import { Collection } from "Heurist";
import { identity } from "lodash";
import { FC } from "react";
import { Link } from "react-router-dom";
import { BooksTimeline, TimelineDataPoint } from "./books-timeline";
import { Loader } from "./loader";

export const CollectionComponent: FC<{
  collection: Collection;
  firstLevel?: boolean;
  showBooksHistogram?: boolean;
  booksHistogramData?: TimelineDataPoint[];
}> = ({ collection, firstLevel, booksHistogramData, showBooksHistogram }) => {
  const startYear = collection.dateDeCreation ? collection.dateDeCreation.range.gte.getFullYear() : null;
  const endYear = collection.dateDeDisparition ? collection.dateDeDisparition.range.lte.getFullYear() : null;

  const TitleTag = firstLevel ? "h5" : "h6";
  const nb_publications = collection.nb_publications;
  return (
    <>
      <TitleTag>
        <i className="fas fa-lines-leaning  mr-1" />

        <Link to={`/collection/${collection.id}`}>{collection.nomDeLaCollection}</Link>
      </TitleTag>
      {(startYear || endYear) && (
        <p>
          {[startYear && `Créée en ${startYear}`, endYear && `Disparue en ${endYear}`].filter(identity).join(" - ")}
        </p>
      )}
      {showBooksHistogram && booksHistogramData && (
        <>
          <div>
            A édité{" "}
            <span className="value">
              {nb_publications} livre{nb_publications || 0 > 1 ? "s" : ""}
            </span>{" "}
            de ce corpus
          </div>
          {booksHistogramData.length > 0 && (
            <BooksTimeline
              booksHistogram={booksHistogramData}
              showMissing={false}
              getBarURL={({ year }) =>
                `/?t=book&q=*&collection.nomDeLaCollection=${encodeURIComponent(
                  collection?.nomDeLaCollection || "",
                )}&dateEdition.range.min=${encodeURIComponent(year)}&dateEdition.range.max=${encodeURIComponent(year)}`
              }
              newTab
            />
          )}
        </>
      )}
      {showBooksHistogram && !booksHistogramData && <Loader tag="h6" />}
    </>
  );
};
