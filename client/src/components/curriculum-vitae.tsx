import { ElementDeCarriere } from "Heurist";
import { groupBy } from "lodash";
import { FC } from "react";
import { YearRangeComponent } from "../components/date";

export const CVLine: FC<{ position: ElementDeCarriere }> = ({ position }) => (
  <div>
    {position.periodeDateDeDebut && position.periodeDateDeFin && (
      <>
        <YearRangeComponent
          dateRange={[position.periodeDateDeDebut.date, position.periodeDateDeFin.date]}
          prefix=""
          glue="-"
          withoutHighlight={true}
        />{" "}
      </>
    )}
    {position.dateEvenement && !position.periodeDateDeDebut && !position.periodeDateDeFin && (
      <>
        {position.periodeDateDeDebut ? " " : ""}
        {position.dateEvenement.date.getFullYear()}{" "}
      </>
    )}
    {(position.periodeDateDeDebut || position.periodeDateDeFin || position.dateEvenement) && " : "}
    {[
      // fonction + matière
      position.fonctionOuGrade,
      position.matiereEnseignee,
      // other function are displayed only for non-teaching evenement
      position.typeEvenementCarriere !== "Enseignement" && position.fonctionAdministrative,
      position.typeEvenementCarriere !== "Enseignement" && position.fonctionEditoriale,
      position.typeEvenementCarriere !== "Enseignement" && position.activiteProfessionnelle,
      position.typeEvenementCarriere !== "Enseignement" && position.activiteAssociative,
      /* TODO : link to organisation ? */
      position.institutionDeRattachement && position.institutionDeRattachement.length > 0
        ? position.institutionDeRattachement.map((e) => e.nomEntiteMorale).join(", ")
        : "",
    ]
      .filter((f) => f && f !== "")
      .join(", ")}
  </div>
);

export const CurriculumVitae: FC<{ CV: ElementDeCarriere[] }> = ({ CV }) => {
  // Define the CV sctions labels and order
  const sections: { [key: string]: string }[] = [
    { type: "Enseignement", label: "Enseignement" },
    { type: "Association", label: "Fonctions associatives" },
    { type: "Edition", label: "Fonctions éditoriales" },
    { type: "Administration", label: "Fonctions administratives" },
    { type: "Fonctions politiques", label: "Fonctions politiques" },
    { type: "Autres fonctions", label: "Autres fonctions" },
  ];
  const linesByTypes = groupBy(CV, (position: ElementDeCarriere) => position.typeEvenementCarriere);
  return (
    <div className="curriculum-vitae">
      {sections.map(
        (section) =>
          linesByTypes[section.type] && (
            <div key={section.type}>
              <h5>
                <span className="highlight">{section.label}</span>
              </h5>
              <ul className="list-unstyled">
                {linesByTypes[section.type].map((position: ElementDeCarriere) => (
                  <li key={position.id}>
                    <CVLine position={position} />
                  </li>
                ))}
              </ul>
            </div>
          ),
      )}
    </div>
  );
};
