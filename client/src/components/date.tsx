import { DateHeurist } from "Heurist";
import { isDate, memoize } from "lodash";

export const DateComponent = ({ date, prefix }: { date?: Date; prefix: string }) =>
  date ? (
    <>
      {prefix ? prefix + " " : ""}
      <span className="value-inline">{new Intl.DateTimeFormat("fr-FR").format(date)}</span>
    </>
  ) : null;

export const YearRangeComponent = ({
  dateRange,
  prefix,
  glue,
  withoutHighlight,
}: {
  dateRange: [Date, Date];
  prefix?: string;
  glue?: string;
  withoutHighlight?: boolean;
}) =>
  dateRange && dateRange.every((d) => isDate(d)) ? (
    <>
      {dateRange[0].getFullYear() === dateRange[1].getFullYear() ? (
        <>
          {prefix !== undefined ? prefix : "en "}
          <span className={withoutHighlight ? "" : "value-inline"}>{dateRange[1].getFullYear()}</span>
        </>
      ) : (
        <>
          {prefix !== undefined ? prefix : "entre "}
          <span className={withoutHighlight ? "" : "value-inline"}>{dateRange[0].getFullYear()}</span>
          {glue ? ` ${glue} ` : " et "}
          <span className={withoutHighlight ? "" : "value-inline"}>{dateRange[1].getFullYear()}</span>
        </>
      )}
    </>
  ) : null;

interface Props {
  prefix?: string;
  suffix?: string;
  date?: DateHeurist;
  onlyYear?: boolean;
}
export const dateHeuristLabel = memoize((props: Props): string => {
  const { date, onlyYear, prefix, suffix } = props;

  let toDisplay = "";
  if (date && date.date) {
    if (date.date.getFullYear() !== 9999) {
      switch (date.type) {
        // Register in heurist like 1900-1910, so we display it like that
        case "year-range":
          const min = date.range.gte.getFullYear();
          const max = date.range.lte.getFullYear();
          if (min !== max) toDisplay = `en ${min} - ${max}`;
          else toDisplay = `en ${min}`;
          break;
        // Register in heurist as a date with borders (TQP & TAQ)
        // so it's an approximation
        case "date-range":
          const min0 = date.range.gte.getFullYear();
          const max0 = date.range.lte.getFullYear();
          if (min0 !== max0) toDisplay = `entre ${min0} et ${max0}`;
          else toDisplay = `en ${min0}`;
          break;
        // Register in heurist as an approximate date (ie. date + uncertainty)
        // We just handle the case with year approximation
        case "date-approximate":
          const min1 = date.range.gte.getFullYear();
          const max1 = date.range.lte.getFullYear();
          toDisplay = `à une date incertaine entre ${min1} et ${max1}`;
          break;
        // Register in heurist like a real date or sometimes with just the year
        case "date":
          if (onlyYear && onlyYear === true) toDisplay = `en ${date.date.getFullYear()}`;
          else {
            if (
              date.range.gte.getMonth() === 0 &&
              date.range.lte.getMonth() === 11 &&
              date.range.gte.getDate() === 1 &&
              date.range.lte.getDate() === 31
            ) {
              toDisplay = `en ${date.date.getFullYear()}`;
            } else {
              toDisplay = `le ${new Intl.DateTimeFormat("fr-FR").format(date.date)}`;
            }
          }
          break;
      }
    } else {
      toDisplay = "( sans date )";
    }
  }
  return ` ${toDisplay && prefix ? `${prefix} ` : ""}${toDisplay}${toDisplay && suffix ? ` ${suffix} ` : ""}`;
});

export const DateHeuristComponent: React.FC<Props> = (props: Props) => {
  return <>{dateHeuristLabel(props)}</>;
};

export function dateHeuristYearRange(date?: DateHeurist): [number, number] | undefined {
  let year: undefined | [number, number] = undefined;
  if (date && date.date) {
    if (date && date.date.getFullYear() !== 9999) {
      switch (date.type) {
        // Register in heurist like 1900-1910, so we display it like that
        case "date-range":
        case "year-range":
        case "date-approximate":
          const min = date.range.gte.getFullYear();
          const max = date.range.lte.getFullYear();
          year = [min, max];
          break;
        // Register in heurist like a real date or sometimes with just the year
        case "date":
          year = [date.date.getFullYear(), date.date.getFullYear()];
          break;
      }
    }
  }
  return year;
}
