import { last, sum } from "lodash";
import { FC } from "react";
import { Link } from "react-router-dom";

import { Organisation_genealogy } from "../types";
import { genealogyItemDatesLabel } from "./editor-genealogy";

export const EditorGenealogyResultComponent: FC<{
  editorGenealogy: Organisation_genealogy;
}> = ({ editorGenealogy }) => {
  return (
    <>
      <h5>
        <i className="fas fa-building mr-1" /> Généalogie de "{last(editorGenealogy.nodes)?.nomEntiteMorale}"
      </h5>
      <p>
        {sum(editorGenealogy.nodes.map((e) => +e.nb_edited_publications))} publications - {editorGenealogy.nodes.length}{" "}
        éditeurs
      </p>
      <ul>
        {editorGenealogy.nodes.map((editor) => {
          // const startYear = editor.dateDeDebut ? editor.dateDeDebut.range.gte.getFullYear() : null;
          // const endYear = editor.dateDeFin ? editor.dateDeFin.range.lte.getFullYear() : null;

          return (
            <li key={editor.id}>
              {genealogyItemDatesLabel(editor)} :{" "}
              {/* {startYear || endYear ? `${[startYear || "", endYear || ""].join(" > ")} :` : ""} */}
              <Link to={`/editeur/${editor.id}/genealogie`}>{editor.nomEntiteMorale}</Link>
            </li>
          );
        })}
      </ul>
    </>
  );
};
