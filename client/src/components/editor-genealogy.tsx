import { DateHeurist, Genealogy, Organisation } from "Heurist";
import chroma from "chroma-js";
import { flatMap, keyBy, map, mapValues, max, min, range, sortBy, uniq, values } from "lodash";
import { CSSProperties } from "react";
import { Link } from "react-router-dom";
import config from "../config";
import { TimelineDataPoint } from "./books-timeline";
import withSize, { SizeState } from "./with-size";

interface EditorTimeBoxProps {
  editor: Organisation;
  width: number;
  yearWidth: number;
  focused: boolean;
  position: number;
  maxBookValue: number; // use to normalize opacity on genealogy max value
  booksTimelineData: TimelineDataPoint[];
}

const DEATHBOX_WIDTH = 20;
const BOX_HEIGHT = 35;
const BOX_MARGIN_BOTTOM = 10;
const BOX_MARGIN_LEFT = 12;
const BOX_BORDER_WIDTH = 3;
const TOTAL_BOX_HEIGHT = BOX_HEIGHT + BOX_MARGIN_BOTTOM + 2 * BOX_BORDER_WIDTH;

interface BoxBorder {
  year: number;
  open: boolean;
  approxYear: number | null;
  approxOpen: boolean;
}
enum BorderType {
  START = "start",
  END = "end",
}
enum DeathType {
  UNKNOWN = "unknown",
  MERGED = "merged",
  OTHER = "other",
}
const boxBorder = (date: DateHeurist | undefined, type: BorderType): BoxBorder => {
  // dates => x position
  let year = null;
  let open: boolean = false;
  let approxYear = null;
  let approxOpen: boolean = false;

  if (date) {
    if (date.type === "date") year = date.date.getFullYear();
    else {
      year = type === BorderType.END ? date.range.gte.getFullYear() : date.range.lte.getFullYear();
      approxYear = type === BorderType.END ? date.range.lte.getFullYear() : date.range.gte.getFullYear();
    }
  }

  // fall back to min/maxYear
  if (!year || year < config.minYear || year > config.maxYear) {
    // if fallback remember to let the box open
    open = true;
    year = type === BorderType.START ? config.minYear : config.maxYear;
  }
  if (approxYear && (approxYear < config.minYear || approxYear > config.maxYear)) {
    // if fallback remember to let the box open
    approxOpen = true;
    approxYear = type === BorderType.START ? config.minYear : config.maxYear;
  }
  return { year, open, approxYear, approxOpen };
};

export const genealogyItemDatesLabel = (editor: Organisation) => {
  const dateLabel = (date: DateHeurist | undefined) => {
    if (date) {
      if (date.type === "date") return date.date.getFullYear();
      else return `${date.range.gte.getFullYear()}~${date.range.lte.getFullYear()}`;
    } else return "";
  };
  return `${dateLabel(editor.dateDeDebut)} - ${dateLabel(editor.dateDeFin)}`;
};

const EditorTimeBox: React.FC<EditorTimeBoxProps> = (props: EditorTimeBoxProps) => {
  const { editor, width, yearWidth, focused, position, booksTimelineData, maxBookValue } = props;
  const yearToX = (year: number) => (year - config.minYear) * yearWidth;

  // prepare data for left/right borders rendering
  const startBorder: BoxBorder = boxBorder(editor.dateDeDebut, BorderType.START);
  let endBorder: BoxBorder = boxBorder(editor.dateDeFin, BorderType.END);
  // no end date but have descendant => use descendant start date
  // if (!editor.dateDeFin) {
  //   //TODO: next editor resolution should be done by parent component
  //   const next = editor.structuresLiees?.find(
  //     (l) =>
  //       (l.direction === "out" && l.relation === "Devient") || (l.direction === "in" && l.relation === "Fusion de"),
  //   );
  //   if (next && next.other && next.other.dateDeDebut) {
  //     // consider only the average date of the next
  //     // stops as open
  //     const stopYear = next.other.dateDeDebut.date.getFullYear() - 1;
  //     endBorder = { year: stopYear, open: true, approxYear: null, approxOpen: false };
  //   }
  // }

  // MAIN ITEMS
  const top = position * TOTAL_BOX_HEIGHT;
  const lineStyle = { top };
  const boxStyle: CSSProperties = {
    top: 0,
    left: yearToX(startBorder.year) - +!startBorder.open * BOX_BORDER_WIDTH,
    width: (endBorder.year - startBorder.year + 1) * yearWidth,
    height: BOX_HEIGHT,
    borderWidth: BOX_BORDER_WIDTH,
    marginBottom: BOX_MARGIN_BOTTOM,
  };
  let boxClassName = "box";
  if (startBorder.approxYear || startBorder.open) boxClassName += " open-start";
  if (endBorder.approxYear || endBorder.open) boxClassName += " open-end";

  // LABEL
  const minBookYear = min(booksTimelineData?.map((dp) => +dp.year)) || Infinity;
  const labelStyle: CSSProperties = {
    top: 0,
    left: Math.min(width / 2 - window.innerWidth / 2 + 50, 0),
    right:
      // labels are placed just before the first graphic element on the line + margin
      -yearToX(min([startBorder.approxYear || Infinity, startBorder.year, minBookYear]) || config.minYear) +
      BOX_MARGIN_LEFT,
    height: BOX_HEIGHT,
    lineHeight: "1.2em",
  };

  // APPROX BOXES
  const startApproxStyle = startBorder.approxYear
    ? {
        top: 0,
        left: yearToX(startBorder.approxYear),
        width: (startBorder.year - startBorder.approxYear) * yearWidth,
        height: BOX_HEIGHT,
        borderWidth: BOX_BORDER_WIDTH,
        borderRight: 0,
      }
    : {};
  const endApproxStyle = endBorder.approxYear
    ? {
        top: 0,
        left: yearToX(endBorder.year),
        width: (endBorder.approxYear - endBorder.year) * yearWidth,
        height: BOX_HEIGHT,
        borderWidth: BOX_BORDER_WIDTH,
        borderLeft: 0,
      }
    : {};

  // HEATMAP ITEM
  const colorScale = chroma.scale(["white", config.colors.primaryColor]);

  // Disparition & fusion cases
  let deathType: DeathType = DeathType.UNKNOWN;
  const deathStyle: CSSProperties = {
    width: DEATHBOX_WIDTH,
    height: BOX_HEIGHT + 2 * BOX_BORDER_WIDTH,
    lineHeight: BOX_HEIGHT + 2 * BOX_BORDER_WIDTH + "px",
    left: yearToX((max([endBorder.year, endBorder.approxYear || 0]) || config.maxYear) + 1) + BOX_BORDER_WIDTH,
  };
  if (editor.structuresLiees?.find((l) => l.relation === "Fusionne avec")) {
    deathType = DeathType.MERGED;
  } else if (editor.natureDeLaDisparition) {
    deathType = DeathType.OTHER;
  }

  return (
    <div className={`editor-timeline ${focused ? "focused" : ""}`} style={lineStyle}>
      {/*  Label */}
      <div style={labelStyle} className="label ellipsis">
        <Link to={`/editeur/${editor.id}`} title={`Éditeur "${editor.nomEntiteMorale}"`}>
          {editor.nomEntiteMorale}
        </Link>
        <br />
        {genealogyItemDatesLabel(editor)}
      </div>
      {/* Approximative start */}
      {startBorder.approxYear && (
        <div style={startApproxStyle} className={`approx-box ${startBorder.approxOpen ? "open-start" : ""}`} />
      )}
      {/* Main box */}
      <div style={boxStyle} className={boxClassName} />
      {/* Book heatmap */}
      {booksTimelineData &&
        booksTimelineData.map(
          (dataPoint) =>
            dataPoint.value !== 0 && (
              <Link
                key={dataPoint.year}
                to={`/?t=book&q=*&editeurs.nomEntiteMorale=${encodeURIComponent(
                  editor?.nomEntiteMorale || "",
                )}&dateEdition.range.min=${encodeURIComponent(
                  dataPoint.year,
                )}&dateEdition.range.max=${encodeURIComponent(dataPoint.year)}`}
                title={`${dataPoint.year}: ${dataPoint.value} publication${dataPoint.value > 1 ? "s" : ""}`}
              >
                <div
                  className="book-heatmap-item"
                  style={{
                    top: BOX_BORDER_WIDTH,
                    left: yearToX(+dataPoint.year),
                    width: yearWidth,
                    height: BOX_HEIGHT,
                    backgroundColor: colorScale(dataPoint.value / (maxBookValue > 0 ? maxBookValue : 1)).hex(),
                  }}
                />
              </Link>
            ),
        )}

      {/* Approximative end */}
      {endBorder.approxYear && (
        <div style={endApproxStyle} className={`approx-box ${endBorder.approxOpen ? "open-end" : ""}`} />
      )}

      {/* Disparition // fusion cases */}
      {deathType !== DeathType.UNKNOWN && (
        <div className="death-indicator" style={deathStyle}>
          <i className="fas fa-info" />
          <span className="label">{editor.natureDeLaDisparition || "Fusion"}</span>
        </div>
      )}
    </div>
  );
};

interface Props {
  editorGenealogy: Genealogy<Organisation>;
  focusedEditorId: string;
  booksHistogramDataByEditor: {
    [key: string]: TimelineDataPoint[];
  };
}

const Arrows: React.FC<{
  arrows: { from: string[]; to: string[] }[];
  editors: Record<string, Organisation>;
  positions: Record<string, number>;
  books: Record<string, TimelineDataPoint[]>;
  width: number;
}> = ({ arrows, editors, positions, books, width }) => {
  const COLOR = "#666";
  // Size:
  const yearWidth = width / (config.maxYear - config.minYear);
  const positionToY = (position: number) => (position + 0.5) * TOTAL_BOX_HEIGHT;
  const LEFT_PADDING = 6;
  const RIGHT_PADDING = 20;

  // Helpers to find arrows positions:
  const areIntoMerging = uniq(arrows.filter(({ from }) => from.length > 1).flatMap(({ from }) => from));
  const endYearXs = mapValues(editors, (editor) => {
    const box = boxBorder(editor.dateDeFin, BorderType.END);
    const hasDeathBox = areIntoMerging.includes(editor.id) || editor.natureDeLaDisparition;

    return (
      (Math.max(box.year, box.approxYear || 0) + 1 - config.minYear) * yearWidth +
      (hasDeathBox ? BOX_BORDER_WIDTH + DEATHBOX_WIDTH : 0)
    );
  });
  const startYearXs = mapValues(editors, (editor) => {
    const box = boxBorder(editor.dateDeDebut, BorderType.START);
    return (
      (Math.min(box.approxYear || Infinity, box.year, min(books[editor.id]?.map((dp) => +dp.year)) || Infinity) -
        config.minYear) *
        yearWidth -
      +!box.open * BOX_BORDER_WIDTH
    );
  });

  return (
    <svg
      className="arrows-container"
      width={width}
      height="100%"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      {arrows.map(({ from, to }, i) => {
        const lastFromPosition = positions[sortBy(from, (id) => -positions[id])[0]];
        const firstToPosition = positions[sortBy(to, (id) => +positions[id])[0]];
        const maxFromX = Math.max(...from.map((id) => endYearXs[id])) + RIGHT_PADDING;
        const minToX = Math.min(...to.map((id) => startYearXs[id])) - LEFT_PADDING;
        const joiningY =
          Math.floor(positionToY(firstToPosition) - 0.5 * TOTAL_BOX_HEIGHT - BOX_MARGIN_BOTTOM / 2) - 0.5;

        return (
          <g key={i}>
            {/* Outgoing arrows from "from" editors */}
            {from.map((id) => {
              const position = positions[id];
              const x = endYearXs[id] - 5;
              const y = positionToY(position);

              return (
                <path
                  key={id}
                  stroke={COLOR}
                  fill="transparent"
                  d={`M${x},${y} L${maxFromX},${y} L${maxFromX},${positionToY(lastFromPosition)}`}
                />
              );
            })}

            {/* Joining arrow */}
            <path
              stroke={COLOR}
              fill="transparent"
              d={[
                `M${maxFromX},${positionToY(lastFromPosition)}`,
                `L${maxFromX},${joiningY}`,
                `L${minToX},${joiningY}`,
                `L${minToX},${positionToY(firstToPosition)}`,
              ].join(" ")}
            />

            {/* Ingoing arrows to "to" editors */}
            {to.map((id) => {
              const position = positions[id];
              const x = startYearXs[id] + 5;
              const y = positionToY(position);

              return (
                <path
                  key={id}
                  stroke={COLOR}
                  fill="transparent"
                  d={`M${minToX},${positionToY(firstToPosition)} L${minToX},${y} L${x},${y}`}
                />
              );
            })}
          </g>
        );
      })}
    </svg>
  );
};

const EditorGenealogyWithSize: React.FC<Props & SizeState> = (props) => {
  const { width, editorGenealogy, focusedEditorId, booksHistogramDataByEditor } = props;

  // todo: build network to order lines

  const yearWidth = width / (config.maxYear - config.minYear);
  const maxBookNumber = max(flatMap(values(booksHistogramDataByEditor), (b) => b.map((dp) => dp.value)));
  const editors: Record<string, Organisation> = keyBy(editorGenealogy.nodes, "id");
  const editorsPositions = editorGenealogy.nodes.reduce(
    (iter, editor, i) => ({
      ...iter,
      [editor.id]: i,
    }),
    {},
  );
  const mergings = editorGenealogy.edges
    .filter((edge) => edge.type !== "Fusionne avec")
    .reduce<Record<string, string[]>>(
      (iter, { from, to }) => ({
        ...iter,
        [to]: (iter[to] || []).concat([from]),
      }),
      {},
    );

  // Compute x step value
  const xCaptionStep =
    [1, 2, 5, 10, 20].find((n) => {
      const labelWidth = (width / (config.maxYear + 1 - config.minYear)) * n;
      return labelWidth >= 25;
    }) || 50;
  const colorScale = chroma.scale(["white", config.colors.primaryColor]);
  return (
    <>
      <ul className="editor-genealogy-caption">
        {range(config.minYear, config.maxYear + 1)
          .filter((y) => y % xCaptionStep === 0)
          .map((year) => (
            <li
              key={year}
              style={{
                left: (year - config.minYear) * yearWidth,
                width: yearWidth,
              }}
            >
              <span>{year}</span>
            </li>
          ))}
      </ul>
      <div
        className="editor-genealogy"
        style={{ height: editorGenealogy.nodes.length * (BOX_HEIGHT + 2 * BOX_BORDER_WIDTH + BOX_MARGIN_BOTTOM) }}
      >
        <Arrows
          arrows={map(mergings, (from, to) => ({ from, to: [to] }))}
          editors={editors}
          positions={editorsPositions}
          books={booksHistogramDataByEditor}
          width={width}
        />
        {editorGenealogy.nodes.map((e, i) => (
          <EditorTimeBox
            key={e.id}
            editor={e}
            position={i}
            width={width}
            yearWidth={yearWidth}
            focused={focusedEditorId === e.id}
            maxBookValue={maxBookNumber || 0}
            booksTimelineData={booksHistogramDataByEditor[e.id]}
          />
        ))}
      </div>
      <h5>Légende</h5>
      <div className="editor-timeline-legend">
        <div className="column">
          {/* column one */}
          <div className="legend-item">
            <div className="box" style={{ borderWidth: BOX_BORDER_WIDTH }}></div>
            <div className="label">Dates connues</div>
          </div>
          <div className="legend-item">
            <div className="approx-box open-end" style={{ borderWidth: BOX_BORDER_WIDTH }}></div>
            <div className="box open-start open-end" style={{ borderWidth: BOX_BORDER_WIDTH }}></div>
            <div className="approx-box open-start" style={{ borderWidth: BOX_BORDER_WIDTH }}></div>
            <div className="label">Dates incertaines</div>
          </div>
          <div className="legend-item">
            <div className="box open-start open-end" style={{ borderWidth: BOX_BORDER_WIDTH }}></div>
            <div className="label">Pas de date</div>
          </div>
        </div>
        <div className="column">
          {/* column two */}

          <div className="legend-item">
            <div className="box" style={{ borderWidth: BOX_BORDER_WIDTH, display: "flex" }}>
              <div
                className="book-heatmap-item"
                style={{ width: yearWidth, height: "100%", backgroundColor: colorScale(0).hex() }}
              ></div>
              <div
                className="book-heatmap-item"
                style={{ width: yearWidth, height: "100%", backgroundColor: colorScale(0.25).hex() }}
              ></div>
              <div
                className="book-heatmap-item"
                style={{ width: yearWidth, height: "100%", backgroundColor: colorScale(0.5).hex() }}
              ></div>
              <div
                className="book-heatmap-item"
                style={{ width: yearWidth, height: "100%", backgroundColor: colorScale(1).hex() }}
              ></div>
            </div>
            <div className="label">Nombre de productions pédagogiques</div>
          </div>
          <div className="legend-item">
            <div className="death-indicator" style={{ width: "20px", lineHeight: "100%" }}>
              <i className="fas fa-info"></i>
              <span className="label">Rachat</span>
            </div>
            <div className="label">Nature de la disparition</div>
          </div>
        </div>
      </div>
    </>
  );
};
export const EditorGenealogyComponent = withSize<Props>(EditorGenealogyWithSize);
