import { Organisation } from "Heurist";
import { identity } from "lodash";
import { FC } from "react";
import { Link } from "react-router-dom";

import { BooksTimeline, TimelineDataPoint } from "./books-timeline";
import { Loader } from "./loader";

interface EditorOrgProps {
  editor: Organisation;
  firstLevel?: boolean;
  showBooksHistogram?: boolean;
  booksHistogramData?: TimelineDataPoint[];
}

const _OrganisationComponent: FC<EditorOrgProps & { as: "editor" | "author" }> = ({
  editor,
  firstLevel,
  booksHistogramData,
  showBooksHistogram,
  as,
}) => {
  const startYear = editor.dateDeDebut ? editor.dateDeDebut.range.gte.getFullYear() : null;
  const endYear = editor.dateDeFin ? editor.dateDeFin.range.lte.getFullYear() : null;

  const TitleTag = firstLevel ? "h5" : "h6";
  const nb_publications = as === "editor" ? editor.nb_edited_publications : editor.nb_authored_publications;
  return (
    <>
      <TitleTag>
        <i className="fas fa-building mr-1" />

        <Link to={`/${as === "editor" ? "editeur" : "auteur-org"}/${editor.id}`}>{editor.nomEntiteMorale}</Link>
      </TitleTag>
      {firstLevel && editor.varianteDuNom && as === "editor" && (
        <p>
          Également{" "}
          <span className="values-list-coma-and">
            {editor.varianteDuNom.map((str, i) => (
              <span key={i}>
                <strong>{str}</strong>
              </span>
            ))}
          </span>
        </p>
      )}
      {(startYear || endYear) && (
        <p>
          {[startYear && `Créée en ${startYear}`, endYear && `Disparue en ${endYear}`].filter(identity).join(" - ")}
        </p>
      )}
      {showBooksHistogram && booksHistogramData && (
        <>
          <div>
            A édité{" "}
            <span className="value">
              {nb_publications} livre{nb_publications || 0 > 1 ? "s" : ""}
            </span>{" "}
            de ce corpus
          </div>
          {booksHistogramData.length > 0 && (
            <BooksTimeline
              booksHistogram={booksHistogramData}
              showMissing={false}
              getBarURL={({ year }) =>
                `/?t=book&q=*&editeurs.nomEntiteMorale=${encodeURIComponent(
                  editor?.nomEntiteMorale || "",
                )}&dateEdition.range.min=${encodeURIComponent(year)}&dateEdition.range.max=${encodeURIComponent(year)}`
              }
              newTab
            />
          )}
        </>
      )}
      {showBooksHistogram && !booksHistogramData && <Loader tag="h6" />}
    </>
  );
};

export const EditorComponent: FC<EditorOrgProps> = (props) => _OrganisationComponent({ ...props, as: "editor" });
export const AuthorOrgComponent: FC<EditorOrgProps> = (props) => _OrganisationComponent({ ...props, as: "author" });
