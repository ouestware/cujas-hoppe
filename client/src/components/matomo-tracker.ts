import { useMatomo } from "@datapunt/matomo-tracker-react";
import { useEffect } from "react";
import { useLocation } from "react-router";

export const MatomoTracker: React.FC = () => {
  const { trackPageView } = useMatomo();
  const location = useLocation();

  useEffect(() => {
    trackPageView({});
  }, [trackPageView, location]);

  return null;
};
