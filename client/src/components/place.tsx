import { Ville } from "Heurist";

export const PlaceComponent = ({ place: { ville: v, pays: p }, prefix }: { place: Ville; prefix?: string }) =>
  v || p ? (
    <>
      {prefix ? prefix + " " : ""}
      <span className="value-inline">
        {v || <i>ville non connue</i>}
        {p && ` (${p})`}
      </span>
    </>
  ) : null;
