import { useSearchExportToCSV } from "../hooks/search-export";
import { ESSearchQueryContext } from "../types";

// Props definition
interface Props {
  filename: string;
  query: ESSearchQueryContext;
  title: string;
}

export const SearchExportButton: React.FC<Props> = (props: Props) => {
  const { filename, query, title } = props;

  const [exportFn, { loading, progress, error }] = useSearchExportToCSV();

  return (
    <button
      title={error ? error.message : title}
      className={`export-btn ${loading ? "loading" : ""} ${error ? "error" : ""}`}
      style={loading ? { opacity: progress / 100 } : {}}
      onClick={() => exportFn(query, filename)}
    >
      <i className="fas fa-download"></i>
    </button>
  );
};
