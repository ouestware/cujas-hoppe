import { Property } from "csstype";
import Graph from "graphology";
import forceAtlas2 from "graphology-layout-forceatlas2";
import FA2LayoutSupervisor from "graphology-layout-forceatlas2/worker";
import { Attributes } from "graphology-types";
import { Component, createRef, RefObject } from "react";
import Select from "react-select";
import { Sigma } from "sigma";

import drawHover from "sigma/rendering/canvas/hover";
import drawLabel from "sigma/rendering/canvas/label";
import { Settings } from "sigma/settings";
import { SigmaNodeEventPayload } from "sigma/sigma";
import { Coordinates, EdgeDisplayData, NodeDisplayData } from "sigma/types";

export type CaptionElement = {
  type: string;
  color: string;
  count: number;
};
export type SigmaProps = {
  graph: Graph;
  selectedNode: string | null;
  setSelectedNode: Function;
  captionElements?: CaptionElement[];
  options?: {};
};
type SigmaState = {
  isFA2Running: boolean;
  search: string;
  searchSelectedOption?: { id: string; label: string } | null;
  autoStopTimeoutID?: number;
  draggedNode?: string;
  cursor?: Property.Cursor;
};

const HIDDEN_COLOR = "#b0b0b0";
const DURATION = 200;

function matchSearch(nodePropsOrOption: any, search: string): boolean {
  return nodePropsOrOption && nodePropsOrOption.label.toUpperCase().indexOf(search.toUpperCase()) > -1;
}

export class SigmaComponent extends Component<SigmaProps, SigmaState> {
  // Local state:
  state: SigmaState = {
    isFA2Running: false,
    search: "",
  };

  // Sigma related instances:
  root: RefObject<HTMLDivElement> = createRef<HTMLDivElement>();
  sigmaRoot: RefObject<HTMLDivElement> = createRef<HTMLDivElement>();
  sigma?: Sigma;
  fa2?: FA2LayoutSupervisor;
  onFullScreenChange = () => this.forceUpdate();

  // FA2 management:
  initFA2() {
    if (this.fa2) {
      this.fa2.kill();
    }

    this.fa2 = new FA2LayoutSupervisor(this.props.graph, { settings: forceAtlas2.inferSettings(this.props.graph) });
  }
  stopFA2() {
    if (!this.fa2) return;
    this.setState({ isFA2Running: false });
    this.fa2.stop();
    this.clearFA2Timeout();
  }
  startFA2() {
    if (!this.fa2) {
      return;
    }

    this.setState({ isFA2Running: true });
    this.fa2.start();
    this.clearFA2Timeout();
  }
  toggleFA2() {
    if (this.state.isFA2Running) {
      this.stopFA2();
    } else {
      this.startFA2();
    }
  }
  clearFA2Timeout() {
    if (typeof this.state.autoStopTimeoutID === "number") {
      window.clearTimeout(this.state.autoStopTimeoutID);
      this.setState({ autoStopTimeoutID: undefined });
    }
  }

  // Zoom management:
  zoom(ratio?: number) {
    if (!this.sigma) return;

    if (!ratio) {
      this.sigma.getCamera().animatedReset({ duration: DURATION });
    } else if (ratio > 0) {
      this.sigma.getCamera().animatedZoom({ duration: DURATION, factor: 1.5 });
    } else if (ratio < 0) {
      this.sigma.getCamera().animatedUnzoom({ duration: DURATION, factor: 1.5 });
    }
  }

  // Fullscreen management:
  toggleFullScreen() {
    if (document.fullscreenElement) {
      document.exitFullscreen();
    } else if (this.root.current) {
      this.root.current.requestFullscreen();
    }
  }
  isFullScreen() {
    return !!document.fullscreenElement;
  }

  // Sigma lifecycle:
  killSigma() {
    if (this.sigma) {
      this.sigma.kill();
      this.sigma = undefined;
    }
  }
  initSigma() {
    if (!this.sigmaRoot.current) return;
    if (this.state.isFA2Running) this.stopFA2();
    if (this.sigma) this.killSigma();

    this.sigma = new Sigma(this.props.graph, this.sigmaRoot.current, {
      labelFont: "Helvetica, sans-serif",
      nodeReducer: this.nodeReducer.bind(this),
      edgeReducer: this.edgeReducer.bind(this),
      labelRenderer: this.labelRenderer.bind(this),
      hoverRenderer: this.hoverRenderer.bind(this),
    });

    this.sigma.on("clickNode", ({ node }) => {
      this.props.setSelectedNode((selected: string) => {
        return selected === node ? null : node;
      });
      this.setState({ searchSelectedOption: null });
    });
    this.sigma.on("clickStage", () => {
      this.props.setSelectedNode(null);
      this.setState({ searchSelectedOption: null });
    });

    this.initFA2();

    if (this.props.graph.order > 1) {
      setTimeout(() => {
        this.startFA2();
        this.setState({
          autoStopTimeoutID: window.setTimeout(() => this.stopFA2(), this.props.graph.order < 500 ? 5000 : 10000),
        });
      }, 0);
    }

    // Deal with nodes drag and dropping:
    const mouseCaptor = this.sigma.getMouseCaptor();
    this.sigma.on("enterNode", this.enterNode.bind(this));
    this.sigma.on("leaveNode", this.leaveNode.bind(this));
    this.sigma.on("downNode", this.downNode.bind(this));
    mouseCaptor.on("mouseup", this.mouseUp.bind(this));
    mouseCaptor.on("mousemove", this.mouseMove.bind(this));
  }

  // Drag and drop management:
  enterNode() {
    if (this.state.isFA2Running || typeof this.state.draggedNode === "string") return;
    this.setState({ cursor: "grab" });
  }
  leaveNode() {
    if (this.state.isFA2Running || typeof this.state.draggedNode === "string") return;
    this.setState({ cursor: undefined });
  }
  downNode(e: SigmaNodeEventPayload) {
    if (this.state.isFA2Running) return;

    this.setState({ cursor: "grabbing", draggedNode: e.node });
    this.props.graph.setNodeAttribute(e.node, "fixed", true);
    this.sigma?.getCamera().disable();
  }
  mouseUp() {
    this.setState({ draggedNode: undefined, cursor: undefined });
    this.sigma?.getCamera().enable();
  }
  mouseMove(e: { x: number; y: number }) {
    if (!this.sigma || typeof this.state.draggedNode !== "string") return;

    // Get new position of node
    const pos = this.sigma.viewportToGraph(e);

    this.props.graph.setNodeAttribute(this.state.draggedNode, "x", pos.x);
    this.props.graph.setNodeAttribute(this.state.draggedNode, "y", pos.y);
  }

  setSearch(value: string): void {
    this.setState({ search: value });
    this.sigma?.refresh();
  }

  panToNode(id: string): void {
    if (!this.sigma) return;

    this.sigma
      .getCamera()
      .animate(this.sigma.getNodeDisplayData(id) as Coordinates, { easing: "linear", duration: 500 });
  }

  computeSearchOptions(): Array<{ id: string; label: string }> {
    const options = this.props.graph
      .nodes()
      .map((id) => {
        const props = this.props.graph.getNodeAttributes(id);
        if (props.hidden === true) return null;
        else return { id: id, label: props.label };
      })
      .filter((opt) => matchSearch(opt, this.state.search)) as Array<{
      id: string;
      label: string;
    }>;
    options.sort((a, b) => (a.label.toUpperCase() > b.label.toUpperCase() ? 1 : -1));
    return options;
  }

  // React lifecycle:
  componentDidMount() {
    this.initSigma();
    this.setState({ searchSelectedOption: null });
    this.root.current!.addEventListener("fullscreenchange", this.onFullScreenChange);
  }

  componentWillUnmount() {
    this.stopFA2();
    this.killSigma();
    this.root.current!.removeEventListener("fullscreenchange", this.onFullScreenChange);
  }
  componentDidUpdate(prevProps: SigmaProps) {
    // if graph has changed
    if (prevProps.graph !== this.props.graph) {
      if (this.props.graph) {
        this.initSigma();
      } else {
        this.killSigma();
      }
    }
  }

  nodeReducer(node: string, data: Attributes): Partial<NodeDisplayData> {
    const newData = { ...data };
    const graph = this.props.graph;
    const nbBooks = this.props.graph.getNodeAttribute(node, "nbBooks") as number;

    if (
      this.props.selectedNode &&
      this.props.selectedNode !== node &&
      !graph.edge(node, this.props.selectedNode) &&
      !graph.edge(this.props.selectedNode, node)
    ) {
      newData.label = "";
      newData.color = HIDDEN_COLOR;
    } else {
      newData.label = data.label;
    }

    newData.highlighted = this.props.selectedNode === node;

    if (nbBooks === 0) {
      newData.label = "";
      newData.hidden = true;
    } else {
      newData.hidden = false;
      newData.label = data.label;
    }

    if (nbBooks > 0) newData.size = Math.max(2, Math.log(nbBooks) * 2);

    // handle search node
    if (this.state.search && this.state.search.length > 0) {
      if (!matchSearch(newData, this.state.search)) {
        newData.label = "";
        newData.color = HIDDEN_COLOR;
      }
    }

    return { ...newData, type: "circle" };
  }
  edgeReducer(edge: string, data: Attributes): Partial<EdgeDisplayData> {
    const newData = { ...data };
    const nbBooks = this.props.graph.getEdgeAttribute(edge, "nbBooks") as number;

    newData.hidden = !!(
      this.props.selectedNode && !this.props.graph.extremities(edge).includes(this.props.selectedNode)
    );
    if (nbBooks > 0) newData.size = Math.log(nbBooks);

    return newData;
  }
  labelRenderer(context: CanvasRenderingContext2D, nodeData: any, settings: Settings) {
    if (nodeData.label) drawLabel(context, nodeData, settings);
  }
  hoverRenderer(context: CanvasRenderingContext2D, nodeData: any, settings: Settings) {
    drawHover(context, { ...nodeData, label: this.props.graph.getNodeAttribute(nodeData.key, "label") }, settings);
  }

  render() {
    const isFullScreen = this.isFullScreen();

    return (
      <div className="sigma" ref={this.root}>
        <div className="sigma-container" ref={this.sigmaRoot} style={{ cursor: this.state.cursor }} />
        <div className="controls">
          <div className="search">
            <Select
              isClearable
              className="react-select"
              classNamePrefix="react-select"
              placeholder="Sélectionner un nœud..."
              options={this.computeSearchOptions()}
              filterOption={() => true}
              value={this.state.searchSelectedOption}
              inputValue={this.state.search}
              noOptionsMessage={({ inputValue }) => {
                if (!inputValue) return null;
                return "Aucun résultat";
              }}
              onInputChange={(inputValue) => {
                this.setSearch(inputValue);
              }}
              onChange={(value) => {
                const id = value && (value as { id: string; label: string }).id;
                if (id) {
                  this.props.setSelectedNode(id);
                  this.setState({ searchSelectedOption: value as { id: string; label: string } });
                  this.panToNode(id);
                } else {
                  this.setState({ searchSelectedOption: null });
                  this.props.setSelectedNode(null);
                }
              }}
            />
          </div>
          <button
            className="btn btn-light"
            onClick={() => this.toggleFA2()}
            title={this.state.isFA2Running ? "Stopper l'animation" : "Démarrer l'animation"}
          >
            {this.state.autoStopTimeoutID && (
              <span>
                <i className="fas fa-spinner fa-pulse fa-3x" />
              </span>
            )}
            {this.state.isFA2Running ? <i className="fas fa-stop" /> : <i className="fas fa-play" />}
          </button>
          <button className="btn btn-light" onClick={() => this.zoom(1)} title="Zoomer">
            <i className="fas fa-search-plus" />
          </button>
          <button className="btn btn-light" onClick={() => this.zoom(-1)} title="Dézoomer">
            <i className="fas fa-search-minus" />
          </button>
          <button className="btn btn-light" onClick={() => this.zoom()} title="Recentrer">
            <i className="far fa-dot-circle" />
          </button>
          <button
            className="btn btn-light"
            onClick={() => this.toggleFullScreen()}
            title={isFullScreen ? "Quitter le plein écran" : "Passer en plein écran"}
          >
            <i className={`fas ${isFullScreen ? "fa-compress" : "fa-expand"}`} />
          </button>
        </div>
        {this.props.captionElements && this.props.captionElements.length > 0 && (
          <ul className="caption">
            {this.props.captionElements.map((element: CaptionElement, i: number) => (
              <li
                key={i}
                title={`${element.count} noeud${element.count > 1 ? "s sont" : " est"} du type ${element.type}`}
              >
                <span className="circle" style={{ background: element.color }} />
                <span className="type">{element.type}</span>
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  }
}
