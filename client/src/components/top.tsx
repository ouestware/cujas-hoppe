import { Link } from "react-router-dom";

export interface TopData {
  count: number;
  list: TopItem[];
}

export interface TopItem {
  value: number;
  label: string;
  id?: string; // will be pass to the link function
}

interface TopProps {
  title: string;
  items: TopData;
  linkTo?: { (id: string): string }; // optional function to craft URL from item id
  placeholder?: string;
}

const DEFAULT_PLACEHOLDER = "Aucune donnée";
const FORBIDDEN_VALUES: { [value: string]: true } = {
  "non existant": true,
  "non existante": true,
};

export const Top: React.FC<TopProps> = (props: TopProps) => {
  const { title, items, linkTo, placeholder } = props;

  // compute sum for percentages
  const totalValue = items.list.reduce((totalValue, i) => totalValue + i.value, 0);

  return (
    <div className="top">
      <h5>
        <span className="highlight">{title}</span>
      </h5>
      {items.list.length ? (
        <ul className="list-unstyled">
          {items.list.map((item) => (
            <li key={item.id || item.label} title={title + " : " + item.label}>
              {linkTo && item.label && !FORBIDDEN_VALUES[item.label] ? (
                <div className="flex">
                  <Link to={linkTo(item.label)} className="ellipsis" title={item.label}>
                    <strong>{item.label}</strong> <span className="sm">- {item.value}</span>
                  </Link>{" "}
                  &nbsp;
                  <Link to={linkTo(item.label)} className="new-tab-link" target="blank_" rel="noopener">
                    <i className="fas fa-external-link-alt" />
                  </Link>
                </div>
              ) : (
                <>
                  <strong>{item.label}</strong> <span className="sm">- {item.value}</span>
                </>
              )}
              <div className="sub-bar">
                <div
                  style={{
                    width: `${(item.value / totalValue) * 100}%`,
                  }}
                />
              </div>
            </li>
          ))}

          {items.count > items.list.length && (
            <li className="sm grey">
              <i>...sur {items.count} valeurs au total</i>
            </li>
          )}
        </ul>
      ) : (
        <p>{placeholder || DEFAULT_PLACEHOLDER}</p>
      )}
    </div>
  );
};
