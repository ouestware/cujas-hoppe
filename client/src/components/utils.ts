import { isEmpty, toPairs, isUndefined, omitBy } from "lodash";

import { FiltersState, FilterState } from "../types";

function getESQueryFromFilter(field: string, filter: FilterState): any | any[] {
  if (filter.type === "terms") return filter.value.map((v) => ({ terms: { [`${field}.raw`]: [v] } }));
  if (filter.type === "dates")
    return {
      range: { [field]: omitBy({ gte: filter.value.min, lte: filter.value.max, format: "yyyy" }, isUndefined) },
    };
}

export function getESQueryBody(query: string, filters: FiltersState, hardFilter?: Record<string, any>[]) {
  const mainQuery = {
    simple_query_string: {
      query: query,
      fields: ["_search"],
      default_operator: "and",
    },
  };
  return !isEmpty(filters) || !isEmpty(hardFilter)
    ? {
        bool: {
          must: mainQuery,
          filter: {
            bool: {
              must: !isEmpty(filters)
                ? toPairs(filters).flatMap(([field, filter]) => getESQueryFromFilter(field, filter))
                : undefined,

              should: hardFilter,
            },
          },
        },
      }
    : mainQuery;
}
