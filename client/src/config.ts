export interface Config {
  minYear: number;
  maxYear: number;
  matomo: {
    urlBase: string;
    siteId: number;
  };
  cms: { [key: string]: number };
  baseURL: string;
  colors: { [key: string]: string };
}

const config: Config = {
  minYear: 1806,
  maxYear: 1954,
  matomo: {
    urlBase: "//bcujas-pwstats.univ-paris1.fr/pw_stat/",
    siteId: 9, // optional, default value: `1`
  },
  cms: {
    apropos: 32693,
    faq: 32695,
    mentionsLegales: 32783,
    welcome: 32784,
  },
  baseURL: "/",
  // /!\ those colors variables are duplicated from CSS
  colors: {
    primaryColor: "#a00932",
    secondaryColor: "#d2c9b8",
    borderColor: "#e1e1e1",
    errorColor: "#dc3545",
    secondaryNode: "#506abf",
    authorLifetimeColor: "#ede9e3",
  },
};

export default config;
