import {
  Collection,
  ElementDeCarriere,
  Genealogy,
  Organisation,
  Personne,
  ProductionPedagogiqueEnDroit,
  WebSitePage,
} from "Heurist";
import { isUndefined, omit, omitBy, zipObject } from "lodash";

import { schema } from "./heurist-schema";

import { UndirectedGraph } from "graphology";
import circular from "graphology-layout/circular";
import { TimelineDataPoint } from "./components/books-timeline";
import { TopData, TopItem } from "./components/top";
import { getESQueryBody } from "./components/utils";
import { NetworkField } from "./pages/network";
import { ESSearchQueryContext, FilterHistogramType, PlainObject } from "./types";

export type SchemaFieldDefinition = {
  id: string;
  required: boolean;
  type: string;
  name: string;
  depth?: number;
  isArray?: boolean;
  resourceTypes?: Array<string>;
};

export interface SchemaComputedFieldDefinition extends Omit<SchemaFieldDefinition, "id"> {
  //computeField?: (o: any) => any;
  // TODO: uncomment (declaring it brings typescript errors...) Actually we don't need this
}

export type SchemaModelDefinition = {
  id: string;
  name: string;
  index_name?: string;
  index_configuration?: any;
  fields: Array<SchemaFieldDefinition>;
  computed_fields?: Array<SchemaComputedFieldDefinition>;
};

// TODO: put this in conf file ?
const MAX_NB_PRODUCTION_IN_PAGE = 20;

/**
 * Cast an ES object to an heurist one.
 * For this, an ES object MUST have a `type` field with the name of the heurist model.
 * But this is the case.
 */
export function esObjectToHeurist<T>(object: any): T {
  const type: string = object.type;
  const heurist: any = Object.assign({}, object);
  const definition: SchemaModelDefinition | undefined = schema.find((model) => model.name === type);
  if (!definition) {
    console.error(`${type} not found in heurist schema definition on object ${JSON.stringify(object)}`);
    throw new Error(`${type} not found in heurist schema definition on object ${JSON.stringify(object)}`);
  }
  Object.keys(object).forEach((field: string) => {
    const fieldDefinition =
      definition.fields.find((def) => def.name === field) ||
      (definition.computed_fields || []).find((def) => def.name === field);
    if (fieldDefinition && object[field] !== null && object[field] !== undefined) {
      // Transform in array if needed
      if (object[field] && !Array.isArray(object[field]) && fieldDefinition.isArray) {
        heurist[field] = [heurist[field]];
      }

      // Check for dateRange
      if (fieldDefinition.type === "dateRange") {
        if (fieldDefinition.isArray) {
          heurist[field] = heurist[field].map((value: any) => {
            return {
              type: value.type,
              date: new Date(value.date),
              range: {
                lte: new Date(value.range.lte),
                gte: new Date(value.range.gte),
              },
            };
          });
        } else {
          heurist[field] = {
            type: heurist[field].type,
            date: new Date(heurist[field].date),
            range: {
              lte: new Date(heurist[field].range.lte),
              gte: new Date(heurist[field].range.gte),
            },
          };
        }
      }

      // Check for date
      if (fieldDefinition.type === "date") {
        if (fieldDefinition.isArray) {
          heurist[field] = heurist[field].map((value: any) => {
            return new Date(value);
          });
        } else {
          heurist[field] = new Date(heurist[field]);
        }
      }

      // Check for nested object for resource type
      if (fieldDefinition.type === "resource" && fieldDefinition.resourceTypes && (fieldDefinition.depth || 0) > 0) {
        if (fieldDefinition.isArray) {
          heurist[field] = heurist[field].map((value: any) => {
            return esObjectToHeurist(value);
          });
        } else {
          heurist[field] = esObjectToHeurist(heurist[field]);
        }
      }

      // Check for nested object for resource type
      if (fieldDefinition.type === "relmarker" && fieldDefinition.resourceTypes && (fieldDefinition.depth || 0) > 0) {
        if (fieldDefinition.isArray) {
          heurist[field] = heurist[field].map((value: any) => {
            return { relation: value.relation, direction: value.direction, other: esObjectToHeurist(value.other) };
          });
        } else {
          heurist[field] = {
            relation: heurist[field].relation,
            direction: heurist[field].direction,
            other: esObjectToHeurist(heurist[field].other),
          };
        }
      }

      // Check for
      // ID ALEPH to linking to CUJAS catalogue
      if (field === "idNBibliographique") {
        if (fieldDefinition.isArray) {
          heurist[field] = object[field].map((e: any) => (+e + "").padStart(9, "0")); // transform '\d+.0' into '\d'
        } else {
          heurist[field] = (+object[field] + "").padStart(9, "0");
        }
      }
    }
  });
  return heurist as T;
}

/***** UTILS */

interface Query {
  index: string;
  query: Object;
}

// newline delemited JSON format for the msearch ES API point
const NDJSON = (queries: Query[]): string => {
  const ndjsonQuery: string = queries.reduce<string>((mqueries: string, q: Query): string => {
    return mqueries + `${JSON.stringify({ index: q.index })}\n${JSON.stringify(q.query)}\n`;
  }, "");
  return ndjsonQuery;
};

// tranform terms aggregation result into TopItem
const aggregationsToTopItems = (buckets: Object[], label?: Function): TopItem[] =>
  buckets.map((b: any) => ({
    id: b.key,
    label: label ? label(b) : b.key,
    value: b.doc_count,
  }));

const editorItemLabel = (b: any) =>
  b.key === "non existante"
    ? b.key
    : b.editeur.hits.hits[0]._source.editeurs?.find((o: Organisation) => o.id === b.key).nomEntiteMorale || b.key;

const authorItemLabel = (b: any) => {
  if (b.key === "non existant") return b.key;
  const author = b.auteur.hits.hits[0]._source.tous_les_auteurs.find((a: Personne) => a.id === b.key);
  return author.nom_complet;
};

/**** AUTHORS *****/

export const getAuthor = (
  id: string,
): Promise<{
  author: Personne;
  books: ProductionPedagogiqueEnDroit[];
  CV: ElementDeCarriere[];
  booksHistogramData: TimelineDataPoint[];
  topEditors: TopData;
  topClassifications: TopData;
  topNiveauDuCours: TopData;
}> => {
  const authorESRequest = {
    method: "POST",
    headers: {
      "Content-Type": "application/x-ndjson",
    },
    body: NDJSON([
      { index: "personne", query: { query: { term: { id: { value: id } } } } },
      {
        index: "productionpedagogiqueendroit",
        query: {
          query: { term: { "tous_les_auteurs.id": id } },
          size: MAX_NB_PRODUCTION_IN_PAGE,
          aggs: {
            books_over_time: {
              date_histogram: {
                field: "dateEdition.date",
                calendar_interval: "year",
                format: "yyyy",
              },
            },
            // using a text field waiting for editors proper relations
            top_editors: {
              terms: {
                field: "editeurs.id.raw",
                missing: "non existant",
              },
              aggs: {
                editeur: { top_hits: { size: 1, _source: { include: ["editeurs"] } } },
              },
            },
            total_editors: { cardinality: { field: `editeurs.id.raw` } },
            top_classifications: {
              terms: {
                field: "classification.raw",
                missing: "non existante",
              },
            },
            total_classifications: { cardinality: { field: `classification.raw` } },
            top_niveauDuCours: {
              terms: {
                field: "niveauDuCours.raw",
                missing: "non existante",
              },
            },
            total_niveauDuCours: { cardinality: { field: `niveauDuCours.raw` } },
            books_without_editionDate: {
              missing: { field: "dateEdition" },
            },
          },
        },
      },
      {
        index: "elementdecarriere",
        query: {
          size: 999,
          query: { term: { "personne.id": id } },
        },
      },
    ]),
  };
  return fetch(`/elasticsearch/personne/_msearch`, authorESRequest).then((r) => {
    return r.json().then((d) => {
      const author: Personne = esObjectToHeurist<Personne>(d.responses[0].hits.hits[0]._source);
      const books: ProductionPedagogiqueEnDroit[] = d.responses[1].hits.hits.map((d: any) =>
        esObjectToHeurist<ProductionPedagogiqueEnDroit>(d._source),
      );
      const booksHistogramData = d.responses[1].aggregations.books_over_time.buckets.map(
        (b: any): TimelineDataPoint => {
          return { year: b.key_as_string, value: b.doc_count };
        },
      );
      const CV: ElementDeCarriere[] = d.responses[2].hits.hits.map((d: any) =>
        esObjectToHeurist<ElementDeCarriere>(d._source),
      );
      return {
        author,
        books,
        CV,
        booksHistogramData,
        // TODO : add editor id here once we have the data...
        topEditors: {
          count: d.responses[1].aggregations.total_editors.value,
          list: aggregationsToTopItems(d.responses[1].aggregations.top_editors.buckets, editorItemLabel),
        },
        topClassifications: {
          count: d.responses[1].aggregations.total_classifications.value,
          list: aggregationsToTopItems(d.responses[1].aggregations.top_classifications.buckets),
        },
        topNiveauDuCours: {
          count: d.responses[1].aggregations.total_niveauDuCours.value,
          list: aggregationsToTopItems(d.responses[1].aggregations.top_niveauDuCours.buckets),
        },
      };
    });
  });
};

/*******  EDITORS *************/

export const getOrganisation = (
  id: string,
  as: "editor" | "author",
): Promise<{
  organisation: Organisation;
  books: ProductionPedagogiqueEnDroit[];
  booksHistogramData: TimelineDataPoint[];
  topAuthorsOrEditors: TopData;
  topClassifications: TopData;
  topNiveauDuCours: TopData;
}> => {
  const editorESRequest = {
    method: "POST",
    headers: {
      "Content-Type": "application/x-ndjson",
    },
    body: NDJSON([
      { index: "organisation", query: { query: { term: { id: { value: id } } } } },
      {
        index: "productionpedagogiqueendroit",
        query: {
          query: { term: { [as === "editor" ? "editeurs.id" : "tous_les_auteurs.id.raw"]: id } },
          size: MAX_NB_PRODUCTION_IN_PAGE,
          aggs: {
            books_over_time: {
              date_histogram: {
                field: "dateEdition.date",
                calendar_interval: "year",
                format: "yyyy",
              },
            },
            top_authors_or_editors: {
              terms: {
                field: as === "editor" ? "tous_les_auteurs.id.raw" : "editeurs.id.raw",
                missing: "non existant",
              },
              aggs: {
                [as === "editor" ? "auteur" : "editeur"]: {
                  top_hits: { size: 1, _source: { include: [as === "editor" ? "tous_les_auteurs" : "editeurs"] } },
                },
              },
            },
            total_authors_or_editors: {
              cardinality: { field: as === "editor" ? "tous_les_auteurs.id.raw" : "editeurs.id.raw" },
            },
            top_classifications: {
              terms: {
                field: "classification.raw",
                missing: "non existante",
              },
            },
            total_classifications: { cardinality: { field: `classification.raw` } },
            top_niveauDuCours: {
              terms: {
                field: "niveauDuCours.raw",
                missing: "non existant",
              },
            },
            total_niveauDuCours: { cardinality: { field: `niveauDuCours.raw` } },
            books_without_editionDate: {
              missing: { field: "dateEdition" },
            },
          },
        },
      },
    ]),
  };
  return fetch(`/elasticsearch/organisation/_msearch`, editorESRequest)
    .then((r) => {
      return r.json();
    })
    .then((d) => {
      const organisation: Organisation = esObjectToHeurist<Organisation>(d.responses[0].hits.hits[0]._source);
      const books: ProductionPedagogiqueEnDroit[] = d.responses[1].hits.hits.map((d: any) =>
        esObjectToHeurist<ProductionPedagogiqueEnDroit>(d._source),
      );
      const booksHistogramData: TimelineDataPoint[] = d.responses[1].aggregations.books_over_time.buckets.map(
        (b: any): TimelineDataPoint => {
          return { year: b.key_as_string, value: b.doc_count };
        },
      );

      return {
        organisation,
        books,
        booksHistogramData,
        topAuthorsOrEditors: {
          count: d.responses[1].aggregations.total_authors_or_editors.value,
          list: aggregationsToTopItems(
            d.responses[1].aggregations.top_authors_or_editors.buckets,
            as === "editor" ? authorItemLabel : editorItemLabel,
          ),
        },
        topClassifications: {
          count: d.responses[1].aggregations.total_classifications.value,
          list: aggregationsToTopItems(d.responses[1].aggregations.top_classifications.buckets),
        },
        topNiveauDuCours: {
          count: d.responses[1].aggregations.total_niveauDuCours.value,
          list: aggregationsToTopItems(d.responses[1].aggregations.top_niveauDuCours.buckets),
        },
      };
    });
};

/*******   BOOKS *************/

export const getBook = (
  id: string,
): Promise<{
  book: ProductionPedagogiqueEnDroit;
}> => {
  const bookESRequest = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: { term: { id: id } },
      size: 999,
    }),
  };

  return fetch(`/elasticsearch/productionpedagogiqueendroit/_search`, bookESRequest)
    .then((r) => r.json())
    .then((d) => {
      const book: ProductionPedagogiqueEnDroit = esObjectToHeurist<ProductionPedagogiqueEnDroit>(
        d.hits.hits[0]._source,
      );
      return {
        book,
      };
    });
};

/*******   COLLECTIONS *************/

export const getCollection = (
  id: string,
): Promise<{
  collection: Collection;
  books: ProductionPedagogiqueEnDroit[];
  booksHistogramData: TimelineDataPoint[];
  topAuthors: TopData;
  topClassifications: TopData;
  topNiveauDuCours: TopData;
}> => {
  const collectionESRequest = {
    method: "POST",
    headers: {
      "Content-Type": "application/x-ndjson",
    },
    body: NDJSON([
      { index: "collection", query: { query: { term: { id: { value: id } } } } },
      {
        index: "productionpedagogiqueendroit",
        query: {
          query: { term: { "collection.id": id } },
          size: MAX_NB_PRODUCTION_IN_PAGE,
          aggs: {
            books_over_time: {
              date_histogram: {
                field: "dateEdition.date",
                calendar_interval: "year",
                format: "yyyy",
              },
            },
            top_authors: {
              terms: {
                field: "tous_les_auteurs.id.raw",
                missing: "non existant",
              },
              aggs: {
                auteur: {
                  top_hits: { size: 1, _source: { include: "tous_les_auteurs" } },
                },
              },
            },
            total_authors: {
              cardinality: { field: "tous_les_auteurs.id.raw" },
            },
            top_classifications: {
              terms: {
                field: "classification.raw",
                missing: "non existante",
              },
            },
            total_classifications: { cardinality: { field: `classification.raw` } },
            top_niveauDuCours: {
              terms: {
                field: "niveauDuCours.raw",
                missing: "non existant",
              },
            },
            total_niveauDuCours: { cardinality: { field: `niveauDuCours.raw` } },
            books_without_editionDate: {
              missing: { field: "dateEdition" },
            },
          },
        },
      },
    ]),
  };
  return fetch(`/elasticsearch/collection/_msearch`, collectionESRequest)
    .then((r) => {
      return r.json();
    })
    .then((d) => {
      const collection: Collection = esObjectToHeurist<Collection>(d.responses[0].hits.hits[0]._source);
      const books: ProductionPedagogiqueEnDroit[] = d.responses[1].hits.hits.map((d: any) =>
        esObjectToHeurist<ProductionPedagogiqueEnDroit>(d._source),
      );
      const booksHistogramData: TimelineDataPoint[] = d.responses[1].aggregations.books_over_time.buckets.map(
        (b: any): TimelineDataPoint => {
          return { year: b.key_as_string, value: b.doc_count };
        },
      );

      return {
        collection,
        books,
        booksHistogramData,
        topAuthors: {
          count: d.responses[1].aggregations.total_authors.value,
          list: aggregationsToTopItems(d.responses[1].aggregations.top_authors.buckets, authorItemLabel),
        },
        topClassifications: {
          count: d.responses[1].aggregations.total_classifications.value,
          list: aggregationsToTopItems(d.responses[1].aggregations.top_classifications.buckets),
        },
        topNiveauDuCours: {
          count: d.responses[1].aggregations.total_niveauDuCours.value,
          list: aggregationsToTopItems(d.responses[1].aggregations.top_niveauDuCours.buckets),
        },
      };
    });
};

/******* NETWORK *************/

const networkRequests = (fields: string[]): Object => ({
  method: "POST",
  headers: {
    "Content-Type": "application/x-ndjson",
  },
  body: NDJSON([
    // get productions to build the network
    {
      index: "productionpedagogiqueendroit",
      query: {
        query: {
          bool: {
            filter: fields.map<Object>((field) => ({
              exists: {
                field,
              },
            })),
          },
        },
        size: 9999,
        aggs: {
          books_over_time: {
            date_histogram: {
              field: "dateEdition.date",
              calendar_interval: "year",
              format: "yyyy",
            },
          },
          books_without_editionDate: {
            missing: { field: "dateEdition" },
          },
        },
      },
    },
    // get production we can't add to the network cause of missing field(s)
    {
      index: "productionpedagogiqueendroit",
      query: {
        query: {
          bool: {
            should: fields.map<Object>((field) => ({
              bool: {
                must_not: {
                  exists: {
                    field,
                  },
                },
              },
            })),
            minimum_should_match: 1,
          },
        },
        size: 9999,
        aggs: {
          missing_books_over_time: {
            date_histogram: {
              field: "dateEdition.date",
              calendar_interval: "year",
              format: "yyyy",
            },
          },
          books_without_editionDate: {
            missing: { field: "dateEdition" },
          },
        },
      },
    },
  ]),
});

const createNodeFromField = (
  p: any,
  field: NetworkField,
  color: string,
  productionDefinition: SchemaModelDefinition | undefined,
) => {
  const fieldDefinition =
    productionDefinition?.fields.find((f) => f.name === field.value) ||
    (productionDefinition?.computed_fields || []).find((f) => f.name === field.value);
  if (fieldDefinition && p[field.value]) {
    const values = fieldDefinition.isArray ? p[field.value] || [] : [p[field.value]];
    return values.map((value: any) => {
      const nodeID = fieldDefinition?.type === "resource" ? value.id : value;
      return {
        key: nodeID,
        label: field.getLabel ? field.getLabel(value) : value,
        type: field.value,
        object: value,
        color,
      };
    });
  } else return [];
};

const aggregationCast: (buckets: { key_as_string: string; doc_count: number }[]) => TimelineDataPoint[] = (
  buckets: { key_as_string: string; doc_count: number }[],
) =>
  buckets.map((b: { key_as_string: string; doc_count: number }): TimelineDataPoint => {
    return { year: b.key_as_string, value: b.doc_count };
  });

export const getNetworkPageData = (
  fields: NetworkField[],
): Promise<{
  network: UndirectedGraph;
  booksHistogramData: TimelineDataPoint[];
  missingBooksHistogramData: TimelineDataPoint[];
}> => {
  const productionDefinition: SchemaModelDefinition | undefined = schema.find(
    (model) => model.name === "productionPedagogiqueEnDroit",
  );

  return fetch(`/elasticsearch/productionpedagogiqueendroit/_msearch`, networkRequests(fields.map((f) => f.value)))
    .then((r) => r.json())
    .then((data) => {
      const network = new UndirectedGraph();
      const productions: ProductionPedagogiqueEnDroit[] = data.responses[0].hits.hits.map((h: any) =>
        esObjectToHeurist<ProductionPedagogiqueEnDroit>(h._source),
      );

      productions.forEach((p) => {
        // add first field as node
        //TODO: pass colors through props ?
        const firstNodes = createNodeFromField(p, fields[0], "#a00932", productionDefinition);
        const secondNodes = createNodeFromField(p, fields[1], "#506abf", productionDefinition);
        firstNodes.concat(secondNodes).forEach((node: any) => {
          network.mergeNode(node.key, { ...node, label: node.label || "" });
          network.updateNodeAttribute(node.key, "size", (n) => Math.sqrt((Math.pow(n, 2) || 0) + 1));
          network.updateNodeAttribute(node.key, "books", (b) => ({ ...b, [p.id]: p }));
        });
        firstNodes.forEach((source: any) => {
          secondNodes.forEach((target: any) => {
            network.mergeEdge(source.key, target.key, { color: "#b0b0b0" });
            network.updateEdgeAttribute(source.key, target.key, "books", (b) => ({ ...b, [p.id]: p }));
            network.updateEdgeAttribute(source.key, target.key, "nbBooks", (n) => (n || 0) + 1);
            network.setEdgeAttribute(
              source.key,
              target.key,
              "size",
              Math.log(network.getEdgeAttribute(source.key, target.key, "nbBooks")),
            );
          });
        });
      });

      // Books used to compute the network counts over time
      const booksHistogramData: TimelineDataPoint[] = aggregationCast(
        data.responses[0].aggregations.books_over_time.buckets,
      );
      // Books not used to compute the network counts over time
      const missingBooksHistogramData: TimelineDataPoint[] = aggregationCast(
        data.responses[1].aggregations.missing_books_over_time.buckets,
      );

      // Initial layout
      circular.assign(network);

      return {
        network,
        booksHistogramData,
        missingBooksHistogramData,
      };
    });
};

/**
 * GENERIC FUNCTIONS:
 * ******************
 */
function getESIncludeRegexp(query: string): string {
  return ".*" + [...query.toLowerCase()].map((char) => `[${char}${char.toUpperCase()}]`).join("") + ".*";
}

export async function getTerms(
  context: ESSearchQueryContext,
  field: string,
  prefix?: string,
  count?: number,
): Promise<{ term: string; count: number }[]> {
  const body: PlainObject = {
    size: 0,
    query: getESQueryBody(context.query, omit(context.filters, [field]), context.hardFilter),
    aggs: {
      termsList: {
        terms: {
          field: `${field}.raw`,
          include: prefix ? `.*${getESIncludeRegexp(prefix)}.*` : undefined,
          size: count || 15,
          order: { _key: "asc" },
        },
      },
    },
  };

  if (prefix) body.query = { prefix: { [field]: prefix } };

  return await fetch(`/elasticsearch/${context.index}/_search`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  })
    .then((res) => res.json())
    .then((data) =>
      data.aggregations.termsList.buckets.map((bucket: { key: string; doc_count: number }) => ({
        term: bucket.key,
        count: bucket.doc_count,
      })),
    );
}
export async function getHistograms(
  context: ESSearchQueryContext,
  fields: string[],
  size: number = 5,
): Promise<PlainObject<FilterHistogramType>> {
  const CARDINALITY_PREFIX = "CARDINALITY::";
  const TERMS_PREFIX = "TERMS::";

  const body: PlainObject = {
    size: 0,
    query: getESQueryBody(context.query, context.filters, context.hardFilter),
    aggs: fields.reduce(
      (iter, field) => ({
        ...iter,
        [CARDINALITY_PREFIX + field]: { cardinality: { field: `${field}.raw` } },
        [TERMS_PREFIX + field]: { terms: { field: `${field}.raw`, size } },
      }),
      {},
    ),
  };

  return await fetch(`/elasticsearch/${context.index}/_search`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  })
    .then((res) => res.json())
    .then((data) =>
      fields.reduce(
        (iter, field) => ({
          ...iter,
          [field]: {
            total: data.aggregations[CARDINALITY_PREFIX + field].value,
            values: data.aggregations[TERMS_PREFIX + field].buckets.map(
              (bucket: { key: string; doc_count: number }) => ({
                label: bucket.key,
                count: bucket.doc_count,
              }),
            ),
          },
        }),
        {},
      ),
    );
}

export function search(
  context: ESSearchQueryContext,
  cleanFn: (rawData: any) => any,
  from: number,
  size: number,
  histogramField?: string,
): Promise<{ list: PlainObject[]; total: number; histogram?: TimelineDataPoint[] }> {
  return fetch(`/elasticsearch/${context.index}/_search`, {
    body: JSON.stringify(
      omitBy(
        {
          size,
          from,
          query: getESQueryBody(context.query, context.filters, context.hardFilter),
          sort: context.sort ? context.sort.expression : undefined,
          aggs: histogramField
            ? {
                histogram: {
                  date_histogram: {
                    field: histogramField,
                    calendar_interval: "year",
                    format: "yyyy",
                  },
                },
              }
            : undefined,
        },
        isUndefined,
      ),
    ),
    headers: {
      "Content-Type": "application/json",
    },
    method: "POST",
  })
    .then((r) => r.json())
    .then((data) => ({
      list: data.hits.hits.map((d: any) => cleanFn({ ...d._source, books: [] })),
      total: data.hits.total.value,
      histogram: histogramField
        ? data.aggregations.histogram.buckets.map((bucket: PlainObject) => ({
            year: bucket.key_as_string,
            value: bucket.doc_count,
          }))
        : undefined,
    }));
}

export async function getBooksHistogramsByIds(
  field: string,
  ids: string[],
): Promise<{ [key: string]: TimelineDataPoint[] }> {
  // do nothing if params
  if (!ids || ids.length === 0) return {};
  const result: any = await fetch(`/elasticsearch/productionpedagogiqueendroit/_msearch`, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-ndjson",
    },
    body: NDJSON(
      ids.map((id) => ({
        index: "productionpedagogiqueendroit",
        query: {
          query: { term: { [`${field}.id`]: id } },
          size: 0,
          aggs: {
            books_over_time: {
              date_histogram: {
                field: "dateEdition.date",
                calendar_interval: "year",
                format: "yyyy",
              },
            },
          },
        },
      })),
    ),
  });
  const data: any = await result.json();
  return zipObject(
    ids,
    (data.responses || []).map((response: any) =>
      response.aggregations.books_over_time.buckets.map((b: any): TimelineDataPoint => {
        return { year: b.key_as_string, value: b.doc_count };
      }),
    ),
  );
}

export function getCMSEntry(id: number): Promise<WebSitePage> {
  return fetch(`/elasticsearch/websitepage/_doc/${id}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
    },
  })
    .then((r) => r.json())
    .then((data) => {
      return esObjectToHeurist<WebSitePage>(data["_source"]);
    });
}

export enum GenealogyType {
  organisation,
  book,
}

export function getGenealogyFromNodeID<T>(nodeID: string, genealogyType: GenealogyType): Promise<Genealogy<T> | null> {
  let indexName: string = "";
  switch (genealogyType) {
    case GenealogyType.organisation:
      indexName = "genealogy_org";
      break;
    case GenealogyType.book:
      indexName = "genealogy_traite";
      break;
  }
  return fetch(`/elasticsearch/${indexName}/_search/`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: {
        term: { "nodes.id": nodeID },
      },
    }),
  })
    .then((r) => r.json())
    .then((data) => {
      if (data.hits && data.hits.total.value > 0)
        return {
          nodes: data.hits.hits[0]["_source"].nodes.map((o: any) => esObjectToHeurist<T>(o)),
          edges: data.hits.hits[0]["_source"].edges,
        };
      else return null;
    });
}
