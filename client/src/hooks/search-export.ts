import { useState } from "react";
import Papa from "papaparse";
import { saveAs } from "file-saver";
import { isArray, isObject, keys, union } from "lodash";
import { ESSearchQueryContext } from "../types";
import { esObjectToHeurist, search } from "../elasticsearchClient";
import { dateHeuristLabel } from "../components/date";

interface ExportReturnType {
  loading: boolean;
  progress: number; // between 0 and 100
  error: Error | null;
}

type ExportHookType = [
  (context: ESSearchQueryContext, filename: string) => Promise<ExportReturnType>,
  ExportReturnType,
];

function castInlineObject(value: any): any {
  let result: any = value;
  if (value.date && value.range) {
    try {
      result = dateHeuristLabel({ date: value });
    } catch (e) {
      console.error(e);
    }
  } else {
    if (value.other) {
      result = `${castInlineObject(value.other)}(${value.relation})`;
    } else
      switch (value["type"]) {
        case "personne":
          result = value.nom_complet;
          break;
        case "organisation":
          result = value.nomEntiteMorale;
          break;
        case "ville":
          result = [value.ville, value.pays].filter((s) => s).join(", ");
          break;
        case "productionPedagogiqueEnDroit":
          result = value.titre;
          break;
      }
  }
  return result;
}

const translateKey = (key: string) => {
  switch (key) {
    case "nodes":
      return "Généalogie";

    case "fonctionAuteur":
      return "Auteurs secondaires";

    default:
      return key;
  }
};

function castForExport(obj: { [key: string]: any }): { [key: string]: any } {
  const result: { [key: string]: any } = {};
  Object.keys(obj).forEach((key: string) => {
    if (
      ![
        "books",
        "type",
        "structuresLiees",
        "personneLiee",
        "otherPersonsInvolved",
        "edges",
        "tous_les_auteurs",
      ].includes(key)
    ) {
      // if it's an array => recursivity
      if (isArray(obj[key])) {
        result[translateKey(key)] = obj[key].map((o: any) => castInlineObject(o));
        result[translateKey(key)] = result[translateKey(key)].join("|");
        // if it's an object
      } else {
        if (isObject(obj[key])) {
          result[translateKey(key)] = castInlineObject(obj[key]);
          // date case
        } else {
          result[translateKey(key)] = obj[key];
        }
      }
    }
  });
  return result;
}

export function useSearchExportToCSV(batchSize = 500): ExportHookType {
  // State of the hook
  const [error, setError] = useState<Error | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [progress, setProgress] = useState<number>(0);

  // Function to do the export
  async function exportFn(context: ESSearchQueryContext, filename: string): Promise<ExportReturnType> {
    setLoading(true);
    setError(null);
    setProgress(0);

    let nbResult = -1;
    let data: any = [];
    let columns: string[] = [];
    try {
      // Load the data in batch mode
      while (data.length !== nbResult) {
        const batchResult: any = await search(
          context,
          (e) => {
            // TODO: reuse searchTypes.cleanFn
            return castForExport(
              e.nodes ? { ...e, nodes: e.nodes.map((o: any) => esObjectToHeurist(o)) } : esObjectToHeurist(e),
            );
          },
          data.length,
          batchSize,
        );
        let elementKeys: string[] = [];
        batchResult.list.forEach((element: any) => {
          elementKeys = union(keys(element), elementKeys);
          data.push(element);
        });
        columns = union(elementKeys, columns);
        nbResult = batchResult.total;
        setProgress((data.length / nbResult) * 100);
      }

      if (data.length === 0) {
        throw new Error("No data to export");
      } else {
        const csv = Papa.unparse(data, { columns });
        const blob = new Blob([csv], { type: "text/csv" });
        saveAs(blob, filename);
      }
      setProgress(100);
    } catch (e) {
      setError(e as Error);
    } finally {
      setLoading(false);
    }

    return { loading, error, progress };
  }

  return [exportFn, { loading, error, progress }];
}
