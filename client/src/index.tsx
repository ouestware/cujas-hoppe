import { MatomoProvider, createInstance } from "@datapunt/matomo-tracker-react";

import ReactDOM from "react-dom/client";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";

import { MatomoTracker } from "./components/matomo-tracker";
import ScrollToTop from "./components/scroll-to-top";
import config from "./config";
import "./index.scss";
import { Footer } from "./layout/footer";
import { Header } from "./layout/header";
// pages
import { MatomoInstance } from "@datapunt/matomo-tracker-react/lib/types";
import { FC, PropsWithChildren, StrictMode } from "react";
import { AboutPage } from "./pages/about";
import { Author } from "./pages/author";
import { Book } from "./pages/book";
import { BookGenealogy } from "./pages/book-genealogy";
import { CollectionPage } from "./pages/collection";
import { Editor, OrgAuthor } from "./pages/editor";
import { EditorGenealogy } from "./pages/editor-genealogy";
import { FAQPage } from "./pages/faq";
import { Home } from "./pages/home/home";
import { LegalsPage } from "./pages/legals";
import { DEFAULT_NETWORK_TYPE, Network } from "./pages/network";

type MatomoProviderFixedType = FC<PropsWithChildren & { value: MatomoInstance }>;
const MatomoProviderFixed: MatomoProviderFixedType = MatomoProvider as MatomoProviderFixedType;
const instance = createInstance(config.matomo);

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);
root.render(
  <StrictMode>
    <BrowserRouter>
      <ScrollToTop />
      <MatomoProviderFixed value={instance}>
        <div className="app-root">
          <Header />
          <main>
            <div className="container">
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/faq" element={<FAQPage />} />
                <Route path="/a-propos" element={<AboutPage />} />
                <Route path="/mentions-legales" element={<LegalsPage />} />
                <Route path="/ouvrage/:id" element={<Book />} />
                <Route path="/ouvrage/:id/genealogie" element={<BookGenealogy />} />
                <Route path="/editeur/:id" element={<Editor />} />
                <Route path="/editeur/:id/genealogie" element={<EditorGenealogy />} />
                <Route path="/collection/:id" element={<CollectionPage />} />
                <Route path="/auteur/:id" element={<Author />} />
                <Route path="/auteur-org/:id" element={<OrgAuthor />} />
                <Route path="/reseau" element={<Navigate to={`/reseau/${DEFAULT_NETWORK_TYPE}`} replace />} />
                <Route path="/reseau/:networkType" element={<Network />} />
              </Routes>
            </div>
          </main>
          <Footer />
        </div>
        <MatomoTracker />
      </MatomoProviderFixed>
    </BrowserRouter>
  </StrictMode>,
);
