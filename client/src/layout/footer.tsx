import { Link } from "react-router-dom";

export const Footer: React.FC = () => {
  return (
    <footer>
      <div className="footer container">
        <span>
          <a href="http://biu-cujas.univ-paris1.fr/" title="Lien vers le site officiel de la Bibliothèque Cujas">
            Bibliothèque Cujas
          </a>{" "}
          <a
            className="new-tab-link"
            href="http://biu-cujas.univ-paris1.fr/"
            target="_blank"
            rel="noopener noreferrer"
            title="Ouvrir dans un nouvel onglet : Lien vers le site officiel de la Bibliothèque Cujas"
          >
            <i className="fas fa-external-link-alt" />
          </a>{" "}
          |{" "}
          <a href="https://www.collexpersee.eu/" title="Lien vers le site ColleEx-Persée">
            CollEx - Persée
          </a>{" "}
          <a
            className="new-tab-link"
            href="https://www.collexpersee.eu/"
            target="_blank"
            title="Ouvrir dans un nouvel onglet : Lien vers le site ColleEx-Persée"
            rel="noopener noreferrer"
          >
            <i className="fas fa-external-link-alt" />
          </a>{" "}
          |{" "}
          <Link to="/mentions-legales" title="Mentions légales du site">
            Mentions légales
          </Link>{" "}
          <Link
            className="new-tab-link"
            to="/mentions-legales"
            target="_blank"
            rel="noopener noreferrer"
            title="Ouvrir dans un nouvel onglet : Mentions légales du site"
          >
            <i className="fas fa-external-link-alt" />
          </Link>{" "}
          |{" "}
          <a href="mailto:HoppeDroit@univ-paris1.fr" title="Envoyer un mail à l'adresse de contact">
            Contact
          </a>
        </span>
        <span>
          <i className="fas fa-code" /> code source disponible sur{" "}
          <a href="https://gitlab.com/ouestware/cujas-hoppe/" title="Liens vers le code source du projet">
            GitLab
          </a>{" "}
          <a
            className="new-tab-link"
            href="https://gitlab.com/ouestware/cujas-hoppe/"
            target="_blank"
            rel="noopener noreferrer"
            title="Ouvrir dans un nouvel onglet : Liens vers le code source du projet"
          >
            <i className="fas fa-external-link-alt" />{" "}
          </a>
        </span>
      </div>
    </footer>
  );
};
