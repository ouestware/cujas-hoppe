import { Link } from "react-router-dom";

export const Header: React.FC = () => {
  return (
    <header>
      <div className="container">
        <h4 className="mr-1 fg">
          <i className="fas fa-book-open mr-1" />{" "}
          <Link to="/" title="Hypothèses d’Observation des Productions Pédagogiques Editées en Droit">
            HOPPE-Droit
          </Link>
        </h4>
        <h5 className="mr-2">
          <Link to="/reseau" title="Réseaux de co-publication">
            Explorer les réseaux
          </Link>
        </h5>
        <h5 className="mr-2"> | </h5>
        <h5 className="mr-2">
          <Link to="/faq" title="Foire Aux Questions">
            F.A.Q.
          </Link>
        </h5>
        <h5 className="mr-2"> | </h5>
        <h5>
          <Link to="/a-propos" title="À propos de ce site web">
            À propos
          </Link>
        </h5>
      </div>
    </header>
  );
};
