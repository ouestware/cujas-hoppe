import { FC } from "react";
import { CMSEntry } from "../components/cms-entry";

import config from "../config";

export const AboutPage: FC = () => (
  <div className="about-page">
    <CMSEntry id={config.cms.apropos} />
  </div>
);
