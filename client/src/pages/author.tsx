import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

import { ElementDeCarriere, Personne, ProductionPedagogiqueEnDroit } from "Heurist";
import { some } from "lodash";
import { BookComponent } from "../components/book";
import { BooksTimeline, TimelineDataPoint } from "../components/books-timeline";
import { CurriculumVitae } from "../components/curriculum-vitae";
import { DateHeuristComponent } from "../components/date";
import { Loader } from "../components/loader";
import { PlaceComponent } from "../components/place";
import { Top, TopData } from "../components/top";
import { getAuthor } from "../elasticsearchClient";
import { NETWORK_TYPES } from "./network";

const personneLieeLabels: { [key: string]: { [key: string]: string } } = {
  EstMariDe: { out: "Époux de", in: "Épouse de" },
  EstCompagneOuCompagnonDe: { in: "Est compagn.e.on de", out: "Est compagn.e.on de" },
  EstFrèreDe: { in: "Fratrie", out: "Fratrie" },
  EstSoeurDe: { in: "Fratrie", out: "Fratrie" },
  EstMembreDeLaFamilleDe: { in: "Autre.s membre.s de la famille", out: "Autre.s membre.s de la famille" },
  EstMariéA: { in: "Est marié à", out: "Est marié à" },
};

export const Author: React.FC = () => {
  // init state
  const { id } = useParams<"id">();
  const [author, setAuthor] = useState<Personne | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [books, setBooks] = useState<ProductionPedagogiqueEnDroit[]>([]);
  const [CV, setCV] = useState<ElementDeCarriere[]>([]);
  const [links, setLinks] = useState<string[]>([]);
  const [booksHistogramData, setBooksHistogramData] = useState<TimelineDataPoint[]>([]);
  const [topEditors, setTopEditors] = useState<TopData>({ count: 0, list: [] });
  const [topClassifications, setTopClassifications] = useState<TopData>({ count: 0, list: [] });
  const [topNiveauDuCours, setTopNiveauDuCours] = useState<TopData>({ count: 0, list: [] });

  // fetch data when props id changed
  useEffect(() => {
    if (id) {
      setLoading(true);
      setError(null);
      getAuthor(id)
        .then((data) => {
          const { author, books, CV, booksHistogramData, topEditors, topClassifications, topNiveauDuCours } = data;
          setAuthor(author);
          setBooks(books);
          setCV(CV);
          setLinks([...(author.lienVersAutorite || []), ...(author.ressourceExterieure || [])]);
          setBooksHistogramData(booksHistogramData);
          setTopEditors(topEditors);
          setTopClassifications(topClassifications);
          setTopNiveauDuCours(topNiveauDuCours);
        })
        .catch((error) => {
          console.error(error);
          setError(`L'identifiant d'auteur ${id} est introuvable.`);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [id]);

  // TOP link methods
  const dataGridUrl = `/?t=book&q=*&tous_les_auteurs.nom_complet=${encodeURIComponent(author?.nom_complet || "")}`;
  // labels
  const birthLabel = author && `Né${author.genre === "Femme" ? "e" : author.genre ? "" : "(e)"}`;
  const deathLabel = author && `Mort${author.genre === "Femme" ? "e" : author.genre ? "" : "(e)"}`;
  // relevant networks
  const relevantNetworks = NETWORK_TYPES.filter(
    (nt) =>
      nt.fields.find((f) => f.value === "tous_les_auteurs") &&
      some(books, (b) =>
        nt.fields.every((f) => {
          const value = b[f.value];
          return value !== undefined ? (Array.isArray(value) ? value.length > 0 : true) : value !== undefined;
        }),
      ),
  );
  // render
  return (
    <>
      {!author && !error && loading && (
        <Loader message="Chargement des données de l'auteur..." className="text-center" />
      )}
      {!!error && error}
      {author && (
        <>
          <h4>
            <i className="fas fa-user mr-1" />
            <span className="highlight">{author.nom_complet}</span>
          </h4>
          <div className="description">
            {author.portrait && (
              <img
                className="cover"
                src={author.portrait}
                title={`Portrait de ${author.nom_complet}`}
                alt={`Portrait de ${author.nom_complet}`}
              />
            )}

            <div>
              {/******* GENDER + BIRTH/DEATH  *******/}
              {(author.lieuDeNaissance || author.dateDeNaissance || author.genre) && (
                <p className="values-list-coma">
                  {author.genre && <span>{author.genre}</span>}
                  {(author.dateDeNaissance || author.lieuDeNaissance) && (
                    <span>
                      {author.dateDeNaissance ? (
                        <DateHeuristComponent date={author.dateDeNaissance} prefix={birthLabel || ""} />
                      ) : (
                        birthLabel
                      )}
                      {author.lieuDeNaissance && <PlaceComponent place={author.lieuDeNaissance} prefix=" à" />}
                    </span>
                  )}
                  {(author.dateDeDeces || author.lieuDeDeces) && (
                    <span>
                      {author.dateDeDeces ? (
                        <DateHeuristComponent date={author.dateDeDeces} prefix={deathLabel || ""} />
                      ) : (
                        deathLabel
                      )}
                      {author.lieuDeDeces && <PlaceComponent place={author.lieuDeDeces} prefix=" à" />}
                    </span>
                  )}
                </p>
              )}
              {/******* NAMES  *******/}
              {author.nomDeJeuneFille && (
                <p>
                  {birthLabel} {author.nomDeJeuneFille}
                </p>
              )}
              {author.nomAuteur && <p>Variantes du nom : {author.nomAuteur.join("; ")}</p>}
              {/****  RELATIONS *******/}
              {author.personneLiee && (
                <p className="values-list-coma">
                  {author.personneLiee
                    // display only values for which we have labels
                    .filter((relation) => personneLieeLabels[relation.relation])
                    .map((relation, i: number) => (
                      <span key={i}>
                        {personneLieeLabels[relation.relation][relation.direction]}{" "}
                        <Link to={`/auteur/${relation.other.id}`}>{relation.other.nom_complet}</Link>
                      </span>
                    ))}
                </p>
              )}
              {CV.length > 0 && (
                <p>
                  <a href="#carriere">
                    <i className="fas fa-arrow-down" /> Voir les éléments de carrière
                  </a>
                </p>
              )}
            </div>
          </div>
          <hr />

          {!!books.length && (
            <>
              <BooksTimeline
                authorTimeline={{
                  birth: author.dateDeNaissance,
                  death: author.dateDeDeces,
                }}
                booksHistogram={booksHistogramData}
                marks={[]}
                getBarURL={({ year }) =>
                  dataGridUrl +
                  `&dateEdition.range.min=${encodeURIComponent(year)}&dateEdition.range.max=${encodeURIComponent(year)}`
                }
                newTab
              />
              <br />
            </>
          )}

          <h5>
            <span className="highlight">Principales valeurs pour les...</span>
          </h5>
          <div className="flex-row-large">
            <Top
              title="Éditeurs"
              items={topEditors}
              placeholder="Aucun éditeur affilié"
              linkTo={(label: string) => `${dataGridUrl}&editeurs.nomEntiteMorale=${encodeURIComponent(label)}`}
            />
            <Top
              title="Classifications"
              items={topClassifications}
              linkTo={(label: string) => `${dataGridUrl}&classification=${encodeURIComponent(label)}`}
            />
            <Top
              title="Niveaux du cours"
              items={topNiveauDuCours}
              linkTo={(label: string) => `${dataGridUrl}&niveauDuCours=${encodeURIComponent(label)}`}
            />
          </div>

          <hr />

          <h5 className="list-title">
            <span className="highlight">Publications</span>
          </h5>

          <div className="mv-1">
            {+author.nb_authored_publications > books.length && (
              <p>Seules {books.length} publications sont listées ci-dessous.</p>
            )}
            <p>
              Parcourir <Link to={dataGridUrl}>la liste des {author.nb_authored_publications} publications</Link>{" "}
              publiées par {author.nom_complet}.<br />
              Situer cet auteur, dans le réseau entre:{" "}
              {relevantNetworks.map((nt, i) => (
                <span key={nt.value}>
                  <Link to={`/reseau/${nt.value}?selected=${id}`} className="mv-1">
                    {nt.label}
                  </Link>
                  {i < relevantNetworks.length - 2 ? ", " : i !== relevantNetworks.length - 1 ? " ou " : ""}
                </span>
              ))}
            </p>
          </div>

          {books.length > 0 ? (
            <ul className="list-unstyled list-grid mv-1">
              {books.map((book: ProductionPedagogiqueEnDroit) => (
                <li key={book.id} className="result">
                  <BookComponent book={book} firstLevel={true} />
                </li>
              ))}
            </ul>
          ) : (
            <p>Aucune publication</p>
          )}

          {CV.length > 0 && (
            <>
              <hr />

              <h5 id="carriere">
                <span className="highlight">Carrière</span>
              </h5>
              <CurriculumVitae CV={CV} />
            </>
          )}

          {links.length > 0 && (
            <>
              <hr />

              <h5 id="carriere">
                <span className="highlight">Voir aussi</span>
              </h5>
              <ul className="list-unstyled mv-1">
                {links.map((link, i) => (
                  <li key={i}>
                    <a href={link}>{link}</a>{" "}
                    <a href={link} className="new-tab-link" target="blank_" rel="noopener">
                      <i className="fas fa-external-link-alt" />
                    </a>
                  </li>
                ))}
              </ul>
            </>
          )}
        </>
      )}
    </>
  );
};
