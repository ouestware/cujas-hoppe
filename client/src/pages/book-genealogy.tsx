import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { Genealogy, ProductionPedagogiqueEnDroit } from "Heurist";
import { BookGenealogyComponent } from "../components/book-genealogy";
import { Loader } from "../components/loader";
import { GenealogyType, getGenealogyFromNodeID } from "../elasticsearchClient";

export const BookGenealogy: React.FC = () => {
  // init state
  const { id } = useParams<"id">();
  const [bookGenealogy, setBookGenealogy] = useState<Genealogy<ProductionPedagogiqueEnDroit> | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  // fetch data
  useEffect(() => {
    if (id) {
      setLoading(true);
      setError(null);
      getGenealogyFromNodeID<ProductionPedagogiqueEnDroit>(id, GenealogyType.book)
        .then((data) => {
          if (data && data.nodes.find((o) => o.id === id)) {
            setBookGenealogy(data);
          } else setError(`L'identifiant de l'ourvage ${id} ne correspond à aucune généalogie.`);
        })
        .catch((error) => {
          console.error(error);
          setError(`L'identifiant de l'ouvrage ${id} ne correspond à aucune généalogie.`);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [id]);
  const focusedBook = bookGenealogy && bookGenealogy.nodes.find((p: ProductionPedagogiqueEnDroit) => p.id === id);
  const genealogyTitle = `Généalogie de ${(focusedBook && focusedBook.titre) || ""} de ${
    focusedBook && focusedBook.auteurs?.map((a) => a.nom_complet).join("; ")
  }`;

  return (
    <>
      {!bookGenealogy && !error && loading && (
        <Loader
          message="Chargement des données de la généalogie de productions pédagogiques..."
          className="text-center"
        />
      )}
      {!!error && error}
      {id && bookGenealogy && (
        <>
          <h4>
            <i className="fas fa-book mr-1" />
            <span className="highlight">{genealogyTitle}</span>
          </h4>
          <BookGenealogyComponent bookGenealogy={bookGenealogy} focusedEditorId={id} />
        </>
      )}
    </>
  );
};
