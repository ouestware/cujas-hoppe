import { compact, identity, sortBy, values } from "lodash";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

import { Organisation, Personne, ProductionPedagogiqueEnDroit } from "Heurist";
import { AuthorComponent } from "../components/author";
import { DateHeuristComponent } from "../components/date";
import { AuthorOrgComponent } from "../components/editor";
import { Loader } from "../components/loader";
import { getBook } from "../elasticsearchClient";

export const Book: React.FC = () => {
  // init state
  const { id } = useParams<"id">();
  const [book, setBook] = useState<ProductionPedagogiqueEnDroit | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const authors: (Personne | Organisation)[] | null =
    book &&
    compact(
      (book.auteurs || []).concat(
        (book.fonctionAuteur || []).map((fonctionAuteur) => ({
          ...(fonctionAuteur.other as Personne),
          fonction: fonctionAuteur.relation,
        })),
      ),
    );

  // fetch data
  useEffect(() => {
    if (id !== undefined) {
      setLoading(true);
      setError(null);
      getBook(id)
        .then((data) => {
          const { book } = data;
          setBook(book);
        })
        .catch((error) => {
          console.error(error);
          setError(`L'identifiant production pédagogique ${id} est introuvable.`);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [id]);
  const outRelationLabels: { [key: string]: string } = {
    "A pour première édition": "A pour première édition : ",
    "Edition suivante de": "Édition suivante de :",
    "Edition précédente de": "Édition précédente de :",
    "Autre édition": "Autre édition : ",
  };
  const inRelationLabels: { [key: string]: string } = {
    "A pour première édition": "Autre édition : ",
    "Edition suivante de": "Édition précédente de : ",
    "Edition précédente de": "Édition suivante de : ",
    "Autre édition": "Autre édition : ",
  };
  // deduplicate relations by linked id
  const relationsEntreEditions: Array<{
    relation: string;
    direction: string;
    other: ProductionPedagogiqueEnDroit;
  }> = sortBy(
    values(
      (book?.relationsEntreEditions || [])
        .filter(
          (r) => !["Autre édition", "A pour première édition", "Autre édition du même ouvrage"].includes(r.relation),
        )
        .reduce(
          (
            relations: { [key: string]: { relation: string; direction: string; other: ProductionPedagogiqueEnDroit } },
            relation,
          ) => {
            // don't replace in by an out
            if (relations[relation.other.id]) if (relation.direction === "out") return relations;
            return { [relation.other.id]: relation, ...relations };
          },
          {},
        ),
    ),
    (r) => (r.direction === "in" ? inRelationLabels[r.relation] : outRelationLabels[r.relation]),
  );

  return (
    <>
      {!book && !error && loading && (
        <Loader message="Chargement des données de l'ouvrage..." className="text-center" />
      )}
      {!!error && error}
      {book && (
        <>
          <h4>
            <i className="fas fa-book mr-1" />
            <span className="highlight">{book.titre}</span>
            {book.complementDeTitre && (
              <>
                <span> : </span>
                <span className="highlight">{book.complementDeTitre.join(", ")}</span>
              </>
            )}
          </h4>

          {book.mentionEdition && (
            <h5>
              <span className="highlight">{book.mentionEdition}</span>
            </h5>
          )}

          {(book.titreDEnsemble || book.titreDePartie) && (
            <h5>
              Dans :{" "}
              <span className="highlight">
                {[book.titreDEnsemble, [book.numeroDePartie, book.titreDePartie].filter(identity).join(" : ")]
                  .filter(identity)
                  .join(" - ")}
              </span>
            </h5>
          )}

          <h5 className="list-title">
            <span className="highlight">Auteurs</span>
          </h5>
          {authors && authors.length ? (
            <ul className="list-unstyled">
              {authors.map((entity: Personne | Organisation) => (
                <li key={entity.id} className="result">
                  {(entity as Personne).nom ? (
                    <AuthorComponent author={entity as Personne} firstLevel={true} />
                  ) : (
                    <AuthorOrgComponent editor={entity as Organisation} firstLevel={true} />
                  )}
                </li>
              ))}
            </ul>
          ) : (
            <p>Aucun auteur renseigné</p>
          )}

          <br />

          <p>
            <span className="caption">Édité</span>
            {book.editeurs && (
              <>
                {" "}
                par{" "}
                <span className="values-list-coma-and">
                  {book.editeurs.map((o: Organisation, i: number) => (
                    <span key={i}>
                      {o.typeOrganisme === "Editeur (personne morale)" ? (
                        <Link to={`/editeur/${o.id}`}>
                          <i className="fas fa-building mr-1" />
                          {o.nomEntiteMorale}
                        </Link>
                      ) : (
                        o.nomEntiteMorale
                      )}
                    </span>
                  ))}
                </span>
              </>
            )}
            {book.dateEdition && <DateHeuristComponent date={book.dateEdition} />}
            {book.villeEdition && (
              <>
                {" à "}
                <span className="value-inline">{book.villeEdition.map((v) => v.ville).join(", ")}</span>
              </>
            )}
          </p>

          {book.relationsEntreEditions && book.relationsEntreEditions.length > 0 && (
            <>
              <br />
              <ul className="list-unstyled">
                {relationsEntreEditions.map((r, i) => (
                  <li key={i}>
                    {r.direction === "out" ? outRelationLabels[r.relation] : inRelationLabels[r.relation]}{" "}
                    <Link to={`/ouvrage/${r.other.id}`}>{r.other.titre}</Link>
                  </li>
                ))}
              </ul>
              {book.relationsEntreEditions && (
                <>
                  Fait partie d'un traité : <Link to={`/ouvrage/${id}/genealogie`}>Généalogie complète</Link>
                </>
              )}
            </>
          )}

          {book.collection && (
            <p>
              Collection :{" "}
              <span className="value-inline">
                {book.collection.map((e, i) => (
                  <>
                    <a key={e.id} href={`/collection/${e.id}`}>
                      <i className="fas fa-lines-leaning mr-1" /> {e.nomDeLaCollection}
                    </a>
                    {book.numeroDansLaCollection && book.numeroDansLaCollection[i] && (
                      <>
                        {" "}
                        (numéro <span className="value-inline">{book.numeroDansLaCollection[i]}</span>)
                      </>
                    )}
                  </>
                ))}
              </span>
            </p>
          )}

          <hr />

          {(book.collation || book.format) && (
            <p>
              Collation : <span className="value-inline">{book.collation}</span>
              {", "}
              <span className="value-inline">{book.format}</span>
            </p>
          )}
          {book.illustrations && (
            <p>
              Illustrations : <span className="value-inline">{book.illustrations.join(", ")}</span>
            </p>
          )}

          {book.classification && (
            <p>
              Classification : <span className="value-inline">{book.classification}</span>
            </p>
          )}

          {(book.collation || book.format || book.illustrations || book.classification) && <hr />}

          {(book.dateCours || (book.niveauDuCours || []).length || book.lieuDuCours) && (
            <>
              {book.dateCours && <p>Cours {book.dateCours && <DateHeuristComponent date={book.dateCours} />}</p>}

              {(book.niveauDuCours || []).length > 0 && (
                <p>
                  Niveau : <span>{book.niveauDuCours!.join(", ")}</span>
                </p>
              )}

              {book.lieuDuCours && (
                <p>
                  Lieu : <span>{book.lieuDuCours.nomEntiteMorale}</span>
                </p>
              )}

              <hr />
            </>
          )}

          {book.cote && book.cote.length && (
            <p>
              Cote(s) Cujas : <span className="value-inline">{book.cote.join(" ; ")}</span>
            </p>
          )}

          <p>
            {book.manquant !== true && book.idNBibliographique && book.idNBibliographique.length > 0 && (
              <a
                href={`https://bcujas-catalogue.univ-paris1.fr/permalink/33CUJAS_INST/u7fiv/alma99${book.idNBibliographique[0]}0107621`}
              >
                <i className="fas fa-link mr-1" />
                <strong>Lien vers la notice Cujas</strong>
              </a>
            )}
            {book.manquant === true && (
              <span>
                <i className="fas fa-unlink mr-1" />
                <strong>Disparu des collections</strong>
              </span>
            )}
          </p>

          {book.isbn && (
            <p>
              ISBN : <span className="value-inline">{book.isbn}</span>
            </p>
          )}
          {book.issn && (
            <p>
              ISSN : <span className="value-inline">{book.issn}</span>
            </p>
          )}
          {book.ppn && (
            <p>
              PPN : <span className="value-inline">{book.ppn.join(", ")}</span>
            </p>
          )}

          {book.liens && book.liens.length > 0 && (
            <>
              <hr />

              <h5 id="carriere">
                <span className="highlight">Vers la version numérisée</span>
              </h5>

              <ul className="list-unstyled mv-1">
                {book.liens.map((lien, i) => (
                  <li key={i}>
                    <a
                      href={lien}
                      title="Lien vers la version numérisée"
                      className="new-tab-link"
                      target="blank_"
                      rel="noopener"
                    >
                      <img width="50" src="/assets/img/download-ebook.svg" alt="Ebook icon" />
                    </a>
                  </li>
                ))}
              </ul>
            </>
          )}
        </>
      )}
    </>
  );
};
