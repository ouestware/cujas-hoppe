import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

import { Collection, Organisation, Personne, ProductionPedagogiqueEnDroit } from "Heurist";
import { sortBy } from "lodash";
import { BookComponent } from "../components/book";
import { BooksTimeline, TimelineDataPoint } from "../components/books-timeline";
import { DateHeuristComponent } from "../components/date";
import { Loader } from "../components/loader";
import { Top, TopData } from "../components/top";
import { getCollection } from "../elasticsearchClient";

export const CollectionPage: React.FC = () => {
  // init state
  const { id } = useParams<"id">();
  const [collection, setCollection] = useState<Collection | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [books, setBooks] = useState<ProductionPedagogiqueEnDroit[]>([]);
  const [booksHistogramData, setBooksHistogramData] = useState<TimelineDataPoint[]>([]);

  const [toAuthors, setTopAuthors] = useState<TopData>({ count: 0, list: [] });
  const [topClassifications, setTopClassifications] = useState<TopData>({ count: 0, list: [] });
  const [topNiveauDuCours, setTopNiveauDuCours] = useState<TopData>({ count: 0, list: [] });

  // fetch data
  useEffect(() => {
    if (id !== undefined) {
      setLoading(true);
      setError(null);
      getCollection(id)
        .then((data) => {
          const { collection, books, booksHistogramData, topAuthors, topClassifications, topNiveauDuCours } = data;
          setCollection(collection);
          setBooks(books);
          setBooksHistogramData(booksHistogramData);
          setTopAuthors(topAuthors);
          setTopClassifications(topClassifications);
          setTopNiveauDuCours(topNiveauDuCours);
        })
        .catch((error) => {
          console.error(error);
          setError(`L'identifiant de la collection ${id} est introuvable.`);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [id]);

  // TOP link methods
  const dataGridurl = `/?t=book&q=*&collection.nomDeLaCollection=${encodeURIComponent(
    collection?.nomDeLaCollection || "",
  )}`;
  const outRelationLabels: { [key: string]: string } = {
    "Suite de": "Suite de :",
    Devient: "Devient :",
    "Fusionne avec": "Fusionne avec :",
    "Fusion de": "Fusion de :",
  };
  const inRelationLabels: { [key: string]: string } = {
    "Suite de": "Devient :",
    Devient: "Suite de :",
    "Fusionne avec": "Fusionne avec :",
    "Fusion de": "Devient :",
  };

  return (
    <>
      {!collection && !error && loading && (
        <Loader message="Chargement des données de la collection..." className="text-center" />
      )}
      {!!error && error}
      {collection && (
        <>
          <h4>
            <i className="fas fa-lines-leaning mr-1" />
            <span className="highlight">
              {collection.nomDeLaCollection}
              {/* {collection.acronyme ? ` (${collection.acronyme})` : ""} */}
            </span>
          </h4>
          {collection.directeurDeCollection && collection.directeurDeCollection.length && (
            <p className="mb-2">
              <span className="caption">Dirigé</span> par{" "}
              <span className="values-list-coma-and">
                {collection.directeurDeCollection.map((entity: Personne | Organisation) => (
                  <span key={entity.id}>
                    <Link to={`/auteur/${entity.id}`}>
                      <i className="fas fa-user mr-1" />
                      {entity.nom_complet}
                    </Link>
                  </span>
                ))}
              </span>
            </p>
          )}
          {collection.editeur && (
            <p className="mb-2">
              <span className="caption">Édité</span> par{" "}
              <span className="values-list-coma-and">
                {collection.editeur.map((o: Organisation, i: number) => (
                  <span key={i}>
                    {o.typeOrganisme === "Editeur (personne morale)" ? (
                      <Link to={`/editeur/${o.id}`}>
                        <i className="fas fa-building mr-1" />
                        {o.nomEntiteMorale}
                      </Link>
                    ) : (
                      o.nomEntiteMorale
                    )}
                  </span>
                ))}
              </span>
            </p>
          )}
          {(collection.dateDeCreation || collection.dateDeDisparition) && (
            <p className="values-list-dash">
              {collection.dateDeCreation && (
                <span>
                  <DateHeuristComponent prefix="Création : " date={collection?.dateDeCreation} />
                </span>
              )}
              {collection.dateDeDisparition && (
                <span>
                  <DateHeuristComponent prefix="Disparition : " date={collection?.dateDeDisparition} />
                </span>
              )}
            </p>
          )}
          {collection.relationsEntreLesCollections && (
            <p>
              <ul className="list-unstyled">
                {sortBy(collection.relationsEntreLesCollections, (s) => (s.direction === "in" ? -1 : 1)).map((s, i) => (
                  <li key={i}>
                    {s.direction === "out" ? outRelationLabels[s.relation] : inRelationLabels[s.relation] || s.relation}{" "}
                    <Link to={`/collection/${s.other.id}`}>{s.other.nomDeLaCollection}</Link>
                  </li>
                ))}
              </ul>
            </p>
          )}
          {(collection.issn || collection.ppn) && (
            <p>
              <ul className="list-unstyled mv-1">
                {collection.issn && <li>ISSN: {collection.issn}</li>}
                {collection.ppn && <li>PPN: {collection.ppn}</li>}
              </ul>
            </p>
          )}
          {collection.sousCollections && (
            <>
              <p>
                <span className="caption">Sous-collections:</span>

                <ul className="list-unstyled">
                  {collection.sousCollections.map((sc, i) => (
                    <li key={i}>- {sc}</li>
                  ))}
                </ul>
              </p>
            </>
          )}
          <hr />
          <BooksTimeline
            booksHistogram={booksHistogramData}
            marks={[]}
            getBarURL={({ year }) =>
              dataGridurl +
              `&dateEdition.range.min=${encodeURIComponent(year)}&dateEdition.range.max=${encodeURIComponent(year)}`
            }
            newTab
          />
          <br />
          <h5>
            <span className="highlight">Principales valeurs pour les...</span>
          </h5>
          <div className="flex-row-large">
            <Top
              title={"Auteurs"}
              items={toAuthors}
              linkTo={(name: string) => `${dataGridurl}&tous_les_auteurs.nom_complet=${encodeURIComponent(name)}`}
            />
            <Top
              title="Classifications"
              items={topClassifications}
              linkTo={(name: string) => `${dataGridurl}&classification=${encodeURIComponent(name)}`}
            />
            <Top
              title="Niveaux du Cours"
              items={topNiveauDuCours}
              linkTo={(name: string) => `${dataGridurl}&niveauDuCours=${encodeURIComponent(name)}`}
            />
          </div>
          <hr />
          <h5 className="list-title">
            <span className="highlight">Publications</span>
          </h5>
          {+collection.nb_publications > books.length && (
            <p className="list-subtitle">
              Seuls {books.length} publications sont listées ci-dessous. Explorer{" "}
              <Link to={dataGridurl}>les {collection.nb_publications} publications</Link> publiées dans la collection "
              {collection.nomDeLaCollection}".
            </p>
          )}
          <ul className="list-unstyled list-grid">
            {books.map((book: ProductionPedagogiqueEnDroit) => (
              <li key={book.id} className="result">
                <BookComponent book={book} firstLevel={true} />
              </li>
            ))}
          </ul>
        </>
      )}
    </>
  );
};
