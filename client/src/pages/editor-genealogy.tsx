import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { Genealogy, Organisation } from "Heurist";
import { TimelineDataPoint } from "../components/books-timeline";
import { EditorGenealogyComponent } from "../components/editor-genealogy";
import { Loader } from "../components/loader";
import { GenealogyType, getBooksHistogramsByIds, getGenealogyFromNodeID } from "../elasticsearchClient";

export const EditorGenealogy: React.FC = () => {
  // init state
  const { id } = useParams<"id">();
  const [editorGenealogy, setEditorGenealogy] = useState<Genealogy<Organisation> | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [booksHistogramsDataByEditor, setBooksHistogramsDataByEditor] = useState<{
    [key: string]: TimelineDataPoint[];
  }>({});

  // fetch data
  useEffect(() => {
    if (id !== undefined) {
      setLoading(true);
      setError(null);
      getGenealogyFromNodeID<Organisation>(id, GenealogyType.organisation)
        .then((data) => {
          if (data && data.nodes.find((o) => o.id === id)) {
            setEditorGenealogy(data);
            getBooksHistogramsByIds(
              "editeurs",
              data.nodes.map((o) => o.id),
            ).then((bookData) => {
              setBooksHistogramsDataByEditor(bookData);
            });
          } else setError(`L'identifiant de l'éditeur ${id} ne correspond à aucune généalogie.`);
        })
        .catch((error) => {
          console.error(error);
          setError(`L'identifiant de l'éditeur ${id} ne correspond à aucune généalogie.`);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [id]);

  const genealogyTitle = `Généalogie de ${
    (editorGenealogy && editorGenealogy.nodes.find((o: Organisation) => o.id === id)?.nomEntiteMorale) || ""
  }`;

  return (
    <>
      {!editorGenealogy && !error && loading && (
        <Loader message="Chargement des données de la généalogie éditeur..." className="text-center" />
      )}
      {!!error && error}
      {id && editorGenealogy && (
        <>
          <h4>
            <i className="fas fa-building mr-1" />
            <span className="highlight">{genealogyTitle}</span>
          </h4>
         {id!==undefined && <EditorGenealogyComponent
            editorGenealogy={editorGenealogy}
            focusedEditorId={id}
            booksHistogramDataByEditor={booksHistogramsDataByEditor}
          />}
        </>
      )}
    </>
  );
};
