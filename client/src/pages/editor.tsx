import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

import { Organisation, ProductionPedagogiqueEnDroit } from "Heurist";
import { some, sortBy } from "lodash";
import { BookComponent } from "../components/book";
import { BooksTimeline, TimelineDataPoint } from "../components/books-timeline";
import { DateHeuristComponent } from "../components/date";
import { Loader } from "../components/loader";
import { PlaceComponent } from "../components/place";
import { Top, TopData } from "../components/top";
import { getOrganisation } from "../elasticsearchClient";
import { NETWORK_TYPES } from "./network";

const EditorOrAuthor: React.FC<{ id: string; as: "editor" | "author" }> = (props) => {
  // init state
  const { id, as } = props;
  const [editor, setEditor] = useState<Organisation | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [books, setBooks] = useState<ProductionPedagogiqueEnDroit[]>([]);
  const [booksHistogramData, setBooksHistogramData] = useState<TimelineDataPoint[]>([]);
  const [links, setLinks] = useState<string[]>([]);
  const [topAuthorsOrEditors, setTopAuthorsOrEditors] = useState<TopData>({ count: 0, list: [] });
  const [topClassifications, setTopClassifications] = useState<TopData>({ count: 0, list: [] });
  const [topNiveauDuCours, setTopNiveauDuCours] = useState<TopData>({ count: 0, list: [] });

  // fetch data
  useEffect(() => {
    setLoading(true);
    setError(null);
    getOrganisation(id, as)
      .then((data) => {
        const { organisation, books, booksHistogramData, topAuthorsOrEditors, topClassifications, topNiveauDuCours } =
          data;
        setEditor(organisation);
        setBooks(books);
        setBooksHistogramData(booksHistogramData);
        setTopAuthorsOrEditors(topAuthorsOrEditors);
        setTopClassifications(topClassifications);
        setTopNiveauDuCours(topNiveauDuCours);
        setLinks(organisation.lienVersAutorite || []);
      })
      .catch((error) => {
        console.error(error);
        setError(`L'identifiant de l'éditeur ${id} est introuvable.`);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [id, as]);

  // TOP link methods
  const dataGridurl =
    as === "editor"
      ? `/?t=book&q=*&editeurs.nomEntiteMorale=${encodeURIComponent(editor?.nomEntiteMorale || "")}`
      : `/?t=book&q=*&tous_les_auteurs.nom_complet=${encodeURIComponent(editor?.nom_complet || "")}`;
  const outRelationLabels: { [key: string]: string } = {
    "Suite de": "Suite de :",
    Devient: "Devient :",
    "Fusionne avec": "Fusionne avec :",
    "Fusion de": "Fusion de :",
  };
  const inRelationLabels: { [key: string]: string } = {
    "Suite de": "Devient :",
    Devient: "Suite de :",
    "Fusionne avec": "Fusionne avec :",
    "Fusion de": "Devient :",
  };

  // relevant networks
  const relevantNetworks = NETWORK_TYPES.filter(
    (nt) =>
      nt.fields.find((f) => f.value === "editeurs") &&
      some(books, (b) =>
        nt.fields.every((f) => {
          const value = b[f.value];
          return value !== undefined ? (Array.isArray(value) ? value.length > 0 : true) : value !== undefined;
        }),
      ),
  );

  return (
    <>
      {!editor && !error && loading && (
        <Loader message="Chargement des données de l'éditeur..." className="text-center" />
      )}
      {!!error && error}
      {editor && (
        <>
          <h4>
            <i className="fas fa-building mr-1" />
            <span className="highlight">
              {editor.nomEntiteMorale}
              {editor.acronyme ? ` (${editor.acronyme})` : ""}
            </span>
          </h4>
          <div className="description">
            {editor.vignetteOuLogo && (
              <img
                className="cover"
                src={editor.vignetteOuLogo}
                title={`Logo de ${editor.nomEntiteMorale}`}
                alt={`Logo de ${editor.nomEntiteMorale}`}
              />
            )}

            <div>
              {editor.ville && (
                <h5 className="mv-0">
                  {editor.ville.map((ville, i) => (
                    <PlaceComponent key={i} place={ville} />
                  ))}
                </h5>
              )}

              {editor.adresse && editor.adresse.length && (
                <p className="values-list-then">
                  {editor.adresse.map((adresse, i) => (
                    <span key={i}>{adresse}</span>
                  ))}
                </p>
              )}

              <br />

              {(editor.dateDeDebut || editor.dateDeFin) && (
                <p className="values-list-dash">
                  {editor.dateDeDebut && (
                    <span>
                      <DateHeuristComponent prefix="Création : " date={editor?.dateDeDebut} />
                    </span>
                  )}
                  {editor.dateDeFin && (
                    <span>
                      <DateHeuristComponent prefix="Disparition : " date={editor?.dateDeFin} />
                    </span>
                  )}
                </p>
              )}

              {editor.natureDeLaDisparition && <p>Nature de la disparition : {editor.natureDeLaDisparition}</p>}

              {editor.structuresLiees && (
                <ul className="list-unstyled">
                  {sortBy(editor.structuresLiees, (s) => (s.direction === "in" ? -1 : 1)).map((s, i) => (
                    <li key={i}>
                      {s.direction === "out"
                        ? outRelationLabels[s.relation]
                        : inRelationLabels[s.relation] || s.relation}{" "}
                      {s.other.typeOrganisme === "Editeur (personne morale)" ? (
                        <Link to={`/editeur/${s.other.id}`}>{s.other.nomEntiteMorale}</Link>
                      ) : (
                        <span>{s.other.nomEntiteMorale}</span>
                      )}
                    </li>
                  ))}
                </ul>
              )}
              {editor.structuresLiees && <Link to={`/editeur/${id}/genealogie`}>Généalogie complète</Link>}

              <br />

              {editor.varianteDuNom && editor.varianteDuNom.length && (
                <>
                  <p>Variante(s) du nom :</p>
                  <ul className="list-unstyled">
                    {editor.varianteDuNom.map((name, i) => (
                      <li key={i}>{name}</li>
                    ))}
                  </ul>
                </>
              )}
            </div>
          </div>

          <hr />

          <BooksTimeline
            booksHistogram={booksHistogramData}
            marks={[]}
            getBarURL={({ year }) =>
              dataGridurl +
              `&dateEdition.range.min=${encodeURIComponent(year)}&dateEdition.range.max=${encodeURIComponent(year)}`
            }
            newTab
          />
          <br />

          <h5>
            <span className="highlight">Principales valeurs pour les...</span>
          </h5>
          <div className="flex-row-large">
            <Top
              title={as === "editor" ? "Auteurs" : "Éditeurs"}
              items={topAuthorsOrEditors}
              linkTo={(name: string) =>
                as === "editor"
                  ? `${dataGridurl}&tous_les_auteurs.nom_complet=${encodeURIComponent(name)}`
                  : `${dataGridurl}&editeurs.nomEntiteMorale=${encodeURIComponent(name)}`
              }
            />
            <Top
              title="Classifications"
              items={topClassifications}
              linkTo={(name: string) => `${dataGridurl}&classification=${encodeURIComponent(name)}`}
            />
            <Top
              title="Niveaux du Cours"
              items={topNiveauDuCours}
              linkTo={(name: string) => `${dataGridurl}&niveauDuCours=${encodeURIComponent(name)}`}
            />
          </div>
          <hr />
          <h5 className="list-title">
            <span className="highlight">Publications</span>
          </h5>

          <div className="mv-1">
            {+editor.nb_authored_publications > books.length && (
              <p>Seules {books.length} publications sont listées ci-dessous.</p>
            )}
            <p>
              Parcourir <Link to={dataGridurl}>la liste des {editor.nb_edited_publications} publications</Link> éditées
              par {editor.nomEntiteMorale}.<br />
              Situer cet éditeur, dans le réseau entre:{" "}
              {relevantNetworks.map((nt, i) => (
                <span key={nt.value}>
                  <Link to={`/reseau/${nt.value}?selected=${id}`} className="mv-1">
                    {nt.label}
                  </Link>
                  {i < relevantNetworks.length - 2 ? ", " : i !== relevantNetworks.length - 1 ? " ou " : ""}
                </span>
              ))}
            </p>
          </div>

          <ul className="list-unstyled list-grid">
            {books.map((book: ProductionPedagogiqueEnDroit) => (
              <li key={book.id} className="result">
                <BookComponent book={book} firstLevel={true} />
              </li>
            ))}
          </ul>
          {editor.note && (
            <>
              <hr />

              <h5 id="note">
                <span className="highlight">Note</span>
              </h5>
              <p dangerouslySetInnerHTML={{ __html: editor.note }}></p>
            </>
          )}

          {links.length > 0 && (
            <>
              <hr />

              <h5 id="carriere">
                <span className="highlight">Voir aussi</span>
              </h5>
              <ul className="list-unstyled mv-1">
                {links.map((link, i) => (
                  <li key={i}>
                    <a href={link}>{link}</a>{" "}
                    <a href={link} className="new-tab-link" target="blank_" rel="noopener">
                      <i className="fas fa-external-link-alt" />
                    </a>
                  </li>
                ))}
              </ul>
            </>
          )}
        </>
      )}
    </>
  );
};

export const Editor: React.FC = () => {
  const { id } = useParams<"id">();
  return id !== undefined ? <EditorOrAuthor id={id} as={"editor"} /> : null;
};
export const OrgAuthor: React.FC = () => {
  const { id } = useParams<"id">();
  if (id !== undefined) return <EditorOrAuthor id={id} as={"author"} />;
  else return null;
};
