import { FC } from "react";

import { CMSEntry } from "../components/cms-entry";
import config from "../config";

export const FAQPage: FC = () => (
  <div className="faq-page">
    <h1>
      <i className="fas fa-question-circle mr-1" />
      <span className="highlight">Foire Aux Questions</span>
    </h1>

    <br />

    <CMSEntry id={config.cms.faq} />
  </div>
);
