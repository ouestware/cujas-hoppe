import { keyBy } from "lodash";

import { OptionType } from "../../components/custom-select";
import { getTerms } from "../../elasticsearchClient";
import { ESSearchQueryContext, FilterType, PlainObject } from "../../types";

/**
 * Helper to get the properly typed function to retrieve options for a given
 * TERMS field.
 */
function asyncOptionsFactory(
  field: string,
  count: number = 200,
): (inputValue: string, context: ESSearchQueryContext) => Promise<OptionType[]> {
  return async (inputValue: string, context: ESSearchQueryContext) =>
    getTerms(context, field, inputValue, count + 1).then((terms) => [
      ...terms.map(({ term }) => ({ label: term, value: term })),
      ...(terms.length > count
        ? [
            {
              value: "HOPPE::DISABLED",
              label: "Pour voir plus de résultats, veuillez affiner votre rechercher",
              isDisabled: true,
            },
          ]
        : []),
    ]);
}

/**
 * Helper to get a full filter definition more the most basic case: a TERMS
 * field with multi-values enabled:
 */
function getBasicTermsFilter(field: string, label?: string): FilterType {
  return {
    id: field,
    type: "terms",
    label: label || field,
    isMulti: true,
    asyncOptions: asyncOptionsFactory(field),
  };
}

/**
 * Helper to get a full filter definition more the most basic case: a DATES
 * field with multi-values enabled:
 */
function getBasicDatesFilter(field: string, label?: string): FilterType {
  return {
    id: field,
    type: "dates",
    label: label || field,
  };
}

const bookFilters: FilterType[] = [
  getBasicTermsFilter("tous_les_auteurs.nom_complet", "Auteur"),
  getBasicTermsFilter("editeurs.nomEntiteMorale", "Éditeur"),
  getBasicDatesFilter("dateEdition.range", "Date d'édition"),
  getBasicTermsFilter("collection.nomDeLaCollection", "Collection"),
  getBasicTermsFilter("classification", "Classification"),
  getBasicTermsFilter("niveauDuCours", "Niveau du cours"),
  getBasicTermsFilter("lieuDuCours.nomEntiteMorale", "Lieu du cours"),
  getBasicDatesFilter("dateCours.range", "Date du cours"),
  getBasicTermsFilter("format", "Format"),
  getBasicTermsFilter("illustrations", "Illustrations"),
];
const bookFiltersDict: PlainObject<FilterType> = keyBy(bookFilters, "id");

const authorFilters: FilterType[] = [
  getBasicTermsFilter("lieuDeNaissance.pays", "Pays de naissance"),
  getBasicTermsFilter("lieuDeNaissance.ville", "Ville de naissance"),
  getBasicTermsFilter("lieuDeDeces.pays", "Pays de décès"),
  getBasicTermsFilter("lieuDeDeces.ville", "Ville de décès"),
];
const authorFiltersDict: PlainObject<FilterType> = keyBy(authorFilters, "id");
const editorFilters: FilterType[] = [
  getBasicTermsFilter("ville.ville", "Ville"),
  getBasicTermsFilter("ville.pays", "Pays"),
];
const editorFiltersDict: PlainObject<FilterType> = keyBy(editorFilters, "id");
const collectionFilters: FilterType[] = [
  getBasicTermsFilter("editeur.nomEntiteMorale", "Éditeur"),
  getBasicTermsFilter("directeurDeCollection.nom_complet", "Directeur"),
];
const collectionFiltersDict: PlainObject<FilterType> = keyBy(collectionFilters, "id");

const editorGenealogyFilters: FilterType[] = [
  getBasicTermsFilter("nodes.natureDeLaDisparition", "Nature de la Disparition"),
];
const editorGenealogyFiltersDict: PlainObject<FilterType> = keyBy(editorGenealogyFilters, "id");

const bookGenealogyFilters: FilterType[] = [
  getBasicTermsFilter("nodes.tous_les_auteurs.nom_complet", "Auteur"),
  getBasicTermsFilter("nodes.editeurs.nomEntiteMorale", "Éditeur"),
  getBasicDatesFilter("nodes.dateEdition.range", "Date d'édition"),
  getBasicTermsFilter("nodes.collection.nomDeLaCollection", "Collection"),
  getBasicTermsFilter("nodes.classification", "Classification"),
];
const bookGenealogyFiltersDict: PlainObject<FilterType> = keyBy(bookGenealogyFilters, "id");

export const filtersByType: PlainObject<FilterType[]> = {
  book: bookFilters,
  author: authorFilters,
  editor: editorFilters,
  editorGenealogy: editorGenealogyFilters,
  bookGenealogy: bookGenealogyFilters,
  collection: collectionFilters,
};
export const filtersDictByType: PlainObject<PlainObject<FilterType>> = {
  book: bookFiltersDict,
  author: authorFiltersDict,
  editor: editorFiltersDict,
  editorGenealogy: editorGenealogyFiltersDict,
  bookGenealogy: bookGenealogyFiltersDict,
  collection: collectionFiltersDict,
};
