import cx from "classnames";
import { compact, constant, isEmpty, isUndefined, keyBy, mapValues, omit, omitBy, without } from "lodash";
import { FC, useEffect, useRef, useState } from "react";

import {
  AsyncCreatableSelect,
  CreatableSelect,
  OptionType,
  objectToStringValue,
  stringToObjectValue,
} from "../../components/custom-select";
import { Loader } from "../../components/loader";
import config from "../../config";
import { getHistograms } from "../../elasticsearchClient";
import {
  DatesFilterState,
  DatesFilterType,
  ESSearchQueryContext,
  FilterHistogramType,
  FilterType,
  FiltersState,
  PlainObject,
  SearchTypeDefinition,
  TermsFilterState,
  TermsFilterType,
} from "../../types";
import { filtersByType } from "./filters-specs";

function getArrayValue(value: string | string[] | undefined): string[] {
  return Array.isArray(value) ? value : compact([value]);
}

const TermsFilter: FC<{
  filter: TermsFilterType;
  state: TermsFilterState;
  histogram?: FilterHistogramType;
  setState: (newState: TermsFilterState | null) => void;
  context: ESSearchQueryContext;
}> = ({ filter, state, setState, context, histogram }) => {
  const remainingCount = histogram ? histogram.total - histogram.values.length : 0;
  const valuesDict: PlainObject<boolean> = mapValues(keyBy(getArrayValue(state.value)), constant(true));

  // TODO:
  // Find a better way to invalidate cache.
  // Indeed, here, the cache must be invalidated when the given context
  // changes. But react-select does not provide a way to invalidate cache on
  // **the current input**, only on previous/upcoming inputs.
  return (
    <div className="filter-block">
      <h5>
        <span className="highlight">{filter.label}</span>
      </h5>
      <div>
        {filter.asyncOptions ? (
          <AsyncCreatableSelect
            key={JSON.stringify(context)}
            loadOptions={(inputValue: string) => filter.asyncOptions && filter.asyncOptions(inputValue, context)}
            value={stringToObjectValue(state.value)}
            isMulti={!!filter.isMulti}
            placeholder={"Rechercher une valeur..."}
            noOptionsMessage={() => "Aucune option disponible dans les filtres actuels"}
            defaultOptions
            onChange={(value) =>
              setState({ type: "terms", value: objectToStringValue(value as OptionType | OptionType[]) })
            }
          />
        ) : (
          <CreatableSelect options={filter.options} value={stringToObjectValue(state.value)} isMulti={filter.isMulti} />
        )}
      </div>
      {histogram ? (
        <ul className="list-unstyled">
          {histogram.values.map(({ label, count }, i) => (
            <li key={i}>
              <div
                className="flex mv-1 filter-bar-container"
                onClick={(e) => {
                  valuesDict[label]
                    ? setState({ type: "terms", value: without(getArrayValue(state.value), label) })
                    : setState({ type: "terms", value: getArrayValue(state.value).concat([label]) });
                  e.preventDefault();
                }}
              >
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a href="#">{valuesDict[label] ? <strong>{label}</strong> : label}</a>
                <span className="sm grey fg ml-1">({count})</span>
                <button className="btn btn-light inline sm">
                  <i className={cx("fas mr-1", valuesDict[label] ? "fa-times" : "fa-plus")} />
                  {valuesDict[label] ? "Retirer" : "Filtrer"}
                </button>
              </div>
              <div className="sub-bar">
                <div style={{ width: (count / (histogram?.maxCount || 1)) * 100 + "%" }} />
              </div>
            </li>
          ))}
          {!!remainingCount && (
            <li className="sm grey">
              <i>
                ...et {remainingCount} autre{remainingCount > 1 ? "s" : ""} valeur{remainingCount > 1 ? "s" : ""}
              </i>
            </li>
          )}
        </ul>
      ) : (
        <Loader tag="h6" className="mv-1" />
      )}
    </div>
  );
};

const DatesFilter: FC<{
  filter: DatesFilterType;
  state: DatesFilterState;
  setState: (newState: DatesFilterState | null) => void;
  context: ESSearchQueryContext;
}> = ({ filter, state, setState }) => {
  const boundaries = {
    min: config.minYear,
    max: config.maxYear,
  };

  const [value, setValue] = useState<{ min: number; max: number }>({ ...boundaries, ...state.value });

  useEffect(() => {
    setValue({ ...boundaries, ...state.value });
    // eslint-disable-next-line
  }, [state]);

  function submit() {
    const min = Math.max(boundaries.min, Math.min(value.min, boundaries.max));
    const max = Math.max(min, Math.min(value.max, boundaries.max));
    setState({
      type: "dates",
      value: omitBy(
        { min: min === boundaries.min ? undefined : min, max: max === boundaries.max ? undefined : max },
        isUndefined,
      ),
    });
  }

  return (
    <div className="filter-block">
      <h5>
        <span className="highlight">{filter.label}</span>
      </h5>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          submit();
        }}
      >
        <div className="flex">
          <label style={{ width: "2em" }} className="mr-2" htmlFor={`${filter.id}-min-input`}>
            De
          </label>
          <div className="fg">
            <input
              id={`${filter.id}-min-input`}
              className="w100"
              type="number"
              step="1"
              min={boundaries.min}
              max={value.max || boundaries.max}
              value={value.min || boundaries.min}
              placeholder={boundaries.min + ""}
              onChange={(e) => setValue({ min: +e.target.value, max: value.max })}
            />
          </div>
        </div>
        <div className="flex mv-1">
          <label style={{ width: "2em" }} className="mr-2" htmlFor={`${filter.id}-max-input`}>
            À
          </label>{" "}
          <div className="fg">
            <input
              id={`${filter.id}-max-input`}
              className="w100"
              type="number"
              step="1"
              min={value.min || boundaries.min}
              max={boundaries.max}
              value={value.max || boundaries.max}
              placeholder={boundaries.max + ""}
              onChange={(e) => setValue({ max: +e.target.value, min: value.min })}
            />
          </div>
        </div>
        <button
          className="btn btn-sm"
          type="submit"
          disabled={value.min === state.value.min && value.max === state.value.max}
        >
          Valider
        </button>
      </form>
    </div>
  );
};

export const Filters: FC<{
  // Filters state management:
  state: FiltersState;
  setState: (newState: FiltersState) => void;
  // Surrounding context:
  query: string;
  searchTypeDefinition: SearchTypeDefinition;
}> = (props) => {
  const filters: FilterType[] = filtersByType[props.searchTypeDefinition.queryType] || [];
  const context: ESSearchQueryContext = {
    query: props.query,
    filters: props.state,
    index: props.searchTypeDefinition.index,
    sort: null,
  };
  const contextFingerprint = JSON.stringify(context);

  const [histograms, setHistograms] = useState<PlainObject<FilterHistogramType>>({});
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isSticky, setIsSticky] = useState<boolean>(false);
  const maxCount = Math.max(
    ...Object.values(histograms).flatMap((histogram) => histogram.values.map((val) => val.count)),
  );

  // Load histograms when context changes:
  useEffect(() => {
    if (!isLoading) {
      setHistograms({});
      setIsLoading(true);
      getHistograms(
        context,
        filters.map((filter) => filter.id),
        5,
      ).then((value) => {
        setIsLoading(false);
        setHistograms(value);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contextFingerprint]);

  // Listen to scroll to detect when block becomes sticked:
  const root = useRef<HTMLDivElement>(null);
  function checkScroll() {
    const top = root.current?.offsetTop;
    const newIsSticky = typeof top === "number" && top !== 0;

    if (newIsSticky !== isSticky) setIsSticky(newIsSticky);
  }
  useEffect(() => {
    window.addEventListener("scroll", checkScroll);
    return function cleanup() {
      window.removeEventListener("scroll", checkScroll);
    };
  });

  return (
    <div className={cx("filters", isSticky && "sticky")} ref={root}>
      <h4>
        <span className="highlight">
          <i className="fas fa-filter mr1" /> Filtres
        </span>
      </h4>
      {filters.map((filter, i) => {
        if (filter.type === "terms")
          return (
            <TermsFilter
              key={i}
              filter={filter as TermsFilterType}
              setState={(newState) =>
                props.setState(
                  newState && (newState.value as string[]).length
                    ? { ...props.state, [filter.id]: newState }
                    : omit(props.state, filter.id),
                )
              }
              histogram={histograms[filter.id] && { ...histograms[filter.id], maxCount }}
              state={(props.state[filter.id] || { type: "terms", value: [] }) as TermsFilterState}
              context={context}
            />
          );
        if (filter.type === "dates")
          return (
            <DatesFilter
              key={i}
              filter={filter as DatesFilterType}
              setState={(newState) =>
                props.setState(
                  newState && !isEmpty(newState.value)
                    ? { ...props.state, [filter.id]: newState }
                    : omit(props.state, filter.id),
                )
              }
              state={(props.state[filter.id] || { type: "dates", value: [] }) as DatesFilterState}
              context={context}
            />
          );
        return null;
      })}
    </div>
  );
};
