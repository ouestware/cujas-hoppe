import { FC, useState } from "react";
import { useNavigate } from "react-router-dom";

import { SearchType, SearchTypeDefinition } from "../../types";
import { DEFAULT_SEARCH_TYPE, SEARCH_TYPES, SEARCH_TYPES_DICT } from "./searchTypes";
import { getDatagridURL } from "./utils";

const Form: FC<{ initialQuery: string; initialSearchType: SearchTypeDefinition }> = (props) => {
  const navigate = useNavigate();
  const [searchType, setSearchType] = useState<SearchType>(props.initialSearchType?.queryType || DEFAULT_SEARCH_TYPE);
  const fullSearchType = SEARCH_TYPES_DICT[searchType];
  const [query, setQuery] = useState<string>(props.initialQuery || "");

  return (
    <form
      action="/"
      id="search-form"
      onSubmit={(e) => {
        e.preventDefault();
        if (fullSearchType && query) {
          navigate(getDatagridURL(searchType, query));
        }
      }}
      onReset={() => {
        setSearchType(DEFAULT_SEARCH_TYPE);
        setQuery("");
        navigate("/");
      }}
    >
      <h1>
        <span className="highlight">Je recherche...</span>
      </h1>
      <div>
        <span className="custom-select">
          <select
            name="t"
            value={searchType || ""}
            onChange={(e) => {
              setSearchType(e.target.value as SearchType);
            }}
          >
            {SEARCH_TYPES.map(({ queryType, label }) => (
              <option value={queryType} key={queryType}>
                {label}
              </option>
            ))}
          </select>
        </span>
        <input
          name="q"
          type="text"
          value={query || ""}
          onChange={(e) => setQuery(e.target.value)}
          placeholder={fullSearchType?.placeholder || ""}
        />
        <button className="btn btn-img" type="reset" title="Annuler la recherche">
          <i className="fas fa-times" />
        </button>
        <button className="btn btn-img" type="submit" title="Rechercher" disabled={!query}>
          <i className="fas fa-search" />
        </button>
      </div>
    </form>
  );
};

export default Form;
