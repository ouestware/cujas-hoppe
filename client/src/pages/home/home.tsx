import cx from "classnames";
import { omit } from "lodash";
import { useCallback, useEffect, useState } from "react";
import { useLocation } from "react-router";
import { useNavigate } from "react-router-dom";

import { BooksTimeline, TimelineDataPoint } from "../../components/books-timeline";
import { CMSEntry } from "../../components/cms-entry";
import { Loader } from "../../components/loader";
import { SearchSort } from "../../components/search-sort";
import { StickyWrapper } from "../../components/sticky-wrapper";
import config from "../../config";
import { getBooksHistogramsByIds, search } from "../../elasticsearchClient";
import { useStateUrl } from "../../hooks/state-url";
import {
  DatesFilterState,
  FiltersState,
  FilterType,
  PlainObject,
  SearchType,
  SearchTypeDefinition,
  SortType,
} from "../../types";
import { Filters } from "./filters";
import { filtersDictByType } from "./filters-specs";
import Form from "./form";
import { ResultsList } from "./results-list";
import { DEFAULT_SEARCH_TYPE, SEARCH_TYPES_DICT } from "./searchTypes";
import { getDatagridURL, SEARCH_QUERY_KEY, SEARCH_TYPE_KEY, SEPARATOR, SIZE } from "./utils";

function getFiltersState(query: URLSearchParams, filtersSpecs: PlainObject<FilterType>): FiltersState {
  const state: FiltersState = {};

  for (const [key, value] of query.entries()) {
    if (key === SEARCH_QUERY_KEY || key === SEARCH_TYPE_KEY || key === "preview") continue;

    // Extract field id from key (to deal with `dateEdition.range.min=XXX` for instance):
    let field = key;
    let param = null;
    if (key.endsWith(".min")) {
      field = key.split(".min")[0];
      param = "min";
    }
    if (key.endsWith(".max")) {
      field = key.split(".max")[0];
      param = "max";
    }
    const filter = filtersSpecs[field];

    if (!filter) continue;

    if (filter.type === "terms") {
      if (state[key]) {
        state[key] = { type: "terms", value: (state[key].value as string[]).concat(value.split(SEPARATOR)) };
      } else {
        state[key] = { type: "terms", value: value.split(SEPARATOR) };
      }
    }

    if (filter.type === "dates" && (param === "min" || param === "max") && parseInt(value) + "" === value) {
      state[field] = { type: "dates", value: { ...(state[field]?.value || {}), [param]: +value } } as DatesFilterState;
    }
  }

  return state;
}

/**
 * Get the search definition for the type of search.
 * Per default, it returns the one for the default search.
 */
function getSearchDefinition(searchType: string): SearchTypeDefinition {
  let searchDef = SEARCH_TYPES_DICT[DEFAULT_SEARCH_TYPE];
  if (searchType && Object.keys(SEARCH_TYPES_DICT).includes(searchType)) {
    searchDef = SEARCH_TYPES_DICT[searchType];
  }
  return searchDef;
}

/**
 * Get the default sort object for the specified search type.
 * /!\ If search definition has no default sort, an error is throw
 */
function getSortDefinition(searchType: string, sort?: string): SortType {
  const searchDef = getSearchDefinition(searchType);
  let sortObj = searchDef.sorts.find((e) => e.label === sort);
  // if not found, we search the default one
  if (!sortObj) {
    sortObj = searchDef.sorts.find((e) => e.default === true);
    if (!sortObj) throw new Error("A search type must have a default sort option");
  }
  return sortObj;
}

export const Home: React.FC = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const queryParams = new URLSearchParams(location.search);

  // State from URL, determines whether or not data should be loaded:
  const registeredQuery = queryParams.get(SEARCH_QUERY_KEY) as string;
  const registeredSearchType = queryParams.get(SEARCH_TYPE_KEY) as SearchType;
  const fullRegisteredSearchType = SEARCH_TYPES_DICT[registeredSearchType || ""];
  const shouldSearch = !!registeredQuery && !!fullRegisteredSearchType;
  const [sort, setSort] = useStateUrl<string>("sort", getSortDefinition(registeredSearchType).label);

  // Top page form state (unplugged to search until form is submitted):

  const [loading, setLoading] = useState<boolean>(false);
  const [isNotOnTop, setIsNotOnTop] = useState<boolean>(false);
  const [filtersState, setFiltersState] = useState<FiltersState>(
    getFiltersState(queryParams, filtersDictByType[registeredSearchType]),
  );

  const [results, setResults] = useState<{ list: PlainObject[]; total: number } | null>(null);
  const [histogram, setHistogram] = useState<TimelineDataPoint[] | null>(null);
  const [booksHistogramsDataByResult, setBooksHistogramsDataByResult] = useState<{
    [key: string]: TimelineDataPoint[];
  }>({});

  useEffect(() => {
    const currentFilterState = getFiltersState(
      new URLSearchParams(location.search),
      filtersDictByType[registeredSearchType],
    );
    setFiltersState(currentFilterState);
    if (shouldSearch) {
      setLoading(true);
      setResults(null);
      setHistogram(null);
      setBooksHistogramsDataByResult({});
      search(
        {
          index: fullRegisteredSearchType.index,
          query: registeredQuery,
          filters: currentFilterState,
          sort: getSortDefinition(registeredSearchType, sort),
          hardFilter: fullRegisteredSearchType.hardFilter,
        },
        fullRegisteredSearchType.cleanFn,
        0,
        SIZE,
        registeredSearchType === "book" ? "dateEdition.date" : undefined,
      ).then((newResults) => {
        setLoading(false);
        setResults(omit(newResults, "histogram"));
        setHistogram(newResults.histogram || null);
        if (fullRegisteredSearchType.fieldInProduction) {
          getBooksHistogramsByIds(
            fullRegisteredSearchType.fieldInProduction,
            newResults.list.map((r) => r.id),
          ).then((newBooksHistogramsData) =>
            setBooksHistogramsDataByResult({ ...booksHistogramsDataByResult, ...newBooksHistogramsData }),
          );
        }
      });
    }
  }, [location.search]); // eslint-disable-line

  /**
   * This function checks if the page is scrolled to the bottom (or near the
   * bottom), and, if there is no data loading and there are more results to
   * fetch, it will load the next N results.
   */
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const checkScroll = useCallback(() => {
    const isNearBottom = window.scrollY + window.innerHeight > document.body.offsetHeight - 500;
    setIsNotOnTop(window.scrollY > window.innerHeight);

    if (isNearBottom && !loading && results && results.list.length < results.total) {
      setLoading(true);
      search(
        {
          index: fullRegisteredSearchType.index,
          query: registeredQuery,
          filters: filtersState,
          sort: getSortDefinition(registeredSearchType, sort),
        },
        fullRegisteredSearchType.cleanFn,
        results.list.length,
        SIZE,
      ).then((newResults) => {
        setLoading(false);
        setResults({ total: newResults.total, list: results.list.concat(newResults.list) });
        if (fullRegisteredSearchType.fieldInProduction) {
          getBooksHistogramsByIds(
            fullRegisteredSearchType.fieldInProduction,
            newResults.list.map((r) => r.id),
          ).then((newBooksHistogramsData) =>
            setBooksHistogramsDataByResult({ ...booksHistogramsDataByResult, ...newBooksHistogramsData }),
          );
        }
      });
    }
  }, [
    fullRegisteredSearchType,
    registeredQuery,
    filtersState,
    registeredSearchType,
    sort,
    loading,
    results,
    booksHistogramsDataByResult,
  ]);

  // Check scroll on window scroll:
  useEffect(() => {
    window.addEventListener("scroll", checkScroll);
    return function cleanup() {
      window.removeEventListener("scroll", checkScroll);
    };
  }, [checkScroll]);

  return (
    <div className={cx("homepage", shouldSearch && "expanded")}>
      <Form initialQuery={registeredQuery} initialSearchType={fullRegisteredSearchType} />

      {shouldSearch && (
        <>
          <div className="contents">
            <StickyWrapper className="side-bar">
              <SearchSort
                value={getSortDefinition(registeredSearchType, sort)}
                options={getSearchDefinition(registeredSearchType).sorts}
                onChange={(value) => {
                  setSort(value.label);
                }}
              />
              <Filters
                state={filtersState}
                setState={(newFiltersState) => {
                  navigate(getDatagridURL(registeredSearchType, registeredQuery, newFiltersState));
                }}
                query={registeredQuery}
                searchTypeDefinition={fullRegisteredSearchType}
              />
            </StickyWrapper>

            <div className="results-list">
              {results ? (
                <ResultsList
                  type={registeredSearchType}
                  list={results.list}
                  booksHistogramsDataByResult={booksHistogramsDataByResult}
                  total={results.total}
                  loading={loading}
                  query={{
                    index: fullRegisteredSearchType.index,
                    query: registeredQuery,
                    filters: filtersState,
                    sort: getSortDefinition(registeredSearchType, sort),
                  }}
                >
                  {histogram && !!results && !!results.total && (
                    <>
                      <BooksTimeline
                        booksHistogram={histogram}
                        marks={[]}
                        boundaries={filtersState.dateEdition && (filtersState.dateEdition as DatesFilterState).value}
                        onSelectionEnd={(from: TimelineDataPoint | null, to: TimelineDataPoint | null) => {
                          const urlQueryParams = new URLSearchParams(window.location.search);
                          if (from !== null && to !== null) {
                            urlQueryParams.set("dateEdition.range.min", from.year);
                            urlQueryParams.set("dateEdition.range.max", to.year);
                          } else {
                            urlQueryParams.delete("dateEdition.range.min");
                            urlQueryParams.delete("dateEdition.range.max");
                          }
                          navigate({ search: `?${urlQueryParams.toString()}` });
                        }}
                      />
                      <br />
                    </>
                  )}
                </ResultsList>
              ) : (
                <Loader />
              )}
            </div>
            <div
              className={cx("scroll-to-top", isNotOnTop && "show")}
              onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
            >
              <i className="fas fa-arrow-up" />
              <br />
              Retour au début de la liste
            </div>
          </div>
        </>
      )}
      {!shouldSearch && (
        <div className="welcome">
          <CMSEntry id={config.cms.welcome} hideLoader={true} />
        </div>
      )}
    </div>
  );
};
