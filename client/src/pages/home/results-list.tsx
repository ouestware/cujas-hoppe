import { isNil } from "lodash";
import { FC, PropsWithChildren } from "react";

import { Collection, Genealogy, Organisation, Personne, ProductionPedagogiqueEnDroit } from "Heurist";
import { AuthorComponent } from "../../components/author";
import { BookComponent } from "../../components/book";
import { BookGenealogyResultComponent } from "../../components/book-genealogy-result";
import { TimelineDataPoint } from "../../components/books-timeline";
import { CollectionComponent } from "../../components/collection";
import { AuthorOrgComponent, EditorComponent } from "../../components/editor";
import { EditorGenealogyResultComponent } from "../../components/editor-genealogy-result";
import { Loader } from "../../components/loader";
import { SearchExportButton } from "../../components/search-export-btn";
import { ESSearchQueryContext, Organisation_genealogy, PlainObject, SearchType } from "../../types";

type Props = {
  loading: boolean;
  type: SearchType;
  total: number;
  list: PlainObject[];
  query: ESSearchQueryContext;
  booksHistogramsDataByResult: { [key: string]: TimelineDataPoint[] };
};

export const ResultsList: FC<PropsWithChildren & Props> = ({
  type,
  list,
  total,
  loading,
  booksHistogramsDataByResult,
  children,
  query,
}) => {
  let listDom: JSX.Element[] = [];
  if (list.length > 0) {
    if (type === "author" && list[0].type && (list[0].type === "personne" || list[0].type === "organisation"))
      listDom = (list as (Personne | Organisation)[]).map((author) => (
        <li key={author.id} className="result">
          {"nom" in author ? (
            <AuthorComponent
              author={author}
              firstLevel={true}
              showBooksHistogram={true}
              booksHistogramData={booksHistogramsDataByResult[author.id]}
            />
          ) : (
            <AuthorOrgComponent
              editor={author}
              firstLevel={true}
              showBooksHistogram={true}
              booksHistogramData={booksHistogramsDataByResult[author.id]}
            />
          )}
        </li>
      ));
    if (type === "book" && list[0].type && list[0].type === "productionPedagogiqueEnDroit")
      listDom = (list as ProductionPedagogiqueEnDroit[]).map((book) => (
        <li key={book.id} className="result">
          <BookComponent book={book} firstLevel={true} />
        </li>
      ));
    if (type === "editor" && !isNil(list[0].nb_edited_publications))
      listDom = (list as Organisation[]).map((editor) => (
        <li key={editor.id} className="result">
          <EditorComponent
            editor={editor}
            firstLevel={true}
            showBooksHistogram={true}
            booksHistogramData={booksHistogramsDataByResult[editor.id]}
          />
        </li>
      ));
    if (type === "collection" && !isNil(list[0].nb_publications))
      listDom = (list as Collection[]).map((collection) => (
        <li key={collection.id} className="result">
          <CollectionComponent
            collection={collection}
            firstLevel={true}
            showBooksHistogram={true}
            booksHistogramData={booksHistogramsDataByResult[collection.id]}
          />
        </li>
      ));
    if (type === "editorGenealogy" && !isNil(list[0].nb_editeurs))
      listDom = (list as Organisation_genealogy[]).map((genealogy, i) => (
        <li
          key={`${genealogy.nodes.length}-${genealogy.edges.length}-${i}`}
          className="result"
          style={{ marginBottom: "1em" }}
        >
          <EditorGenealogyResultComponent editorGenealogy={genealogy} />
        </li>
      ));
    if (type === "bookGenealogy" && !isNil(list[0].nb_editions))
      listDom = (list as Genealogy<ProductionPedagogiqueEnDroit>[]).map((genealogy, i) => (
        <li
          key={`${genealogy.nodes.length}-${genealogy.edges.length}-${i}`}
          className="result"
          style={{ marginBottom: "1em" }}
        >
          <BookGenealogyResultComponent bookGenealogy={genealogy} />
        </li>
      ));
  }

  return (
    <>
      <div className="results-list-header">
        <h4>
          <span className="highlight">
            <i className="fas fa-list-ul mr-1" /> {total || "Aucun"} résultat{total > 1 ? "s" : ""}
          </span>
        </h4>
        {total > 0 && (
          <SearchExportButton
            title="Exporter le résultat de la recherche en CSV"
            filename={`export-${query.index}-${query.query}.csv`}
            query={query}
          />
        )}
      </div>
      <div className="results-list-content">
        {children}
        <ul>
          {loading ? (
            <>
              <hr />
              <Loader />
            </>
          ) : (
            <>{listDom}</>
          )}
        </ul>
      </div>
    </>
  );
};
