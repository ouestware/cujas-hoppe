import { Organisation, ProductionPedagogiqueEnDroit } from "Heurist";
import { keyBy } from "lodash";
import { esObjectToHeurist } from "../../elasticsearchClient";
import { SearchType, SearchTypeDefinition } from "../../types";

export const DEFAULT_SEARCH_TYPE: SearchType = "book";

export const SEARCH_TYPES: SearchTypeDefinition[] = [
  {
    queryType: "book",
    index: "productionpedagogiqueendroit",
    label: "Dans les ouvrages",
    placeholder: "*, Droit, économie, international, public...",
    cleanFn: esObjectToHeurist,
    sorts: [
      { label: "Pertinence", expression: { _score: "desc" }, default: true },
      { label: "Titre A->Z", expression: { "titre.raw": "asc" } },
      { label: "Titre Z->A", expression: { "titre.raw": "desc" } },
      { label: "Date d'édition croissante", expression: { "dateEdition.date": "asc" } },
      { label: "Date d'édition décroissante", expression: { "dateEdition.date": "desc" } },
    ],
  },
  {
    queryType: "author",
    index: "personne,organisation",
    fieldInProduction: "tous_les_auteurs",
    label: "Dans les auteurs",
    placeholder: "*, Solus, Rousseau...",
    cleanFn: esObjectToHeurist,
    sorts: [
      { label: "Pertinence", expression: { _score: "desc", "type.raw": "desc" }, default: true },
      { label: "Nom A->Z", expression: { "nom_complet.raw": "asc" } },
      { label: "Nom Z->A", expression: { "nom_complet.raw": "desc" } },
      { label: "Nombre de publication décroissant", expression: { nb_authored_publications: "desc" } },
      { label: "Nombre de publication croissant", expression: { nb_authored_publications: "asc" } },
      { label: "Date de naissance croissante", expression: { "dateDeNaissance.date": "asc" } },
      { label: "Date de naissance décroissante", expression: { "dateDeNaissance.date": "desc" } },
      { label: "Date de décès croissante", expression: { "dateDeDeces.date": "asc" } },
      { label: "Date de décès décroissante", expression: { "dateDeDeces.date": "desc" } },
    ],
    hardFilter: [
      {
        term: {
          type: "personne",
        },
      },
      {
        range: {
          nb_authored_publications: {
            gt: 0,
          },
        },
      },
    ],
  },
  {
    queryType: "editor",
    index: "editeur",
    fieldInProduction: "editeurs",
    label: "Dans les éditeurs",
    placeholder: "*, Dalloz, Sirey...",
    cleanFn: esObjectToHeurist,
    sorts: [
      { label: "Pertinence", expression: { _score: "desc" }, default: true },
      { label: "Nom A->Z", expression: { "nom_complet.raw": "asc" } },
      { label: "Nom Z->A", expression: { "nom_complet.raw": "desc" } },
      { label: "Nombre de publication décroissante", expression: { nb_edited_publications: "desc" } },
      { label: "Nombre de publication croissante", expression: { nb_edited_publications: "asc" } },
      { label: "Date croissante", expression: { "dateDeDebut.date": "asc" } },
      { label: "Date décroissante", expression: { "dateDeDebut.date": "desc" } },
    ],
  },
  {
    queryType: "collection",
    index: "collection",
    fieldInProduction: "collection",
    label: "Dans les collections",
    placeholder: "*, ...",
    cleanFn: esObjectToHeurist,
    sorts: [
      { label: "Pertinence", expression: { _score: "desc" }, default: true },
      { label: "Titre A->Z", expression: { "nomDeLaCollection.raw": "asc" } },
      { label: "Titre Z->A", expression: { "nomDeLaCollection.raw": "desc" } },
      { label: "Nombre de publications croissant", expression: { nb_publications: "asc" } },
      { label: "Nombre de publications décroissant", expression: { nb_publications: "desc" } },
    ],
  },
  {
    queryType: "editorGenealogy",
    index: "genealogy_org",
    //fieldInProduction: "editeurs",
    label: "Dans les généalogie d'éditeurs",
    placeholder: "*, Marescq...",
    cleanFn: (g) => ({ ...g, nodes: g.nodes.map((o: Organisation) => esObjectToHeurist(o)) }),
    sorts: [
      { label: "Pertinence", expression: { _score: "desc" }, default: true },
      {
        label: "Nombre de publication décroissante",
        expression: { "nodes.nb_edited_publications": { order: "desc", mode: "sum" } },
      },
      {
        label: "Nombre de publication croissante",
        expression: { "nodes.nb_edited_publications": { order: "asc", mode: "sum" } },
      },
    ],
  },
  {
    queryType: "bookGenealogy",
    index: "genealogy_traite",
    //fieldInProduction: "editeurs",
    label: "Dans les généalogie des traités",
    placeholder: "*, ...",
    cleanFn: (g) => ({ ...g, nodes: g.nodes.map((o: ProductionPedagogiqueEnDroit) => esObjectToHeurist(o)) }),
    sorts: [
      { label: "Pertinence", expression: { _score: "desc" }, default: true },
      { label: "Titre A->Z", expression: { "nodes.titre.raw": "asc" } },
      { label: "Titre Z->A", expression: { "nodes.titre.raw": "desc" } },
      { label: "Nombre d'éditions croissant", expression: { nb_editions: "asc" } },
      { label: "Nombre d'éditions décroissant", expression: { nb_editions: "desc" } },
    ],
  },
];

export const SEARCH_TYPES_DICT = keyBy(SEARCH_TYPES, "queryType");
