import { FiltersState, FilterState, SearchType } from "../../types";
import { compact, toPairs } from "lodash";

export const SEARCH_QUERY_KEY = "q";
export const SEARCH_TYPE_KEY = "t";
export const SEPARATOR = "|";
export const SIZE = 50;

export function getDatagridURL(type: SearchType, query: string, filters?: FiltersState): string {
  if (!type || !query) return "/";

  const filterPairs: string[][] = toPairs(filters || {}).flatMap(([k, v]: [string, FilterState]) => {
    if (v.type === "terms") {
      return [[k, v.value.join(SEPARATOR)]];
    }
    if (v.type === "dates") {
      const min = v.value.min;
      const max = v.value.max;
      return compact([min ? [`${k}.min`, min + ""] : null, max ? [`${k}.max`, max + ""] : null]);
    }

    return [];
  });

  return (
    "/?" +
    [[SEARCH_TYPE_KEY, type], [SEARCH_QUERY_KEY, query], ...filterPairs]
      .map(([k, v]) => `${encodeURIComponent(k)}=${encodeURIComponent(Array.isArray(v) ? v.join(SEPARATOR) : v)}`)
      .join("&")
  );
}
