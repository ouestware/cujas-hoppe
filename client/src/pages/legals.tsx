import { FC } from "react";

import { CMSEntry } from "../components/cms-entry";
import config from "../config";

export const LegalsPage: FC = () => (
  <div className="legals-page">
    <h1>
      <i className="fas fa-balance-scale mr-1" />
      <span className="highlight">Mentions légales</span>
    </h1>

    <br />

    <CMSEntry id={config.cms.mentionsLegales} />
  </div>
);
