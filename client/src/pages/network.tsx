import { Collection, Organisation, Personne, ProductionPedagogiqueEnDroit } from "Heurist";
import { UndirectedGraph } from "graphology";
import { keyBy, orderBy, range, some, union, values } from "lodash";
import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";

import { AuthorComponent } from "../components/author";
import { BooksTimeline, TimelineDataPoint } from "../components/books-timeline";
import { AuthorOrgComponent, EditorComponent } from "../components/editor";
import { Loader } from "../components/loader";
import { CaptionElement, SigmaComponent } from "../components/sigma";
import config from "../config";
import { getNetworkPageData } from "../elasticsearchClient";
import { useStateUrl } from "../hooks/state-url";
import { PlainObject } from "../types";

/* NETWORK CHOICES DEFINITIONS */
export interface NetworkField {
  value: keyof ProductionPedagogiqueEnDroit;
  label: string;
  getLabel?: Function;
}

interface NetworkChoice {
  value: string;
  label: string;
  fields: NetworkField[];
}

const authorField: NetworkField = {
  value: "tous_les_auteurs",
  label: "auteurs",
  getLabel: (a: Personne | Organisation) =>
    "nom_complet" in a ? (a as Personne).nom_complet : (a as Organisation).nomEntiteMorale,
};
const editorField: NetworkField = {
  value: "editeurs",
  label: "éditeurs",
  getLabel: (o: Organisation) => o.nomEntiteMorale,
};

const nodeColors = [config.colors.primaryColor, config.colors.secondaryNode];
export const NETWORK_TYPES: NetworkChoice[] = [
  {
    value: "auteurs-editeurs",
    label: "Auteurs et éditeurs",
    fields: [authorField, editorField],
  },
  {
    value: "auteurs-classifications",
    label: "Auteurs et classification",
    fields: [authorField, { value: "classification", label: "classifications" }],
  },
  {
    value: "editeurs-classifications",
    label: "Éditeurs et classification",
    fields: [editorField, { value: "classification", label: "classifications" }],
  },
  {
    value: "auteurs-collections",
    label: "Auteurs et collections",
    fields: [
      authorField,
      { value: "collection", label: "collection", getLabel: (c: Collection) => c.nomDeLaCollection },
    ],
  },
  {
    value: "auteurs-niveauDuCours",
    label: "Auteurs et niveau du cours",
    fields: [authorField, { value: "niveauDuCours", label: "niveaux du cours" }],
  },
];
const NETWORK_TYPES_DICT: PlainObject<NetworkChoice> = keyBy(NETWORK_TYPES, "value");
export const DEFAULT_NETWORK_TYPE = NETWORK_TYPES[0].value;

function filterBooksInSelectedRange(
  books: ProductionPedagogiqueEnDroit[],
  filterYearFrom: number | null,
  filterYearTo: number | null,
): ProductionPedagogiqueEnDroit[] {
  return filterYearFrom && filterYearTo
    ? books.filter(
        (book: ProductionPedagogiqueEnDroit) =>
          book.dateEdition &&
          some(
            range(book.dateEdition.range.gte.getFullYear(), book.dateEdition.range.lte.getFullYear() + 1),
            (y: number) => y >= filterYearFrom && y <= filterYearTo,
          ),
      )
    : books;
}
/**
 * Filter a graph based on a period
 */
function filterGraph(
  graph: UndirectedGraph,
  filterYearFrom: number | null,
  filterYearTo: number | null,
): { graph: UndirectedGraph; nbSelectedBooks: number } {
  let selectedBooksIDs: string[] = [];
  graph.forEachNode((node, data) => {
    const visibleBooks = filterBooksInSelectedRange(values(data.books), filterYearFrom, filterYearTo);
    graph.setNodeAttribute(node, "nbBooks", visibleBooks.length);
    selectedBooksIDs = union(
      selectedBooksIDs,
      visibleBooks.map((b) => b.id),
    );
  });
  graph.forEachEdge((edge, data) => {
    const visibleBooks = filterBooksInSelectedRange(values(data.books), filterYearFrom, filterYearTo);
    graph.setEdgeAttribute(edge, "nbBooks", visibleBooks.length);
  });

  return { graph, nbSelectedBooks: selectedBooksIDs.length };
}

export const Network: React.FC = () => {
  const navigate = useNavigate();
  const { networkType } = useParams<"networkType">();

  // states
  const [fullNetworkType, setFullNetworkType] = useState<NetworkChoice>(NETWORK_TYPES_DICT[networkType || 0]);
  const [missingBooksHistogramData, setMissingBooksHistogramData] = useState<TimelineDataPoint[]>([]);
  // Data management:
  const [networkData, setNetworkData] = useState<UndirectedGraph | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [booksHistogramData, setBooksHistogramData] = useState<TimelineDataPoint[]>([]);
  const [nbSelectedBooks, setNbSelectedBooks] = useState<number | null>(null);

  // url params
  const [filterYearFrom, setFilterYearFrom] = useStateUrl<number | null>("from", null);
  const [filterYearTo, setFilterYearTo] = useStateUrl<number | null>("to", null);
  const [selectedNode, setSelectedNode] = useStateUrl<string | null>("selected", null);
  const [neighborsSort, setNeighborsSort] = useStateUrl<string>("sort", "nbBooks|desc");

  // When component mounts
  //  => init & load data
  useEffect(() => {
    // Setting the full network type
    if (networkType !== undefined) {
      const networkConfig = NETWORK_TYPES_DICT[networkType];
      setFullNetworkType(networkConfig);

      // Init data loading
      setLoading(true);
      setError(null);

      // Fetch data
      getNetworkPageData(networkConfig.fields)
        .then((data) => {
          const { network, booksHistogramData, missingBooksHistogramData } = data;
          setNetworkData(network);
          setBooksHistogramData(booksHistogramData);
          setMissingBooksHistogramData(missingBooksHistogramData);
        })
        .catch(() => {
          setError(`Erreur lors de la récupération des données du réseau ${networkType}.`);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [networkType]);

  // When year filters or data changed
  //  => update/filter the graph
  useEffect(() => {
    if (networkData !== null) {
      // we update the graph to filter nodes
      const { graph, nbSelectedBooks } = filterGraph(networkData, filterYearFrom, filterYearTo);

      setNbSelectedBooks(nbSelectedBooks);
      // TODO: getting missing books number n sleected range requires a ES query => #flemme
      //setMissingBooksRangeHistogramData(missingBooksRangeHistogramData);

      // Check if the selected node is in the graph AND in the period.
      // if so we keep it, otherwise we remove it from selection state
      if (
        selectedNode &&
        (!graph.hasNode(selectedNode) ||
          (graph.hasNode(selectedNode) && graph.getNodeAttribute(selectedNode, "nbBooks") === 0))
      ) {
        console.log("removing selectednode");
        setSelectedNode(null);
      }
    }
  }, [filterYearFrom, filterYearTo, networkData, selectedNode, setSelectedNode, setNbSelectedBooks]);

  const totalPublication = booksHistogramData.reduce((sum, d) => sum + d.value, 0);
  const totalMissingPublication = missingBooksHistogramData.reduce((sum, d) => sum + d.value, 0);

  const selectedNodeAttributes =
    selectedNode && networkData && networkData.hasNode(selectedNode)
      ? networkData.getNodeAttributes(selectedNode)
      : null;
  const captionElements: CaptionElement[] = fullNetworkType.fields.map((f, i) => ({
    type: f.label,
    color: nodeColors[i],
    count: networkData
      ? networkData.nodes().filter((id) => networkData.getNodeAttribute(id, "type") === f.value).length
      : 0,
  }));

  // neighborood of selectedNode sorted by edge weight == nbBooks
  const neighbors =
    selectedNode && networkData && networkData.hasNode(selectedNode)
      ? networkData
          .neighbors(selectedNode)
          .filter((item) => {
            return (
              networkData.getNodeAttribute(item, "nbBooks") > 0 &&
              networkData.getEdgeAttribute(selectedNode, item, "nbBooks") > 0
            );
          })
          .map((n) => ({
            ...networkData.getNodeAttributes(n),
            nbBooks: networkData.getEdgeAttribute(selectedNode, n, "nbBooks"),
          }))
      : [];

  return (
    <>
      <div className="flex flex-column">
        <h3 className="mr-1 network-title">
          Je veux voir <span className="highlight">le réseau</span> des relations entre <br />
          <span className="custom-select">
            <select name="t" value={networkType} onChange={(e) => navigate(`/reseau/${e.target.value}`)}>
              {NETWORK_TYPES.map(({ value, label }) => (
                <option value={value} key={value}>
                  {label}
                </option>
              ))}
            </select>
          </span>{" "}
          de{" "}
          <input
            name="from"
            type="number"
            step="1"
            min={config.minYear}
            max={config.maxYear}
            value={filterYearFrom ? filterYearFrom : config.minYear}
            size={4}
            onChange={(e) => {
              setFilterYearFrom(Number.parseInt(e?.target.value) || null);
              if (!filterYearTo) setFilterYearTo(config.maxYear);
            }}
          />{" "}
          à{" "}
          <input
            name="to"
            type="number"
            step="1"
            min={config.minYear}
            max={config.maxYear}
            value={filterYearTo ? filterYearTo : config.maxYear}
            size={4}
            onChange={(e) => {
              if (!filterYearFrom) setFilterYearFrom(config.minYear);
              setFilterYearTo(Number.parseInt(e?.target.value) || null);
            }}
          />
          .
        </h3>
      </div>

      {loading && !error && <Loader message="Chargement des données du réseau..." />}
      {!!error && error}
      {booksHistogramData.length > 0 && (
        <>
          <h4>
            <i className="fas fa-chart-column mr-1" />
            <span className="highlight">Nombre de publications par année</span>
          </h4>
          <BooksTimeline
            booksHistogram={booksHistogramData}
            missingBooksHistogram={missingBooksHistogramData}
            showMissing={true}
            marks={[]}
            highlightedPeriod={filterYearFrom && filterYearTo ? [filterYearFrom, filterYearTo] : undefined}
            onSelectionEnd={(from, to) => {
              if (from && to) {
                setFilterYearFrom(Number.parseInt(from.year));
                setFilterYearTo(Number.parseInt(to.year));
              } else {
                setFilterYearFrom(null);
                setFilterYearTo(null);
              }
            }}
          />
          <div className="legend">
            <div className="barchart-legend" />
            publications prises en compte : {filterYearFrom ? `${nbSelectedBooks}/` : ""}
            {totalPublication}
            <br />
            <div className="barchart-missing-legend" />
            pourcentage des publications non incluses (mention des{" "}
            {fullNetworkType.fields.map((f) => f.label).join(" et/ou ")} manquante dans le document) :{" "}
            {totalMissingPublication} soit{" "}
            {((100 * totalMissingPublication) / (totalMissingPublication + totalPublication)).toFixed(1)}%
          </div>
        </>
      )}
      <h4 style={{ marginTop: "1em" }}>
        <i className="fas fa-project-diagram mr-1" />
        <span className="highlight">
          Relations entre {fullNetworkType.label}{" "}
          {`${filterYearFrom || config.minYear}-${filterYearTo || config.maxYear}`}
        </span>
      </h4>
      <div id="graph">
        {networkData && (
          <SigmaComponent
            graph={networkData}
            selectedNode={selectedNode && networkData.hasNode(selectedNode) ? `${selectedNode}` : null}
            setSelectedNode={(value: string) => {
              setSelectedNode(value);
              const element = document.getElementById("graph");
              if (element) element.scrollIntoView({ behavior: "smooth" });
            }}
            captionElements={captionElements}
          />
        )}
      </div>

      {selectedNodeAttributes && (
        <div>
          <br />
          {selectedNodeAttributes && selectedNodeAttributes.object.type === "personne" && (
            <AuthorComponent author={selectedNodeAttributes.object} firstLevel={true} />
          )}
          {selectedNodeAttributes &&
            selectedNodeAttributes.object.type === "organisation" &&
            (selectedNodeAttributes.type === "tous_les_auteurs" ? (
              <AuthorOrgComponent editor={selectedNodeAttributes.object} firstLevel={true} />
            ) : (
              <EditorComponent editor={selectedNodeAttributes.object} firstLevel={true} />
            ))}
          {selectedNodeAttributes && selectedNodeAttributes.object.type === "collection" && (
            <h4>
              <i className="fas fa-lines-leaning mr-1" />{" "}
              <Link to={`/collection/${selectedNodeAttributes.object.id}`}>{selectedNodeAttributes.label}</Link>
            </h4>
          )}
          {selectedNodeAttributes &&
            !["personne", "organisation", "collection"].includes(selectedNodeAttributes.object.type) && (
              <h4>{selectedNodeAttributes.label}</h4>
            )}
        </div>
      )}

      {neighbors.length > 0 && networkData && (
        <>
          <div>
            Lié à {neighbors.length}{" "}
            {fullNetworkType.fields.find((f) => f.value !== selectedNodeAttributes?.type)?.label || ""} (par{" "}
            {filterBooksInSelectedRange(values(selectedNodeAttributes?.books), filterYearFrom, filterYearTo).length}{" "}
            ouvrages)
            <span>, trié par </span>
            <span className="custom-select">
              <select name="sort" value={neighborsSort} onChange={(e) => setNeighborsSort(e.target.value)}>
                <option value="nbBooks|desc">Nombre de publication décroissant</option>
                <option value="nbBooks|asc">Nombre de publication croissant</option>
                <option value="label|asc">{"Nom A->Z"}</option>
                <option value="label|desc">{"Nom Z->A"}</option>
              </select>
            </span>
          </div>
          <NeighborsCards
            neighbors={neighbors}
            selectedNode={selectedNodeAttributes}
            sort={neighborsSort}
            filterYearFrom={filterYearFrom}
            filterYearTo={filterYearTo}
          />
        </>
      )}
    </>
  );
};

interface NeighborsCardsProps {
  neighbors: any[];
  selectedNode: any;
  sort: string;
  filterYearFrom: number | null;
  filterYearTo: number | null;
}
export const NeighborsCards: React.FC<NeighborsCardsProps> = (props: NeighborsCardsProps) => {
  const { neighbors, selectedNode, sort, filterYearFrom, filterYearTo } = props;
  const neighborFilterName: { [key: string]: string } = {
    tous_les_auteurs: "tous_les_auteurs.nom_complet",
    auteurs: "tous_les_auteurs.nom_complet", // should not be used anymore
    editeurs: "editeurs.nomEntiteMorale",
    classification: "classification",
    collection: "collection",
    niveauDuCours: "niveauDuCours",
  };
  const iconeClassName: { [key: string]: string } = {
    personne: "fas fa-user mr-1",
    organisation: "fas fa-building mr-1",
  };

  // sort the list
  const parts = sort.split("|");
  const field = parts.length === 2 ? parts[0] : sort;
  const order = (parts.length === 2 ? parts[1] : "asc") as "desc" | "asc";
  const list = orderBy(neighbors, [field], [order]);

  return (
    <div className="neighbors-list">
      {list.map((o: any) => {
        if (neighborFilterName[o.type]) {
          let URLToDataGrid = `/?t=book&q=*&${encodeURIComponent(neighborFilterName[o.type])}=${encodeURIComponent(
            o.label,
          )}&${encodeURIComponent(neighborFilterName[selectedNode.type])}=${encodeURIComponent(selectedNode.label)}`;
          if (filterYearFrom) URLToDataGrid += `&dateEdition.range.min=${filterYearFrom}`;
          if (filterYearTo) URLToDataGrid += `&dateEdition.range.max=${filterYearTo}`;

          return (
            <span key={o.key} className="flex">
              {iconeClassName[o?.object?.type || ""] ? <i className={iconeClassName[o.object.type]} /> : ""}
              <Link to={URLToDataGrid} className="link-with-counts" title={`${o.label} (${o.nbBooks})`}>
                <span className="ellipsis">{o.label}</span>
                <span>({o.nbBooks})</span>
              </Link>
              &nbsp;
              <Link
                to={URLToDataGrid}
                className="new-tab-link"
                target="blank_"
                rel="noopener"
                title={`Ouvrir dans un nouvel onglet : ${o.label} (${o.nbBooks})`}
              >
                <i className="fas fa-external-link-alt" />
              </Link>
            </span>
          );
        } else return <span>{o.type} </span>;
      })}
    </div>
  );
};
