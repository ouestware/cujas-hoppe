import { Organisation } from "Heurist";
import { OptionType } from "./components/custom-select";

/**
 * Util type to represent maps of typed elements, but implemented with
 * JavaScript objects.
 */
export type PlainObject<T = any> = { [k: string]: T };

/**
 * HOMEPAGE-CENTERED TYPES:
 * ************************
 */
export type SearchType = "author" | "editor" | "book" | "editorGenealogy" | "bookGenealogy" | "collection";

export type SortType = {
  label: string; // used to display in select box
  expression: any;
  default?: boolean; //default false
};

export type SearchTypeDefinition = {
  queryType: SearchType;
  index: string;
  fieldInProduction?: string; // used to retrieve booksHistogram for author and editor
  label: string;
  placeholder: string;
  cleanFn: (rawData: any) => any;
  sortField?: string;
  reversedSort?: boolean;
  sorts: Array<SortType>;
  hardFilter?: Array<Record<string, any>>;
};

export type TermsFilterState = {
  type: "terms";
  value: string[];
};

export type DatesFilterState = {
  type: "dates";
  value: {
    min?: number;
    max?: number;
  };
};

export type FilterState = TermsFilterState | DatesFilterState;

export type FiltersState = { [key: string]: FilterState };

export type ESSearchQueryContext = {
  index: string;
  query: string;
  filters: FiltersState;
  sort: SortType | null;
  hardFilter?: Array<Record<string, any>>;
};

export type TermsFilterType = {
  id: string;
  type: "terms";
  label: string;
  isMulti?: boolean;
  cacheOptions?: boolean;
  options?: OptionType[];
  asyncOptions?: (inputValue: string, context: ESSearchQueryContext) => Promise<OptionType[]>;
};

export type DatesFilterType = {
  id: string;
  type: "dates";
  label: string;
};

export type FilterType = TermsFilterType | DatesFilterType;

export type FilterHistogramType = { values: { label: string; count: number }[]; total: number; maxCount?: number };

export interface Link {
  type: string;
  from: string;
  to: string;
}
// Type for a family
export interface Organisation_genealogy {
  nodes: Array<Organisation>;
  edges: Array<Link>;
}
