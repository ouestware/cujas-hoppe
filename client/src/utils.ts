/**
 * Guess the type of a string and return it casted.
 * examples:
 *   - "true" => true
 *   - "10.1" => 10.1
 */
export function stringGuessCast(value: string): any {
  if (value !== null) {
    if (isNaN(value as unknown as number) === false) {
      return parseFloat(value) % 1 === 0 ? parseInt(value) : (parseFloat(value) as unknown);
    }
    if (value === "false" || value === "true") {
      return (value === "true") as unknown;
    }
  }
  return value;
}

/**
 * Return the position on the page of an html element
 */
export function getElementPosition(element: HTMLElement): [number, number] {
  let x = 0;
  let y = 0;
  let ele = element;
  while (true) {
    x += ele.offsetLeft;
    y += ele.offsetTop;
    if (ele.offsetParent === null) {
      break;
    }
    ele = ele.offsetParent as HTMLElement;
  }
  return [x, y];
}

export function colorHexToRgb(hex: string): { r: number; g: number; b: number } {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  if (!result) throw new Error(`Invalid hex color : ${hex}`);
  return {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
  };
}
