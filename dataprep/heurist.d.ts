declare module "Heurist" {

  export enum DateType {
    YEAR_RANGE = "year-range",
    DATE_RANGE = "date-range",
    DATE_APPROXIMATE = "date-approximate",
    DATE = "date",
  }

  export interface Genealogy<T> {
    nodes: Array<T>;
    edges: Array<{from:string, to:string, type:string}>
  }

  export interface DateHeurist {
    range: {
      gte: Date,
      lte: Date
    },
    date: Date,
    type: DateType
  }


  export interface WebSitePage { 
    id: string;
    type: string;
    title: string;
    text?: string;
  }

  export interface Organisation { 
    id: string;
    type: string;
    nomEntiteMorale: string;
    acronyme?: string;
    note?: string;
    dateDeDebut?: DateHeurist;
    dateDeFin?: DateHeurist;
    typeOrganisme?: string;
    pays?: Array<string>;
    ville?: Array<Ville>;
    vignetteOuLogo?: string;
    personneLiee?: Array<{ relation: string, direction: string, other: Personne}>;
    varianteDuNom?: Array<string>;
    structuresLiees?: Array<{ relation: string, direction: string, other: Organisation}>;
    lienVersAutorite?: Array<string>;
    adresse?: Array<string>;
    natureDeLaDisparition?: string;
    nb_edited_publications: string;
    nb_authored_publications: string;
    nom_complet: string;
  }

  export interface Personne { 
    id: string;
    type: string;
    nom: string;
    dateDeNaissance?: DateHeurist;
    dateDeDeces?: DateHeurist;
    prenom?: string;
    genre?: string;
    paysDeNaissance?: string;
    lieuDeNaissance?: Ville;
    portrait?: string;
    personneLiee?: Array<{ relation: string, direction: string, other: Personne}>;
    lieuDeDeces?: Ville;
    paysDeDeces?: string;
    nomDeJeuneFille?: string;
    nomAuteur?: Array<string>;
    lienVersAutorite?: Array<string>;
    ressourceExterieure?: Array<string>;
    nom_complet: string;
    nb_authored_publications: string;
  }

  export interface Ville { 
    id: string;
    type: string;
    ville: string;
    pays?: string;
  }

  export interface ElementDeCarriere { 
    id: string;
    type: string;
    descriptionEvenement?: string;
    dateEvenement?: DateHeurist;
    periodeDateDeDebut?: DateHeurist;
    periodeDateDeFin?: DateHeurist;
    personne?: Personne;
    lieu?: Ville;
    autresPersonnesLiees?: Array<{ relation: string, direction: string, other: Personne}>;
    structuresLiees?: Array<{ relation: string, direction: string, other: Organisation}>;
    typeEvenementCarriere: string;
    matiereEnseignee?: string;
    intituleDuCours?: string;
    fonctionEditoriale?: string;
    fonctionAdministrative?: string;
    activiteProfessionnelle?: string;
    activiteAssociative?: string;
    niveauEnseigne?: string;
    fonctionOuGrade?: string;
    institutionDeRattachement?: Array<Organisation>;
  }

  export interface ProductionPedagogiqueEnDroit { 
    id: string;
    type: string;
    titre: string;
    titreDePartie?: string;
    dateEdition?: DateHeurist;
    auteurs?: Array<Personne | Organisation>;
    titreDEnsemble?: string;
    manquant?: boolean;
    relationsEntreEditions?: Array<{ relation: string, direction: string, other: ProductionPedagogiqueEnDroit}>;
    collection?: Array<Collection>;
    editeurs?: Array<Organisation>;
    editedVolume?: string;
    collation?: string;
    classification?: string;
    isbn?: string;
    cote?: Array<string>;
    liens?: Array<string>;
    complementDeTitre?: Array<string>;
    mentionEdition?: string;
    numeroDansLaCollection?: Array<string>;
    format?: string;
    illustrations?: Array<string>;
    issn?: string;
    ppn?: Array<string>;
    idNBibliographique?: Array<string>;
    dateCours?: DateHeurist;
    lieuDuCours?: Organisation;
    discipline?: string;
    niveauDuCours?: Array<string>;
    fonctionAuteur?: Array<{ relation: string, direction: string, other: Personne | Organisation}>;
    numeroDePartie?: string;
    nombreTotalDePages?: string;
    nomEditeurPageDeTitre?: Array<string>;
    villeEdition?: Array<Ville>;
    tous_les_auteurs: Array<Personne | Organisation>;
  }

  export interface Collection { 
    id: string;
    type: string;
    nomDeLaCollection: string;
    dateDeCreation?: DateHeurist;
    dateDeDisparition?: DateHeurist;
    issn?: string;
    ppn?: string;
    sousCollections?: Array<string>;
    directeurDeCollection?: Array<Personne>;
    editeur: Array<Organisation>;
    relationsEntreLesCollections?: Array<{ relation: string, direction: string, other: Collection}>;
    nb_publications: string;
  }

}
