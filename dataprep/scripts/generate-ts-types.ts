import * as fs from "fs";
import { promisify } from "util";
import config, { SchemaCommonFieldDefinition } from "../src/config";

export const writeFile = promisify(fs.writeFile);

function capitalizeFirstLetter(text: string): string {
  return text.charAt(0).toUpperCase() + text.slice(1);
}

const TAB = "  ";

function heursitFieldToTypescriptType(field: SchemaCommonFieldDefinition): string {
  let tsType = ``;
  switch (field.type) {
    case "date":
      tsType = "Date";
      break;
    case "geo":
      tsType = "[number, number]";
      break;
    case "boolean":
      tsType = "boolean";
      break;
    case "integer":
    case "year":
    case "float":
      tsType = "number";
      break;
    case "dateRange":
      tsType = "DateHeurist";
      break;
    case "resource":
      tsType = (field.resourceTypes || [])
        .map((id) => {
          const modelDef = config.schema.find((model) => model.id === id);
          return `${modelDef ? capitalizeFirstLetter(modelDef.name) : "any"}`;
        })
        .join(" | ");
      break;
    case "relmarker":
      tsType = `{ relation: string, direction: string, other: ${(field.resourceTypes || [])
        .map((id) => {
          const modelDef = config.schema.find((model) => model.id === id);
          return `${modelDef ? capitalizeFirstLetter(modelDef.name) : "any"}`;
        })
        .join(" | ")}}`;
      break;
    default:
      tsType = "string";
      break;
  }
  if (field.isArray) {
    tsType = `Array<${tsType}>`;
  }

  return `\n${TAB}${TAB}${field.name}${field.required ? "" : "?"}: ${tsType};`;
}

// Generate types
let tsTypes = `declare module "Heurist" {

  export enum DateType {
    YEAR_RANGE = "year-range",
    DATE_RANGE = "date-range",
    DATE_APPROXIMATE = "date-approximate",
    DATE = "date",
  }

  export interface Genealogy<T> {
    nodes: Array<T>;
    edges: Array<{from:string, to:string, type:string}>
  }

  export interface DateHeurist {
    range: {
      gte: Date,
      lte: Date
    },
    date: Date,
    type: DateType
  }

`;

config.schema.forEach((model) => {
  let modelTs = `\n${TAB}export interface ${capitalizeFirstLetter(
    model.name,
  )} { \n${TAB}${TAB}id: string;\n${TAB}${TAB}type: string;`;
  model.fields.forEach((field) => {
    modelTs += heursitFieldToTypescriptType(field);
  });
  (model.computed_fields ? model.computed_fields : []).forEach((field) => {
    modelTs += heursitFieldToTypescriptType(field);
  });
  modelTs += `\n${TAB}}\n`;
  tsTypes += modelTs;
});
tsTypes += "\n}\n";

// Writing types into a file
fs.writeFile("./heurist.d.ts", tsTypes, "utf8", (err) => {
  if (err) throw err;
  console.log("The file has been saved!");
});
