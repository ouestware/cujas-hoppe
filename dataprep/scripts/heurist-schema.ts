import { Heurist } from "../src/services/heurist";
import config from "../src/config";

async function exec() {
  const heurist = new Heurist(config.heurist);
  await heurist.login();
  const result = await heurist.generateSchema();
  console.log(JSON.stringify(result, null, 2));
  process.exit();
}

exec();
