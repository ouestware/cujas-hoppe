import { Family } from "../services/genealogy";
import { schema } from "./heurist-schema";
import { Config } from "./index";

const config_default: Config = {
  error_with_stack: true,
  cron_schedule: "0 11 4 * * *",
  logs: {
    console_level: "info",
    file_level: "error",
    file_maxsize: "50m",
    file_retention: "1",
    file_path: "./",
  },
  elasticsearch: {
    nodes: ["http://localhost:9200"],
    sniffOnStart: true,
    sniffInterval: 60000,
  },
  heurist: {
    url: "https://heurist.huma-num.fr/heurist/",
    login: process.env.HOPPE_HEURIST_LOGIN || "",
    password: process.env.HOPPE_HEURIST_PASSWORD || "",
    database: "algo_hoppe",
  },
  schema: schema,
  genealogies: [
    {
      index_name: "genealogy_org",
      model_id: 4,
      field_name: "structuresLiees",
      // keep only genealogies which contains an éditeur
      postprocess: (families: Array<Omit<Family, "ids">>) =>
        families
          .filter((f: Omit<Family, "ids">) => f.nodes.some((n) => n.typeOrganisme === "Editeur (personne morale)"))
          .map((f: Omit<Family, "ids">) => ({ ...f, nb_editeurs: f.nodes.length })),
    },
    {
      index_name: "genealogy_traite",
      model_id: 53,
      field_name: "relationsEntreEditions",
      // add nb_editions to be able to sort on this info in search
      postprocess: (families: Array<Omit<Family, "ids">>) =>
        families.map((f: Omit<Family, "ids">) => ({ ...f, nb_editions: f.nodes.length })),
    },
  ],
};

export default config_default;
