import config_default from "./default";
import * as config_test from "./test.json";
import * as config_docker from "./docker.json";
import * as config_production from "./production.json";
import { merge } from "lodash";
import { Family } from "../services/genealogy";

export interface ConfigElasticSearch {
  // List of ES hosts
  nodes: Array<string>;
  // Sniffing : discover cluster member at startup ?
  sniffOnStart: boolean;
  // Sniffing interval
  sniffInterval: number;
}

export type SchemaCommonFieldDefinition = {
  name: string;
  type: string;
  required: boolean;
  isArray?: boolean;
  // just need in SchemaFieldDefinition, but with that I avoid a type guard ...
  resourceTypes?: Array<string>;
};

export type SchemaHeuristFieldDefinition = SchemaCommonFieldDefinition & {
  id: string;
  depth?: number;
  relationTypes?: Array<{ id: string; name: string }>;
  // Allow you to make some transformation based on the field value
  // it's usefull for example to make some mapping with a dictionnary
  postprocess?: (current: any) => any;
};

export type SchemaComputedFieldDefinition = SchemaCommonFieldDefinition & {
  // Allow you to create you field with what ever you want, you have acces to
  // full db + the current objet
  depth?: number;
  relationTypes?: Array<{ id: string; name: string }>;
  computeField?: (db: { [modelId: string]: Array<any> }, obj: any) => any;
};

export type SchemaModelDefinition = {
  id: string;
  name: string;
  index_name?: string;
  index_configuration?: any;
  fields: Array<SchemaHeuristFieldDefinition>;
  computed_fields?: Array<SchemaComputedFieldDefinition>;
};

export type ConfigDataSchema = Array<SchemaModelDefinition>;

export interface ConfigHeurist {
  url: string;
  login: string;
  password: string;
  database: string;
}

export interface ConfigGenealogy {
  index_name: string;
  model_id: number;
  field_name: string;
  // Allow you to make some transformation on families
  postprocess?: (current: Array<Omit<Family, "ids">>) => Array<Omit<Family, "ids">>;
}

export interface ConfigLog {
  console_level: string;
  file_level: string;
  file_maxsize: string;
  file_retention: string;
  file_path: string;
}

export interface Config {
  error_with_stack: boolean;
  cron_schedule: string;
  logs: ConfigLog;
  elasticsearch: ConfigElasticSearch;
  heurist: ConfigHeurist;
  schema: ConfigDataSchema;
  genealogies: Array<ConfigGenealogy>;
}

let config: Config;

switch (process.env.configuration) {
  case "test":
    config = merge(config_default, config_test as Partial<Config>);
    break;
  case "docker":
    config = merge(config_default, config_docker as Partial<Config>);
    break;
  case "production":
    config = merge(config_default, config_production as Partial<Config>);
    break;
  default:
    config = { ...config_default };
    break;
}

export default config;
