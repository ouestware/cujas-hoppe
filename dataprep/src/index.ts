import * as cron from "node-cron";
import { ElasticSearch } from "./services/elasticsearch";
import { Heurist } from "./services/heurist";
import { Logger, getLogger } from "./services/logger";
import { createGenealogyIndices } from "./services/genealogy";
import config from "./config";
import { heuristComputeFields, heuristPostProcessing } from "./utils";

// logger
const log: Logger = getLogger("Main");

async function doTheJob() {
  try {
    // Create the ES service
    log.info(`Creating ES client`);
    const elastic = new ElasticSearch(config.elasticsearch);

    // Create the heurist service
    log.info(`Creating Heurist client`);
    const heurist = new Heurist(config.heurist);
    log.info(`Heurist auth`);
    await heurist.login();

    // Get heurist dictionnary
    const dictionnary = await heurist.getDictionnary();

    // In memory datastorage of heurist's models
    let data: { [model: string]: Array<any> } = {};

    // Retrieve the full database from heurist
    log.info(`Download the full database from heurist`);
    await Promise.all(
      config.schema.map(async (model) => {
        log.info(`Data for model ${model.name} (${model.id})`);
        // retrieve the data from Heurist
        const list = await heurist.getModelList(model.id);
        data[model.id] = list && list.length > 0 ? list : [];
        return null;
      }),
    );

    log.info(`Heurist compute field`);
    data = heuristComputeFields(config.schema, data);

    log.info(`Heurist post processing`);
    const dataToImport: { [key: string]: Array<any> } = heuristPostProcessing(config.schema, dictionnary, data);

    // Make the index
    log.info(`Index to ElasticSearch`);
    // do the import
    await Promise.all(
      config.schema.map(async (model) => {
        const index_name = model.index_name || model.name.toLowerCase();
        await elastic.createIndex(index_name, model);
        if (data[model.id].length > 0) {
          const report = await elastic.bulkImport(index_name, dataToImport[model.id]);
          if (report) {
            log.error(
              `ES Bulk import error for model ${model.name} (${model.id}) : ${JSON.stringify(report, null, 2)}`,
            );
            throw new Error(
              `ES Bulk import error for model ${model.name} (${model.id}) : ${JSON.stringify(report, null, 2)}`,
            );
          } else {
            log.info(`Success for model ${model.name} (${model.id})`);
          }
        } else {
          log.warn(`Model ${model.name} (${model.id}) has no data`);
        }
        return null;
      }),
    );

    // Create index alias
    await elastic.createIndexAlias("editeur", "organisation", {
      filter: { term: { "typeOrganisme.raw": "Editeur (personne morale)" } },
    });

    log.info(`Create genealogies`);
    await createGenealogyIndices(dataToImport);

    log.info(`Import process successful`);
  } catch (e) {
    log.error(`Import process in error : ${e}`);
  }
}

// Run the job at startup
doTheJob();

// Register the job as a cron
cron.schedule(config.cron_schedule, () => {
  doTheJob();
});
