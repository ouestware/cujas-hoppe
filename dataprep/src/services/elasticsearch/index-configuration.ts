export const indexConfiguration: any = {
  settings: {
    analysis: {
      filter: {
        french_stop: {
          type: "stop",
          stopwords: "_french_",
        },
      },
      analyzer: {
        IndexAnalyzer: {
          filter: ["lowercase", "asciifolding", "word_delimiter", "french_stop"],
          type: "custom",
          tokenizer: "whitespace",
        },
        SearchAnalyzer: {
          filter: ["lowercase", "asciifolding", "word_delimiter", "french_stop"],
          type: "custom",
          tokenizer: "whitespace",
        },
      },
    },
  },
  mappings: {
    properties: {
      id: {
        type: "keyword",
        store: true,
      },
      _search: {
        type: "text",
        store: false,
        analyzer: "IndexAnalyzer",
        search_analyzer: "SearchAnalyzer",
        fields: {
          raw: {
            type: "keyword",
            ignore_above: 256,
          },
        },
      },
      _suggest: {
        type: "completion",
        analyzer: "IndexAnalyzer",
        search_analyzer: "SearchAnalyzer",
        preserve_separators: true,
        preserve_position_increments: true,
      },
    },
    dynamic_templates: [
      {
        strings: {
          match_mapping_type: "string",
          mapping: {
            type: "text",
            fields: {
              raw: {
                type: "keyword",
                ignore_above: 256,
              },
            },
            copy_to: ["_suggest", "_search"],
          },
        },
      },
    ],
  },
};
