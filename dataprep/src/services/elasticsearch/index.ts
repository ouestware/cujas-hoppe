import { Client } from "@elastic/elasticsearch";
import { values } from "lodash";
import { ConfigElasticSearch, SchemaModelDefinition } from "../../config";
import { Logger, getLogger } from "../logger";
import { indexConfiguration } from "./index-configuration";
import { BulkErrorReport } from "./types";

/**
 * ElasticSearch service
 */
export class ElasticSearch {
  // logger
  private log: Logger = getLogger("ElasticService");
  // Nodejs es client
  client: Client;

  constructor(config: ConfigElasticSearch) {
    this.client = new Client(config);
  }

  /**
   * Create, or delete+create if it already exists the index.
   *
   * @param {string} index The name of the index to create
   * @param {SchemaModelDefinition} schema The model schema
   * @throws {Error} When an ES call failed
   */
  async createIndex(index: string, schema: SchemaModelDefinition): Promise<void> {
    this.log.info(`Create index ${index}`);
    // Check if the index already exists and delete it if it's needed.
    const exists = await this.client.indices.exists({
      index: index,
    });
    if (exists) {
      this.log.warn(`Index ${index} already exists, so we delete it before its recreation`);
      await this.client.indices.delete({
        index: index,
        allow_no_indices: true,
      });
    }

    // Creating the index
    this.log.debug(`Creating index ${index} for model ${JSON.stringify(schema, null, 2)}`);
    await this.client.indices.create({
      index: index,
      body: this.schemaModelDefinitionToIndexConfiguration(schema),
    });
  }

  /**
   * Delete an ES index.
   *
   * @param {string} index The name of the index to delete
   * @returns {boolean} <code>true</code> if the index was found and deleted, <code>false</code> otherwise.
   * @throws {Error} When an ES call failed
   */
  async deleteIndex(index: string): Promise<boolean> {
    this.log.info(`Delete index ${index}`);
    const exists = await this.client.indices.exists({
      index: index,
    });
    if (!exists) {
      return false;
    }
    await this.client.indices.delete({
      index: index,
      allow_no_indices: true,
    });
    return true;
  }

  /**
   * Create (or recreate) an ES index alias.
   *
   * @param {string} name The name of the alias
   * @param {string} index The name of the index to alias
   * @param {object} body The config for the alias
   * @throws {Error} When an ES call failed
   */
  async createIndexAlias(name: string, index: string, body: any): Promise<void> {
    // Search if the alias exist
    this.log.info(`Create index alias ${name}:${index}`);
    const exists = await this.client.indices.existsAlias({ index, name });

    // if it exist, we delete it
    if (exists) {
      this.log.debug(`Delete index alias ${name}:${index}`);
      await this.client.indices.deleteAlias({ index, name });
    }

    // Create the alias
    this.log.info(`Put index alias ${name}:${index}`);
    await this.client.indices.putAlias({ index, name, body });
  }

  /**
   * Make a bulk import
   * @param {string} index The index where to import the data
   * @param {Array<object>} the data to import
   */
  async bulkImport(index: string, data: Array<any>): Promise<BulkErrorReport | null> {
    this.log.info(`ES Bulk import on index ${index} with ${data.length} documents`);
    const body = data.flatMap((doc) => [{ index: { _index: index, _id: doc.id } }, doc]);
    this.log.info(`Indexing ${data.length} documents in index ${index}`);

    const response = await this.client.bulk({
      refresh: true,
      body,
    });
    this.log.debug(`ES bulk response is ${JSON.stringify(response, null, 2)}`);

    if (response.errors) {
      const erroredDocuments: BulkErrorReport = [];
      // The items array has the same order of the dataset we just indexed.
      // The presence of the `error` key indicates that the operation
      // that we did for the document has failed.
      response.items.forEach((action) => {
        const operations = values(action);

        if (operations[0].error) {
          erroredDocuments.push({
            // If the status is 429 it means that you can retry the document,
            // otherwise it's very likely a mapping error, and you should
            // fix the document before to try it again.
            status: operations[0].status + "",
            error: JSON.stringify(operations[0].error),
          });
        }
      });
      return erroredDocuments;
    }

    return null;
  }

  private schemaModelDefinitionToIndexConfiguration(schema: SchemaModelDefinition): any {
    const config = Object.assign({}, indexConfiguration);
    // Year range
    schema.fields
      .filter((e) => e.type === "dateRange")
      .forEach((field) => {
        config.mappings.properties[field.name] = {
          properties: {
            range: { type: "date_range" },
            date: { type: "date" },
            type: { type: "text" },
          },
        };
      });
    // Geo
    schema.fields
      .filter((e) => e.type === "geo")
      .forEach((field) => {
        config.mappings.properties[field.name] = {
          type: "geo_point",
        };
      });
    return config;
  }
}
