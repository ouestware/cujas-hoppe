// ES Type definitions
interface BulkError {
  status: string;
  error: string;
}

export type BulkErrorReport = Array<BulkError>;
