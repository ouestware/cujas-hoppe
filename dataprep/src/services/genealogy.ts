import { intersection, omit, union, unionBy, sortBy, maxBy, flatten } from "lodash";
import { ElasticSearch } from "./elasticsearch";
import config from "../config/index";
import { Logger, getLogger } from "./logger";
import { DirectedGraph } from "graphology";
import { bidirectional as shortestPath } from "graphology-shortest-path/unweighted";

// logger
const log: Logger = getLogger("Genealogy");

interface Link {
  type: string;
  from: string;
  to: string;
}
// Type for a family
export interface Family {
  ids: Array<string>;
  nodes: Array<any>;
  edges: Array<Link>;
}

export async function createGenealogyIndices(data: { [model: string]: Array<any> }): Promise<void> {
  const es = new ElasticSearch(config.elasticsearch);

  await Promise.all(
    config.genealogies.map(async (genealogy) => {
      // Create the index
      await es.createIndex(genealogy.index_name, {
        id: genealogy.index_name,
        name: genealogy.index_name,
        fields: [],
      });

      // Compute the genealogies
      const result: Array<Family> = commputeGenealogie(data[genealogy.model_id], genealogy.field_name);

      let families = result.map((item) => omit(item, ["ids"]));
      if (genealogy.postprocess) families = genealogy.postprocess(families);

      // Bulk insert
      await es.bulkImport(genealogy.index_name, families);
    }),
  );
}

/**
 * Given an id, return its corresponding family or null if not
 */
function findFamily(families: Array<Family>, id: string): Family | null {
  let result: Family | null = null;
  families.forEach((family: Family) => {
    if (family.ids.includes(id)) {
      result = family;
    }
  });
  return result;
}

/**
 * Create a list of families from the list of editors
 */
function commputeGenealogie(data: Array<any>, field_name: string): Array<Family> {
  let families: Array<Family> = [];

  // Construct the families by just checking links
  // we only sets the ids and the links arrays
  data.forEach((item: any) => {
    // check if it is linked to an other editor
    if (item[field_name] && item[field_name].length > 0) {
      // retrieve all the linked id
      item[field_name].map((link: { relation: string; direction: string; other: any }) => {
        // Get the family of the link, or create it
        const familySource = findFamily(families, item.id);
        const familyTarget = findFamily(families, link.other.id);

        let family = familySource || familyTarget;

        // Merge families.
        // It can happen if the link is a bridge between  two branches
        if (familySource && familyTarget) {
          families = mergeFamilies(families, familySource, familyTarget);
          family = findFamily(families, item.id);
        }
        let isNewFamily = false;
        // Family creation
        if (family === null) {
          family = { ids: [], nodes: [], edges: [] };
          isNewFamily = true;
        }

        // Update the family
        // ~~~~~~~~~~~~~~~~~
        // add the editor id if needed
        if (family.ids.includes(item.id) === false) family.ids.push(item.id);
        // Add the linked entity's id if needed
        if (family.ids.includes(link.other.id) === false) family.ids.push(link.other.id);

        // add the relationship if needed
        if (["Fusion de", "Suite de", "Edition suivante de"].includes(link.relation))
          link.direction = link.direction === "in" ? "out" : "in";
        const rel: Link =
          // TODO: solve the fusion de reverse issue upstream
          link.direction === "out" //Fusion de are stored in the wrong way in Heurist
            ? { type: link.relation, from: item.id, to: link.other.id }
            : { type: link.relation, from: link.other.id, to: item.id };
        if (family.edges.findIndex((l: Link) => l.from === rel.from && l.to === rel.to && l.type === rel.type) === -1)
          family.edges.push(rel);

        // if it's a creation, push it
        if (isNewFamily) families.push(family);
      });
    }
  });

  // filling families with the node's object
  families = families.map((family: Family) => {
    family.nodes = family.ids.map((id: string) => {
      return data.find((e: any) => e.id === id);
    });
    // sort by date and branch
    switch (field_name) {
      case "structuresLiees":
        return sortEditorFamily(family);
      case "relationsEntreEditions":
        return sortBookGenealogy(family);
      default:
        return family;
    }
  });

  return families;

  function mergeFamilies(families: Array<Family>, familySource: Family, familyTarget: Family): Array<Family> {
    const mergedFamily = {
      ids: union(familySource.ids, familyTarget.ids),
      nodes: unionBy(familySource.nodes, familyTarget.nodes, "id"),
      edges: unionBy(familySource.edges, familyTarget.edges, (e) => `${e.from}${e.to}`),
    };

    const result = families.filter(
      (family) =>
        intersection(family.ids, familySource.ids).length === 0 &&
        intersection(family.ids, familyTarget.ids).length === 0,
    );
    result.push(mergedFamily);
    return result;
  }
  function createFamilyGraph(family: Family, edgeTypesToDiscard: string[]): DirectedGraph {
    const familyGraph = new DirectedGraph();
    family.nodes.forEach((n) => {
      familyGraph.mergeNode(n.id, n);
    });
    family.edges
      .filter((e) => !edgeTypesToDiscard.includes(e.type))
      .forEach((e: Link) => {
        familyGraph.mergeEdge(e.from, e.to, { type: e.type });
      });
    return familyGraph;
  }
  function sortEditorFamily(family: Family): Family {
    log.debug(`sort Family ${family.ids}`);
    const edgeTypesToDiscard = ["Fusionne avec"];
    // build a directed graph to sort the genealogy
    const familyGraph = createFamilyGraph(family, edgeTypesToDiscard);

    const sortNodeByDate = (id: string) => {
      const node = familyGraph.getNodeAttributes(id);
      if (node.type === "organisation") {
        return node.dateDeDebut ? node.dateDeDebut.date : -Infinity;
      }
    };

    // entrypoints
    const entrypoints = sortBy(
      familyGraph.nodes().filter((id) => familyGraph.inDegree(id) === 0),
      sortNodeByDate,
    );
    const orderedIDs: Array<string> = [];

    // main recursive graph crawl method
    const addDownstreamBranchFromID = (id: string) => {
      let next: string | null = id;
      while (
        next &&
        !orderedIDs.includes(next) &&
        familyGraph.outDegree(next) <= 1 &&
        familyGraph.inDegree(next) <= 1
      ) {
        orderedIDs.push(next);
        // we are sure there is only one next
        // stop if we reach a dead-end or an already added node
        next = familyGraph.outDegree(next) > 0 ? familyGraph.outNeighbors(next)[0] : null;
      }
      if (next && !orderedIDs.includes(next)) {
        // priority to incoming branches
        const upstreamBranchEntry = findLonguestUpStreamEntrypoint(next);
        if (upstreamBranchEntry) {
          log.debug(`rewind to ${upstreamBranchEntry}`);
          addDownstreamBranchFromID(upstreamBranchEntry);
          // the downstream will be added later by the upstreambranch downstream process
        } else {
          // add crossing only if upstream branches has been exhausted

          orderedIDs.push(next);
          // now add outgoing branches
          // TODO: define an order
          familyGraph
            .outNeighbors(next)
            .filter((id) => !orderedIDs.includes(id))
            .forEach((downstreamNode) => addDownstreamBranchFromID(downstreamNode));
        }
      }
      // return the last node we didn't add : a deadend
      return next;
    };
    // method to rewind from a crossing point to the farest entrypoint from which to downstream
    const findLonguestUpStreamEntrypoint = (upstreamNode: string): string | null => {
      const unvisitedEntrypoints = familyGraph
        .nodes()
        .filter((id) => familyGraph.inDegree(id) === 0 && !orderedIDs.includes(id));
      const paths = flatten(
        familyGraph
          // get upstream branches starting points
          .inNeighbors(upstreamNode)
          // filter already visited branches
          .filter((id) => !orderedIDs.includes(id))
          // get the entrypoint with longuest path
          .map((upstreamNode) => {
            const pathsToEntryPoints = unvisitedEntrypoints.map(
              (e) => [e, shortestPath(familyGraph, e, upstreamNode) ?? []] as Array<string>,
            );
            return (
              pathsToEntryPoints
                // keep only entrypoints which has a path to the upstreamnode
                .filter((es) => !!es[1])
            );
          }),
      );
      const longuestpath = maxBy(paths, (es) => (es[1] ? es[1].length : 0));
      if (longuestpath && longuestpath.length > 0) return longuestpath[0];
      //[0] to keep only the netrypoint (i.e. toremove path)
      else return null;
    };

    // start graph crawl from oldest entrypoint
    addDownstreamBranchFromID(entrypoints[0]);
    log.debug(`sorted Family ${orderedIDs}`);
    // chekc if we taversed all nodes
    if (orderedIDs.length !== familyGraph.order) {
      log.error(
        `The ordered nodes (${orderedIDs.length}/${familyGraph.order}) don't cover all genealogy nodes : ${familyGraph
          .nodes()
          .filter((n) => !orderedIDs.includes(n))
          .join(",")}`,
      );
    }
    // reorder family by orderedID
    return { ...family, nodes: sortBy(family.nodes, (n) => orderedIDs.indexOf(n.id)), ids: orderedIDs };
  }

  function sortBookGenealogy(bookGenealogy: Family): Family {
    const sortNodeByDate = (id: any) => [
      familyGraph.getNodeAttributes(id).dateEdition?.range.gte.getFullYear() || Infinity,
      familyGraph.getNodeAttributes(id).dateEdition?.range.lte.getFullYear() || -Infinity,
    ];
    // build a directed graph to sort the genealogy
    const edgeTypesToDiscard = [
      "A pour première édition",
      "Autre édition du même ouvrage",
      "Autre édition",
      "Editions",
    ];

    const familyGraph = createFamilyGraph(bookGenealogy, edgeTypesToDiscard);
    // entrypoints
    const entrypoints = sortBy(
      familyGraph.nodes().filter((id) => familyGraph.inDegree(id) === 0),
      sortNodeByDate,
    );

    const branch = (node: string): Array<string> => {
      return [node, ...flatten(familyGraph.outNeighbors(node).map(branch))];
    };
    const orderedNodeIds = flatten(entrypoints.map(branch));
    return {
      ...bookGenealogy,
      nodes: sortBy(bookGenealogy.nodes, (n) => orderedNodeIds.indexOf(n.id)),
      ids: orderedNodeIds,
    };
  }
}
