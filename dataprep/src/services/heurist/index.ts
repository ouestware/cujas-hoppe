import axios, { Method } from "axios";
import { head, isArray, isNil, merge } from "lodash";
import * as querystring from "querystring";
import { stringToCamelCase } from "../../utils";
import config, { ConfigDataSchema, ConfigHeurist } from "./../../config";
import { Logger, getLogger } from "./../logger";

export interface Dictionnary {
  enum: { [key: string]: string };
  relation: { [key: string]: string };
}

export class Heurist {
  private log: Logger = getLogger("Heurist");
  config: ConfigHeurist;
  sessionId: string | undefined;

  /**
   * Default constructor.
   */
  constructor(config: ConfigHeurist) {
    this.config = config;
    this.sessionId = undefined;
  }

  /**
   * Make the API auth process on heurist.
   *
   * @throws {Error} Throw an error if there is an exception.;
   */
  async login(): Promise<void> {
    const result = await this.makeCall(
      "usr_info.php",
      {
        a: "login",
        db: this.config.database,
        username: this.config.login,
        password: this.config.password,
        session_type: "shared",
      },
      "POST",
    );
    if (result.data.status !== "ok") {
      throw new Error(`An error occured during login process: ${JSON.stringify(result.data)}`);
    }

    // searching the sessionId
    const sessionIdCookie = (result.headers["set-cookie"] || [])
      .filter((e) => e.startsWith("heurist-sessionid"))
      .shift();
    this.log.debug(`Session id cookie is ${sessionIdCookie}`);

    if (isNil(sessionIdCookie))
      throw new Error(`Can't find cookie in login response headers : ${JSON.stringify(result.headers)}`);

    const regexResult = sessionIdCookie.match(/heurist-sessionid=(.*);/);
    if (isNil(regexResult)) throw new Error(`Can't find sessionId in cookie ID`);

    this.log.debug(`heurist-sessionid is ${regexResult[1]}`);
    this.sessionId = regexResult[1];
  }

  /**
   * Generate the schema of Heurist.
   */
  async generateSchema(): Promise<ConfigDataSchema> {
    const result = await this.makeCall("sys_structure.php", {
      rectypes: "all",
      terms: "all",
      mode: "2",
    });
    const schemaDefinition = result.data.data.rectypes;
    const relationDefinition = Object.keys(result.data.data.terms.termsByDomainLookup.relation).map((id) => {
      return result.data.data.terms.termsByDomainLookup.relation[id].concat(id);
    });

    return Object.keys(schemaDefinition.names).map((modelId) => {
      return {
        id: modelId,
        name: stringToCamelCase(schemaDefinition.names[modelId]),
        fields: Object.keys(schemaDefinition.typedefs[modelId].dtFields)
          .map((fieldId) => {
            const field = schemaDefinition.typedefs[modelId].dtFields[fieldId];
            return {
              id: fieldId,
              name: stringToCamelCase(field[DT_FIELD_NAMES.DisplayName]) || "NA",
              required: field[DT_FIELD_NAMES.RequirementType] === "required" ? true : false,
              type: field[DT_FIELD_NAMES.Type] || "string",
              isArray:
                parseInt(field[DT_FIELD_NAMES.MaxValues]) > 1 ||
                parseInt(field[DT_FIELD_NAMES.MaxValues]) === 0 ||
                field[DT_FIELD_NAMES.Type] === "resource" ||
                field[DT_FIELD_NAMES.Type] === "relmarker"
                  ? true
                  : false,
              depth:
                field[DT_FIELD_NAMES.Type] === ("resource" || field[DT_FIELD_NAMES.Type] === "relmarker")
                  ? 1
                  : undefined,
              resourceTypes:
                field[DT_FIELD_NAMES.Type] === "resource" || field[DT_FIELD_NAMES.Type] === "relmarker"
                  ? field[DT_FIELD_NAMES.PtrFilteredIDs].split(",")
                  : undefined,
              relationTypes:
                field[DT_FIELD_NAMES.Type] === "relmarker"
                  ? relationDefinition
                      .filter(
                        (rel) =>
                          rel[DT_RELATION_NAMES.trm_ParentTermID] === field[DT_FIELD_NAMES.FilteredJsonTermIDTree],
                      )
                      .map((rel) => {
                        return {
                          id: rel[rel.length - 1],
                          name: rel[DT_RELATION_NAMES.trm_Label],
                        };
                      })
                  : undefined,
            };
          })
          // separator are not a data type, it's just a UI info for the form
          .filter((t) => t.type !== "separator"),
      };
    });
  }

  /**
   * Given a model id, this function call heurist and
   * returns an array of object. It's like a `select * from myTable`.
   */
  async getModelList(id: string): Promise<any> {
    const modelDef = config.schema.find((e) => e.id === id);
    if (!modelDef) {
      throw new Error(`Model with ID ${id} is not defined in configuration`);
    }

    const result = await this.makeCall("record_search.php", {
      q: JSON.stringify({ t: id }),
      w: "a",
      limit: "100000",
      needall: "1",
      detail: "complete",
    });
    this.log.info(
      `Heurist returns ${Object.keys(result.data.data.records).length} result(s) for model ${modelDef.name}`,
    );
    this.log.debug(
      `Heurist result for model ${modelDef.name} is : ${JSON.stringify(result.data.data.records, null, 2)}`,
    );

    const allRelations: Array<{ recID: number; sourceID?: number; targetID?: number; trmID: number }> = (
      result.data.data.relations && result.data.data.relations.direct ? result.data.data.relations.direct : []
    ).concat(
      result.data.data.relations && result.data.data.relations.reverse ? result.data.data.relations.reverse : [],
    );

    return Object.keys(result.data.data.records).map((id) => {
      const record = result.data.data.records[id];
      const object: { [key: string]: unknown } = {
        id: id,
        type: modelDef.name,
      };
      modelDef.fields.forEach((field) => {
        if (field.type !== "relmarker") {
          const fieldParsedValue = this.parseHeuristField(record, field, record.d[field.id]);
          if (fieldParsedValue) {
            object[field.name] = fieldParsedValue;
          }
        } else {
          const relations = allRelations
            .filter(
              (rel) =>
                "" + rel.recID === "" + id && (field.relationTypes || []).find((e) => "" + e.id === "" + rel.trmID),
            )
            .map((rel) => {
              return {
                relation: (field.relationTypes || []).find((e) => "" + e.id === "" + rel.trmID)?.name,
                direction: rel.targetID ? "out" : "in",
                other: {
                  id: "" + (rel.targetID ? rel.targetID : rel.sourceID),
                  // yes it's a shortcut, but in the nested function, if the object is empty (only id & type)
                  // it is removed. And to find the element we only need the good ID.
                  // So finally it's good
                  type: head(field.resourceTypes),
                },
              };
            });
          if (relations.length > 0) {
            object[field.name] = relations;
          }
        }
      });
      return object;
    });
  }

  async getDictionnary(): Promise<Dictionnary> {
    const result: Dictionnary = { enum: {}, relation: {} };
    const data = await this.makeCall("sys_structure.php", {
      mode: "2",
      terms: "all",
    });
    Object.keys(data.data.data.terms.termsByDomainLookup.relation).map((id) => {
      result.relation[id] = data.data.data.terms.termsByDomainLookup.relation[id][0];
    });
    Object.keys(data.data.data.terms.termsByDomainLookup.enum).map((id) => {
      result.enum[id] = data.data.data.terms.termsByDomainLookup.enum[id][0];
    });
    return result;
  }

  /**
   * Make a HTTP call to Heurist
   *
   * @param {string} ctrl The heurist controller to call
   * @param {object} body The body to send
   * @param {Method} method The heurist controller to call
   * @returns {Promise<AxiosResponse>} The axios response
   * @throws {AxiosError} If an error occured (ie. the code is not a 20X)
   */
  private async makeCall(ctrl: string, body: any, method: Method = "POST") {
    // deafult header
    const headers = {
      withCredentials: true,
      Cookie: this.sessionId ? `heurist-sessionid=${this.sessionId};` : "",
      "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    };
    this.log.debug(`Calling heurist with ${ctrl}, ${querystring.stringify(merge({ db: this.config.database }, body))}`);
    // make the http call
    const response = await axios({
      url: `${this.config.url}${this.config.url.endsWith("/") ? "/" : ""}hserv/controller/${ctrl}`,
      method: method,
      headers: headers,
      responseType: "json",
      data: querystring.stringify(merge({ db: this.config.database }, body)),
    });
    this.log.debug(`Heurist response is ${JSON.stringify(response.data, null, 2)}`);
    return response;
  }

  private parseHeuristField(record: { id: string; type: string }, definition: any, value: any) {
    let result: any = null;
    if (isArray(value)) {
      // if it's an array, we filter it by null values
      result = value
        .filter((e) => !NULLABLE_FIELD_VALUES.includes(e))
        .map((e) => {
          return this.castHeuristSingleValue(record, definition, e);
        });

      // because every field are returned as an array, we reformat it
      if (value.length === 1) {
        if (result.lenght === 0) {
          result = null;
        }
        result = result[0];
      }
    } else {
      if (!NULLABLE_FIELD_VALUES.includes(value)) {
        result = this.castHeuristSingleValue(record, definition, value);
      }
    }
    if (definition.required === true && result === null) {
      throw new Error(
        `Field ${definition.name} (${definition.id}) on record ${JSON.stringify(
          record,
        )} is missing but required in schema definition`,
      );
    }

    // transform the result to array or not
    if (definition.isArray && !isArray(result) && result !== null) {
      result = [result];
    }
    if (!definition.isArray && isArray(result) && definition.type !== "geo") {
      result = result.length > 0 ? result[0] : null;
    }
    return result;
  }
  // method not used anymore choosing first date to represent range. Keep it here as it might change
  // private middleDate(date1: Date, date2: Date) {
  //   const startDate = date1 < date2 ? date1 : date2;
  //   const endDate = date1 < date2 ? date2 : date1;

  //   return new Date(startDate.getTime() + (endDate.getTime() - startDate.getTime()) / 2);
  // }
  private castHeuristSingleValue(record: { id: string; type: string }, definition: any, value: any) {
    let result: any = null;
    if (value !== undefined && value !== null && ("" + value).trim() !== "") {
      switch (definition.type) {
        case "year":
        case "integer": {
          result = parseInt(value);
          if (isNaN(result)) {
            this.log.warn(
              `Can't cast value "${value}" to integer for field ${definition.name} (${
                definition.id
              }) on record ${JSON.stringify(record)}`,
            );
            result = null;
          }
          break;
        }
        // this is a custom type
        case "dateRange": {
          const yearRangeValue = value;

          // format can be YYYY-YYYY in the date field
          const matchYears = ("" + yearRangeValue).match(/^([0-9]{4})-([0-9]{4})$/);
          if (matchYears && matchYears.length === 3) {
            const gte = this.castHeuristSingleDate(matchYears[1]);
            const lte = this.castHeuristSingleDate(matchYears[2], true);
            result = {
              type: "year-range",
              date: gte,
              range: {
                gte,
                lte,
              },
            };
            this.log.debug(`dateRange - ${value} is just a year range => ${JSON.stringify(result)}`);
          }

          // format can be |VER=1|TYP=p|TPQ=1800|TAQ=1811|DET=0|CLD=Gregorian|SPF=0|EPF=0
          const macthComplexeDatePeriod = ("" + yearRangeValue).match(/TPQ=([0-9-]*).*TAQ=([0-9-]*)/);
          if (macthComplexeDatePeriod && macthComplexeDatePeriod.length === 3) {
            const gte = this.castHeuristSingleDate(macthComplexeDatePeriod[1]);
            const lte = this.castHeuristSingleDate(macthComplexeDatePeriod[2], true);
            result = {
              type: "date-range",
              date: gte,
              range: {
                gte,
                lte,
              },
            };
            this.log.debug(
              `dateRange - ${value} is a TPQ / TAQ date : ${macthComplexeDatePeriod} => ${JSON.stringify(result)}`,
            );
          }

          // format can be  |VER=1|TYP=s|DAT=1852-10|DET=0|CLD=Gregorian"
          const matchStringDate = ("" + yearRangeValue).match(/\|VER=1\|TYP=s\|DAT=([0-9-]*)\|.*/);
          if (matchStringDate && matchStringDate.length === 2) {
            const stringDate = this.castHeuristSingleDate(matchStringDate[1]);
            result = {
              type: "date",
              date: stringDate,
              range: {
                gte: stringDate,
                lte: stringDate,
              },
            };
          }

          // format can be  |VER=1|TYP=f|DAT=1900|RNG=P50Y|COM=1XXX|DET=0|CLD=Gregorian|PRF=0
          const matchApproximateDate = ("" + yearRangeValue).match(/\|DAT=([0-9-]*)\|.*RNG=P([0-9]*)([Y|M|D])\|/);
          if (matchApproximateDate && matchApproximateDate.length === 4) {
            const approximateDate = this.castHeuristSingleDate(matchApproximateDate[1]);
            if (approximateDate) {
              const approximateDuration = parseInt(matchApproximateDate[2]);
              switch (matchApproximateDate[3]) {
                case "Y": {
                  result = {
                    type: "date-approximate",
                    date: new Date(approximateDate.getTime()),
                    range: {
                      gte: new Date(approximateDate.setFullYear(approximateDate.getFullYear() - approximateDuration)),
                      lte: new Date(
                        approximateDate.setFullYear(approximateDate.getFullYear() + 2 * approximateDuration),
                      ),
                    },
                  };
                  break;
                }
                case "M": {
                  result = {
                    type: "date-approximate",
                    date: new Date(approximateDate.getTime()),
                    range: {
                      gte: new Date(approximateDate.setMonth(approximateDate.getMonth() - approximateDuration)),
                      lte: new Date(approximateDate.setMonth(approximateDate.getMonth() + 2 * approximateDuration)),
                    },
                  };
                  break;
                }
                case "D": {
                  const dayDurationInMs = approximateDuration * 3600 * 24 * 1000;
                  result = {
                    type: "date-approximate",
                    date: new Date(approximateDate.getTime()),
                    range: {
                      gte: new Date(approximateDate.getTime() - dayDurationInMs),
                      lte: new Date(approximateDate.getTime() + 2 * dayDurationInMs),
                    },
                  };
                  break;
                }
              }
            }
            this.log.debug(`dateRange - ${value} is a RNG date : ${matchApproximateDate} => ${JSON.stringify(result)}`);
          }

          // if no result, we try to cast the value as only one date
          if (result === null) {
            const singleDate = this.castHeuristSingleDate(yearRangeValue);
            if (singleDate === null) {
              this.log.warn(
                `Can't cast value "${value}" to date range for field ${definition.name} (${
                  definition.id
                }) on record ${JSON.stringify(record)}`,
              );
            } else {
              result = {
                type: "date",
                date: this.castHeuristSingleDate(yearRangeValue),
                range: {
                  gte: this.castHeuristSingleDate(yearRangeValue),
                  lte: this.castHeuristSingleDate(yearRangeValue, true),
                },
              };
            }
          }

          if (result === null && value !== undefined && ("" + value).trim() !== "") {
            this.log.warn(
              `Can't cast value "${value}" to year range for field ${definition.name} (${
                definition.id
              }) on record ${JSON.stringify(record)}`,
            );
          } else {
            if (result.range && result.range.gte > result.range.lte) {
              // check if range is in the right order, swap if not
              const swap = result.range.gte;
              result.range.gte = result.range.lte;
              result.range.lte = swap;
            }
          }

          break;
        }
        case "date": {
          result = this.castHeuristSingleDate(value);
          if (result === null && value !== undefined && ("" + value).trim() !== "") {
            this.log.warn(
              `Can't cast value "${value}" to date for field ${definition.name} (${
                definition.id
              }) on record ${JSON.stringify(record)}`,
            );
          }
          break;
        }
        case "boolean": {
          result = !!value;
          break;
        }
        case "resource": {
          // this.log.warn(`resource is ${value} for field ${definition.name} (${definition.id})`);
          result = value;
          break;
        }
        case "relmarker": {
          this.log.warn(
            `relmarker is "${value}" for field ${definition.name} (${definition.id}) on record ${JSON.stringify(
              record,
            )}`,
          );
          result = value;
          break;
        }
        case "float": {
          result = parseFloat(value);
          if (isNaN(result)) {
            this.log.warn(
              `Can't cast value "${value}" to float for field ${definition.name} (${
                definition.id
              }) on record ${JSON.stringify(record)}`,
            );
            result = null;
          }
          break;
        }
        case "geo": {
          const matchRegex = ("" + value).match(/^p POINT\((-?[0-9]*\.[0-9]*) (-?[0-9]*\.[0-9]*)\)$/);
          if (matchRegex === null || matchRegex.length !== 3) {
            this.log.warn(
              `Can't cast value "${value}" to geo point for field ${definition.name} (${
                definition.id
              }) on record ${JSON.stringify(record)}`,
            );
            result = null;
          } else {
            result = [parseFloat(matchRegex[1]), parseFloat(matchRegex[2])];
          }
          break;
        }
        case "file": {
          result = `${config.heurist.url}?db=${config.heurist.database}&file=${value.ulf_ObfuscatedFileID}`;
          break;
        }
        default:
          result = "" + value;
          break;
      }
    }
    return result;
  }

  /**
   * Parse a date from heurist and return a JS date.
   * @param {string} dateValue The heurist value to parse
   * @param {boolean} toLastDayOfYear If we have a partial date (ie. just a year) per default we put first day of the year. But if this at true, instead we put the last day of the year (ie 31st december)
   * @retruns {Date}
   */
  private castHeuristSingleDate(dateValue: string, toLastDayOfYear = false): Date | null {
    let result = null;

    // it's a standard date from heurist, ie YYYY-MM-DD HH:mm:ss
    result = new Date(dateValue);

    // it's a complexe date in heurist
    const complexeDateMatch = dateValue.match(/DAT=([0-9-]*)/);
    if (complexeDateMatch && complexeDateMatch.length === 2) {
      dateValue = complexeDateMatch[1];
    }

    // date in DD/MM/YYYY
    const splitAtSlash = ("" + dateValue).split("/");
    if (splitAtSlash.length === 3) {
      result = new Date(parseInt(splitAtSlash[2]), parseInt(splitAtSlash[1]) - 1, parseInt(splitAtSlash[0]));
    }

    // date in MM/YYYY
    if (splitAtSlash.length === 2) {
      result = new Date(parseInt(splitAtSlash[1]), parseInt(splitAtSlash[0]) - 1, 1);
    }

    // date in YYYY-MM
    const matchYearMonth = ("" + dateValue).match(/^([0-9]{4})-([0-9]{2})$/);
    if (matchYearMonth && matchYearMonth.length === 2) {
      result = new Date(parseInt(matchYearMonth[1]), parseInt(matchYearMonth[0]) - 1, 1);
    }

    // date periode in format "YYYY-YYYY" : we take the first
    const matchPeriodYears = ("" + dateValue).match(/^([0-9]{4})-([0-9]{4})$/);
    if (matchPeriodYears && matchPeriodYears.length === 3) {
      if (toLastDayOfYear) {
        result = new Date(parseInt(matchPeriodYears[1]), 11, 31);
      } else {
        result = new Date(parseInt(matchPeriodYears[1]), 0, 1);
      }
    }

    // date in format "YYYY"
    const matchPeriodYear = ("" + dateValue).match(/^([0-9]{4})$/);
    if (matchPeriodYear && matchPeriodYear.length === 2) {
      if (toLastDayOfYear) {
        result = new Date(parseInt(matchPeriodYear[1]), 11, 31);
      } else {
        result = new Date(parseInt(matchPeriodYear[1]), 0, 1);
      }
    }

    // Check if the date is valid
    if (result !== null && isNaN(result.getTime())) {
      result = null;
    }
    return result;
  }
}

/**
 * List the field name (with their array index) of a rectype definition.
 * Can be retrieve with `window.hWin.HEURIST4.rectypes.typedefs.dtFieldNamesToIndex`
 */
const DT_FIELD_NAMES: { [field: string]: number } = {
  DisplayName: 0,
  DisplayHelpText: 1,
  DisplayExtendedDescription: 2,
  DisplayOrder: 3,
  DisplayWidth: 4,
  DisplayHeight: 5,
  DefaultValue: 6,
  RecordMatchOrder: 7,
  CalcFunctionID: 8,
  RequirementType: 9,
  NonOwnerVisibility: 10,
  Status: 11,
  OriginatingDBID: 12,
  MaxValues: 13,
  MinValues: 14,
  DisplayDetailTypeGroupID: 15,
  FilteredJsonTermIDTree: 16,
  PtrFilteredIDs: 17,
  CreateChildIfRecPtr: 18,
  PointerMode: 19,
  PointerBrowseFilter: 20,
  OrderForThumbnailGeneration: 21,
  rst_TermIDTreeNonSelectableIDs: 22,
  Modified: 23,
  LocallyModified: 24,
  dty_TermIDTreeNonSelectableIDs: 25,
  FieldSetRectypeID: 26,
  Type: 27,
  ConceptID: 28,
};

/**
 * List the field name (with their array index) of a relationship definition.
 * Can be retrieve with `window.hWin.HEURIST4.terms.fieldNamesToIndex`
 */
const DT_RELATION_NAMES = {
  trm_AddedByImport: 6,
  trm_ChildCount: 10,
  trm_Code: 15,
  trm_ConceptID: 17,
  trm_Depth: 12,
  trm_Description: 2,
  trm_Domain: 8,
  trm_HasImage: 18,
  trm_IDInOriginatingDB: 5,
  trm_InverseTermID: 1,
  trm_IsLocalExtension: 7,
  trm_Label: 0,
  trm_LocallyModified: 14,
  trm_Modified: 13,
  trm_OntID: 9,
  trm_OriginatingDBID: 4,
  trm_ParentTermID: 11,
  trm_SemanticReferenceURL: 16,
  trm_Status: 3,
};

const NULLABLE_FIELD_VALUES = [
  "",
  "TEXT non trouve",
  "[sans date]",
  "XXXX",
  "[Lieu de naissance non renseigné]",
  "[Lieu de mort non renseigné]",
];
