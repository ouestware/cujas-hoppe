import { ConfigDataSchema, SchemaHeuristFieldDefinition, SchemaModelDefinition } from "./config";
import { Logger, getLogger } from "./services/logger";
import { Dictionnary } from "./services/heurist";
import { isObject, omitBy } from "lodash";

const log: Logger = getLogger("Utils");

const allFieldsOfModel = (modelDef: SchemaModelDefinition): SchemaHeuristFieldDefinition[] => {
  return modelDef.fields.concat((modelDef?.computed_fields as SchemaHeuristFieldDefinition[]) || []);
};

export function stringToCamelCase(text: string) {
  return text
    .toLowerCase()
    .replace("(s)", "s")
    .replace(/[()<>?]/g, "")
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/[^a-zA-Z0-9]+(.)/g, (_m, chr) => chr.toUpperCase())
    .trim();
}

export function heuristPostProcessing(
  schema: ConfigDataSchema,
  dictionnary: Dictionnary,
  data: { [model: string]: Array<any> },
): { [model: string]: Array<any> } {
  // Construct an index by id of all the db
  const itemById: { [id: string]: any } = {};
  Object.keys(data).forEach((id) => {
    data[id].forEach((model) => {
      if (itemById[model.id]) {
        log.info(
          `Model with id ${model.id} already exist : ${JSON.stringify(
            itemById[model.id],
            null,
            2,
          )} AND ${JSON.stringify(model, null, 2)}`,
        );
      }
      itemById[model.id] = model;
    });
  });

  // Init the result
  const dataToReturn: { [key: string]: Array<any> } = {};

  // for every model we have
  Object.keys(data).forEach((modelId) => {
    // searching in schema the model definition with the nested fields
    const modelDef = schema.find((e) => e.id === modelId);

    // iterate over the collection for the model
    dataToReturn[modelId] = data[modelId].map((model) => {
      const result = Object.assign({}, model);

      if (modelDef) {
        // nested and postprocessing on heurist field
        allFieldsOfModel(modelDef).forEach((fieldDef) => {
          if (model[fieldDef.name]) {
            result[fieldDef.name] = getFieldValue(
              schema,
              itemById,
              dictionnary,
              fieldDef,
              model[fieldDef.name],
              fieldDef.depth || 1,
            );
          }
        });
      }

      return result;
    });
  });

  return dataToReturn;
}

/**
 * Recursive function that compute the field value of a model item.
 *
 * @param {ConfigDataSchema} schema the data schema to import
 * @param {{ [key: string]: any }} itemById map of all DB indexed by the model id
 * @param {Dictionnary}  dictionnary The Heurist dictionnary
 * @param {SchemaHeuristFieldDefinition} fieldDefinition The schema definition of the current field
 * @param {any} fieldValue The value of the current field
 * @param {number} depth The depth value for the recursivity (ie should we continue the rescursivity)
 * @returns The field value
 */
function getFieldValue(
  schema: ConfigDataSchema,
  itemById: { [key: string]: any },
  dictionnary: Dictionnary,
  fieldDefinition: SchemaHeuristFieldDefinition,
  fieldValue: any,
  depth: number,
): any {
  let result = undefined;

  // if the value is an array, we recall this function (with same params) for each element
  if (Array.isArray(fieldValue) && ["enum", "resource", "relmarker"].includes(fieldDefinition.type)) {
    result = fieldValue
      .map((value) => getFieldValue(schema, itemById, dictionnary, fieldDefinition, value, depth))
      .filter((e) => e !== undefined && e !== null);
  } else {
    switch (fieldDefinition.type) {
      // Enum is a ref to a dictionnary item
      case "enum":
        if (("" + fieldValue).trim() !== "") {
          // Make a log if the enum is not present (not normal)
          if (dictionnary.enum[fieldValue] === undefined) {
            log.warn(`Enum with id "${fieldValue}" for field ${JSON.stringify(fieldDefinition)} is missing`);
            break;
          }
          if (dictionnary.enum[fieldValue].trim() !== "") {
            result = dictionnary.enum[fieldValue];
          }
        }
        break;
      // Resource is a ref to an other object by its id
      case "resource":
        if (depth <= 0) {
          result = null;
          break;
        }
        if (("" + fieldValue).trim() !== "") {
          if (itemById[fieldValue] === undefined) {
            log.warn(
              `Field of type ${JSON.stringify(
                fieldDefinition,
              )} with id "${fieldValue}" is not present in our database. Perhaps a model type has been missed in the schema or the foreign key is absent in heurist.`,
            );
          } else {
            const item = Object.assign({}, itemById[fieldValue]);
            // searching in schema the model definition for the nested object
            const modelDef = schema.find((e) => e.name === item.type);
            if (modelDef) {
              result = Object.assign({}, item);
              // recursivity
              allFieldsOfModel(modelDef).forEach((field) => {
                if (item[field.name]) {
                  item[field.name] = getFieldValue(schema, itemById, dictionnary, field, item[field.name], depth - 1);
                }
              });
            }
            result = item;
          }
        }
        break;
      case "relmarker":
        if (depth <= 0) {
          result = null;
          break;
        }
        if (fieldValue.other && fieldValue.relation) {
          if (itemById[fieldValue.other.id] === undefined) {
            log.error(
              `Field of type ${JSON.stringify(
                fieldDefinition,
              )} with id "${fieldValue}" is not present in our database. Perhaps a model type has been missed in the schema or the foreign key is absent in heurist.`,
            );
          } else {
            const item = Object.assign({}, itemById[fieldValue.other.id]);
            // searching in schema the model definition with the nested fields
            const modelDef = schema.find((e) => e.name === item.type);
            if (modelDef) {
              // recursivity
              allFieldsOfModel(modelDef).forEach((field) => {
                if (item[field.name]) {
                  item[field.name] = getFieldValue(schema, itemById, dictionnary, field, item[field.name], depth - 1);
                }
              });
            }
            result = Object.assign({ relation: fieldValue.relation, direction: fieldValue.direction }, { other: item });
          }
        }
        break;
      default:
        result = fieldValue;
        break;
    }

    if (fieldDefinition.postprocess) {
      result = fieldDefinition.postprocess(result);
    }

    // if the value is just an object with two props (ie. id & type), it's an empty object
    if (
      isObject(result) &&
      Object.keys(result).includes("id") &&
      Object.keys(omitBy(result, (value) => value === undefined)).length === 2
    ) {
      log.warn(`Field ${fieldDefinition.name} is empty : ${JSON.stringify(result)}`);
      result = undefined;
    }
  }
  return result;
}

export function heuristComputeFields(
  schema: ConfigDataSchema,
  data: { [model: string]: Array<any> },
): { [model: string]: Array<any> } {
  schema.map((model) => {
    if (model.computed_fields && model.computed_fields.length > 0) {
      data[model.id] = data[model.id].map((item) => {
        (model.computed_fields || []).forEach((computeFieldDef) => {
          if (computeFieldDef.computeField) item[computeFieldDef.name] = computeFieldDef.computeField(data, item);
        });

        return item;
      });
    }
  });
  return data;
}
