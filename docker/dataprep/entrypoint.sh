#!/bin/bash
cd /dataprep

if [ "$MODE" = "dev" ]; then
  echo "/!\\ Mode is set to DEV /!\\"
else
  echo "/!\\ Mode is set to PRODUCTION /!\\"
fi
echo "(i) Npm version is $(npm -v)"
echo "(i) Node version is $(node -v)"

echo
echo " ~"
echo " ~ Install dependencies"
echo " ~"
echo
npm install
export configuration="docker"

# Waiting until es is available
echo "Waiting es to be ready"
until $(curl --output /dev/null --silent --head --fail http://elasticsearch:9200/); do
    echo 'Still waiting for es'
    sleep 1
done

if [ "$MODE" = "dev" ]; then
  echo
  echo " ~"
  echo " ~ Start the application"
  echo " ~"
  echo
  npm run start
else
  echo
  echo " ~"
  echo " ~ Building the application"
  echo " ~"
  echo
  npm run build

  echo
  echo " ~"
  echo " ~ Run the production server"
  echo " ~"
  echo
  node ./dist/index.js
fi
